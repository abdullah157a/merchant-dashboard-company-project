
$('#close__leftsidebar').on('click',function () {

    $('body').addClass("m-brand--minimize m-aside-left--minimize");
    $('#m_aside_left_minimize_toggle').removeClass('m-brand__toggler--active');
});
// Side bar icon show and dide
$('#m_aside_left_minimize_toggle').click(function () {
    $('.side_bar_close_image').toggle();
});

$(".cancel-order-dd-list").change(function () {
    if(this.value=="Other"){
        $('.cancel-reason').show();
    }
    else{
        $('.cancel-reason').hide();
    }
});

// click on cancel button to hide
$(document).ready(function(){
    $("#hide").click(function(){
        $(".filter-form").hide();
    });
});

$("#mobile_no").on('keyup change', function (){
    if($(this).val() !==''){
        $('#mobile_phone_error').hide();
        $("#mobile_pencil").hide();
        $('#mobile_no').attr('style', 'background: #ffffff !important');
        $('#mobile_no').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
        $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#mobile_phone_error').show();
        $("#mobile_no").css("border", "2px solid #e88a8a");
        $("#mobile_pencil").show();
        $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");
    }
});

$("#user_email").on('keyup change', function (){
    if($(this).val() !==''){
        $('#email_error').hide();
        $('#user_email').attr('style', 'background: #ffffff !important');
        $('#user_email').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
        $("#email_pencil").hide();
        $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#email_error').show();
        $("#user_email").css("border", "2px solid #e88a8a");
        $("#email_pencil").show();
        $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");
    }
});
$("#password").on('keyup change', function (){
    if($(this).val() !==''){
        $('#password_error').hide();
        $("#password").css("border", "2px solid rgb(53, 186, 182)");
        $('#password').attr('style', 'background: #ffffff !important');
        $('#password').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
        $(".pencel_icon").hide();
        $(".login-eye").show();
        $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#password_error').show();
        $(".pencel_icon").show();
        $("#password").css("border", "2px solid #e88a8a");
        $(".login-eye").hide();
        $(".login-open-eye").hide();
        $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");

    }
});


$( '#m_filter_dd ' ).on('click', function () {
    if($(".dropdown-menu").hasClass("show")){
        $('.dropdown-menu').prop('selectedIndex', 1); // select 4th option
        $('.bootstrap-select > .dropdown-toggle:after').attr('style', 'content: !important');
        $('.bootstrap-select > .dropdown-toggle:after').attr('style', 'content:\f0d8 !important');
    }
    else{
        $('.bootstrap-select > .dropdown-toggle:after').attr('style', 'content:\f0dd !important');
    }


});


// search Filter
$('#searchfilter').click(function () {
       $('.filter-form').toggle();

});


// user profile js
function createprofilestars(n) {
    return new Array(n+1).join("*")
}


$(document).ready(function() {

    var timer = "";

    // $(".panel").append($('<input type="text" class="profilehidpassw" />'));

    $(".profilehidpassw").attr("name", $(".profilepass").attr("name"));

    $(".profilepass").attr("type", "text").removeAttr("name");

    $("body").on("keypress", ".profilepass", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".profilehidpassw").val($(".profilehidpassw").val() + character);
        }


    });

    $("body").on("keyup", ".profilepass", function(e) {
        $(".login-open-eye").hide();
        var code = e.which;
        if (code == 8) {
            var length = $(".profilepass").val().length;
            $(".profilehidpassw").val($(".profilehidpassw").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.profilepass').val().length;
            $(".profilepass").val(createprofilestars(current_val - 1) + $(".profilepass").val().substring(current_val - 1));
        }

        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".profilepass").val(createprofilestars($(".profilepass").val().length));
        }, 200);

    });

});

// Order Riview js
$('.review-order-link').click(function () {
    $('.order_review_box').show();
    $('.order_review_box').css({"width":"300px","background-color":"#f7f7f7","border":"1px solid rgb(249, 249, 249)","box-shadow":" 0 0px 0px 0 rgba(0, 0, 0, 0)","position":"relative","bottom":"1em","margin-left":"12px"});
    $('#order_details').removeClass("order-details");
    $('.order-review-para-text').css({"margin": "-0.6875em  0.75em"});
    $('.responsive_order_id').css({"display": "flex","padding":"28px 0px 0px 0px"});
    $('.order_responsive_order_id').css({"display": "flex","padding":"28px 0px 0px 0px"});
    $('.data_value_id').css("text-align","right");
    $('.col-md-9').css("display","none");
    $('.side_bar_line').hide();
    $('.case__opened').hide();
    $('.order-review').hide();
    $('.contact-info').hide();
    $('.m-subheader__title').hide();
    $('.review-order-link').hide();
    $('.order-review-para-text').show();
    $('.dasktop_order_id').hide();
    $('#alert-message').hide();
    $(".order_review_box_heading").hide();
});



//chat box js

$('.chat-box-title').click(function () {
    $('#inbox-sidebar').addClass("chatbox-responsive-title");
    $('.m-subheader__title ').addClass('m-responsive-subheader__title');
    $('#r_m_aside_left_minimize_toggle').addClass('chatbox-sidebar');
    $(".m-footer").addClass("chatbox-footer");
    $('.go-back').addClass("return-chatbox");
    $('#inbox-body').addClass('chat-box');
    $('#m_header_topbar').addClass("chatbox-topbar");
});

var starPasswordValue;
$(".login-eye").click(function(){
    $(".login-open-eye").show();
    $('.login-eye').hide();
    starPasswordValue=$(".m-profile-password-input").val();
    $('#password').val($('.profilehidpassw').val());
});
$(".login-open-eye").click(function(){
    $(".login-eye").show();
    $(".login-open-eye").hide();
    $('#password').val(starPasswordValue);
});

$("input[type=file]").on("click", function(e){
    e.stopPropagation();

});

$('#OpenImgUpload').click(function(e){
    $('#imgupload').val('');
    $('#imgupload').trigger('click');

});
$("#imgupload").change(function(){
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#editProfile').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

        $(".delete_profile").show();

    }
}


$(".delete_profile").click(function(e){

    $('#editProfile').attr('src', 'assets/img/profile/edit-profile.svg');
    $(".delete_profile").toggle();
});

function openRightMenu() {
    document.getElementById("rightMenu").style.display = "block";
    $('body').append('<div class="m-quick-notification-sidebar-overlay"></div>');
}


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            $('#'+$(this).attr('data-id')).text('Show more');
            $('#'+$(this).attr('data-id')).css('color','rgba(0, 0, 0, 0.5)');
            $('#'+this.id).removeClass('text');
        } else {
            <!--  alert(this.id); -->
            panel.style.maxHeight = panel.scrollHeight + "px";
            $('#'+$(this).attr('data-id')).text('Show less');
            $('#'+$(this).attr('data-id')).css('color','rgba(0, 0, 0, 0.7)');
            $('#'+this.id).addClass('text');

        }
    });
}

/* get help js */

$('html').click(function() {
    $('#rightMenu').hide();
    $('.m-quick-notification-sidebar-overlay').remove();
});

$('#m_header_topbar').click(function(event){
    event.stopPropagation();
});


$("#help_menu").click(function(){
    document.getElementById("helpMenu").style.display = "block";
});

$("#refund-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "hidden";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#ffd95a");

});

$("#chargeback-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "hidden";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#ffd95a");
});

$("#chat-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "hidden";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#ffd95a");
});


$("#other-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "visible";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#d6d6d6");
});

$("#subject_description").bind("keyup change", function(e) {
    if($(this).val()!='')
        $('.continue-btn').css("background-color","#ffd95a");
    else{
        $('.continue-btn').css("background-color","#d6d6d6");
    }
})

$('html').click(function() {
    $('#helpMenu').hide();
});

$('#help_menu').click(function(event){
    event.stopPropagation();
});

$('#helpMenu').click(function(event){
    event.stopPropagation();
});

/* End get help js */

$( ".r_aside_left_minimize_toggle" ).click(function(){
   $('.m-aside-left').css("left","auto");
});

$('html').click(function() {
   $(".m-aside-left").removeAttr("style");

});

$('#m_aside_left').click(function(event){
   event.stopPropagation();
});


$('#close_sidebar').click(function(){
    $('body').removeClass('m-brand--minimize m-aside-left--minimize');
});

$(document).ready(function(){
    $(".filter").click(function(event){
        $('.filter').toggleClass("m-dropdown--open");
        if($('.filter').hasClass("m-dropdown--open")){
            $('.filter').css({"background-color":"#f0cb00","border-radius":"25px"});
            $('.filter_down_arrow').css("visibility","hidden");
            $('.filter_up_arrow').css("visibility","visible");
        }
        else{
            $('.filter').css({"background-color":"transparent","border-radius":"0px"});
            $('.filter_down_arrow').css("visibility","visible");
            $('.filter_up_arrow').css("visibility","hidden");
        }
        event.stopPropagation();
    });

});

// Disputes filter close
$('body').click(function () {
    $('.filter').removeClass('m-dropdown--open');
    $('.filter').css({"background-color":"transparent","border-radius":"25px"});
    $('.filter_down_arrow').css("visibility","visible");
    $('.filter_up_arrow').css("visibility","hidden");
});



$( ".cancel-order-dd-list" ).change(function() {
    $('.btn__cancel-order').removeClass('disabled');
    $('.btn__cancel-order').addClass('btn-cancel-order');
    $('.btn-submit-order').removeClass('disabled');
    $('.btn-submit-order').addClass('btn-track-order');
});

$('.btn-submit-order').click(function () {
    $('.cancel_order_tab_content').hide();
    $('.order-review').hide();
    $('.submission-tab').show();
});

$(document).ready(function() {
    $("#cancel-upload").click( function () {
        $('.attatchments').hide();
    });
    $("#cancel-attatchment").click( function () {
       $('.attatchments').hide();
    });
});

document.getElementById('get_file').onclick = function() {
    document.getElementById('my_file').click();
};

document.getElementById('my_file').onchange = function () {
    $('.attatchments').show();
    $('.filename').html(this.files.item(0).name);
    ValidateData();
};

// upload file js
const inpFile=document.getElementById("my_file");
const progressBarFill=document.querySelector("#progressBar > .file-progress-bar");
inpFile.addEventListener("change",uploadFile);
function uploadFile(e) {

    const xhr= new XMLHttpRequest();
    xhr.open("POST","upload.php");
    xhr.upload.addEventListener("progress" , e =>{
        const percent=e.lengthComputable ?(e.loaded / e.total) * 100 : 0 ;
        progressBarFill.style.width =percent.toFixed(2) + "%";
    });
    xhr.send(new FormData(uploadForm));
    setTimeout(function () {
        $('.doc-file').show()
        $('.file-close').css('display','none')
        $(".trash-file").show()
    }, 4000);
}

// checkbox work
if ($('.checkbox_1').is(':checked') == true) {
    $('#reason-dd-1').show();
} else {
    $('#reason-dd-1').hide();
}
if ($('.checkbox_2').is(':checked') == true) {
    $('#reason-dd-2').show();
} else {
    $('#reason-dd-2').hide();
}



$("#all_products").change(function() {
    if(this.checked) {
        $(".checkbox_1").prop("checked", true);
        $(".checkbox_2").prop("checked", true);
        $('#reason-dd-1').show();
        $('#reason-dd-2').show();

    }
    else{
        $(".checkbox_1").prop("checked", false);
        $(".checkbox_2").prop("checked", false);
        $('#reason-dd-1').hide();
        $('#reason-dd-2').hide();
    }
});

$(".checkbox_1").change(function() {
    if(this.checked) {
        $('#reason-dd-1').show();
    }
    else{
        $('#reason-dd-1').hide();
    }
    ValidateData();
});

$(".checkbox_2").change(function() {
    if(this.checked) {
        $('#reason-dd-2').show();
    }
    else{
        $('#reason-dd-2').hide();
    }
    ValidateData();
});

$("#reason-dd-1").change(function() {
    ValidateData();
});

$("#reason-dd-2").change(function() {
    ValidateData();
});

$(".refund-description").change(function() {
    ValidateData();
});

$('.arrow-up').click(function () {
    var inputValue=$('.total_products').val();
    var total = parseInt(inputValue)+1;
    $('.total_products').val(total);
});

$('.arrow-down').click(function () {
    var inputValue=$('.total_products').val();
    var total = parseInt(inputValue)-1;
    $('.total_products').val(total);
});



function ValidateData() {
    $("#reason-dd-1").change(function() {
       var reason=$('#reason-dd-1 :selected').val();
       var refundDetails=$('.refund-description').val();
        if(reason !='' && refundDetails !=''){
            $('.btn-submit-refund').removeClass('disabled');
            $('.btn-submit-refund').addClass('btn-track-order');
        }
    });
    $("#reason-dd-2").change(function() {
        var  reason=$('#reason-dd-2 :selected').val();
        var refundDetails=$('.refund-description').val();
        if(reason !='' && refundDetails !=''){
            $('.btn-submit-refund').removeClass('disabled');
            $('.btn-submit-refund').addClass('btn-track-order');
        }
    });

    $(".refund-description").on("keyup change",function() {
        var  reason=$('#reason-dd-1 :selected').val();
        var  reason1=$('#reason-dd-2 :selected').val();
        var refundDetails=$('.refund-description').val();
        if((reason !='' || reason1!='') && refundDetails !=''){
            $('.btn-submit-refund').removeClass('disabled');
            $('.btn-submit-refund').addClass('btn-track-order');
        }
    });
}



$( "#m_aside_left" ).mouseover(function()
{
    $(this).addClass("btn-primary");
    $(this).removeClass("btn-default hovered");
});

$( "#m_aside_left" ).mouseout(function()
{
    alert('out');
    $(this).removeClass("btn-primary");
    $(this).addClass("btn-default");
});