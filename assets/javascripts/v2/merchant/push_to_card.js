//= require cable

$(document).on('show.bs.modal', '#newP2CModal', function () {
    if($("#location_id").val() === "") {
        $("#Add_new_div_fields").hide();
        $("#balance").html('0.00');
        $(".check_name").prop("disabled", true);
        $("#check_recipient").prop("disabled", true);
        $("#check_amount").val("").prop("disabled", true);
        $(".confirm_btn").prop("disabled", true);
        $(".select_card").prop("disabled", true);
        $("#fee_deduct").text('');
        $("#amount_limit_error").text('');
        $("#check_description").prop("disabled", true);
        $("#banks").prop("disabled", true);
        $("#cardNo").prop("disabled", true);
        $("#exp_date_debit").prop("disabled", true);
        $("#cvv").prop("disabled", true);
        $("#zip_code").prop("disabled", true);
        $("#fee_div").html('');
    }
});

$(document).on('change','#filter',function () {
    data = {}
    $('#filter_form input[type="checkbox"]').each(function(index){
        if($('#filter_form input[type="checkbox"]')[index].checked){
            $('#push_to_card_filter').append("<input type='hidden' name="+$('#filter_form input[type="checkbox"]')[index].name+" value='"+
                     $('#filter_form input[type="checkbox"]')[index].value+"' />");
        }
    })

     $('#push_to_card_filter').submit();
})



$(document).ready(function () {

    $(document).on("click",".ok--btn",function () {
        window.location.href='/v2/merchant/push_to_card'
    });
    $(document).on("click focus keypress","#exp_date_debit",function () {
        $('#exp_date_debit').inputmask('99/99')
    });
    var globalTimeout = null;
    $(document).on("click","#view_detail",function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        // globalTimeout = setTimeout(function() {
        // }, 500);
    });
});

$("body").delegate('.permission-dropdown--menu a','click',function(){

    var value=$(this).attr('data-card');
    $('.permission-dropdown').text(value);
    $('.dropdown-menu--pad').removeClass('show');
    if($(this).attr('data-value')){
        $('#bank_select_error').html('');
    }
});

$("#location_id").on('change', function () {
    on_location_select();
});
$(document).on("click", ".show-dynamic-modal", function (ev) {
    ev.preventDefault();
    var url = $(this).data("url");
    var modal_selector = $(this).data("modal_selector");
    var modal_container_selector = $(this).data("modal_container_selector");
    var method = $(this).data("method");
    $.ajax({
        url: url,
        type: method,
        success: function (data) {
            $(modal_container_selector).empty().html(data);
            $(modal_selector).modal();
            $("#location_id").selectpicker();
            $("#bank_account_name").selectpicker()
        }
    });
    return false;
});
//calculate fee ammount
var total_fee
var total_amount
var card_no_val
$(document).on('keyup','#check_amount', function () {
    $("#fee_div").html('<span id="amount">Amount: $00.00</span> &nbsp;&nbsp; <span id="fee">Fee: $00.00</span> &nbsp;&nbsp; <span class="total style-1" id="total">Total: $00.00</span>');
    $('.message').css('color','red');
    $('.amount_limit_error').css('color','red')
    $('.amount_limit_errors').css('color','red')
    var amount = $(this).val();
    var fee_dollar = 0;
    var fee_perc = 0;
    var balance = $('#balance').text();
    balance = balance.replace(/,/g, "");
    balance = parseFloat(balance);
    var p2c_limit = parseFloat($('#pushlimit').val());
    var p2c_type = $('#pushlimittype').val();
    var p2c_done = 0;

    if ($('#pushtotalcount').val().length > 0) {
        var p2c_done = parseFloat($('#pushtotalcount').val());
    }
    if($('#fee_dollar').val().length > 0 ){
        var fee_dollar = parseFloat($('#fee_dollar').val());
    }
    if ($('#fee_perc').val().length > 0) {
        var fee_perc = parseFloat($('#fee_perc').val());
    }
    if (amount == ""){
        $("#fee_div").html('');
        $('#amount_limit_error').html('');
    } else if (amount == 0) {
        $(this).val('');
    } else {
        var percentage = 0;
        percentage = (parseFloat(amount) * fee_perc)/100;
        total_fee = fee_dollar + percentage;
        total_amount = parseFloat(amount) + total_fee;
        $("#amount").html('Amount: $' + parseFloat(amount).toFixed(2))
        $("#fee").html('Fee: $' + total_fee.toFixed(2));
        $("#total").html('Total:  $' + total_amount.toFixed(2));
        if ((parseFloat(amount)+ parseFloat(total_fee.toFixed(2))).toFixed(2) > balance){
            // $('#fee_deduct').html('');
            $('#amount_limit_errors').html('').text("Insufficient balance!");
            $(".confirm_btn").prop("disabled", true);
        }
        else{
            $('#amount_limit_errors').html('');
            $(".confirm_btn").prop("disabled", false);
        }
       if(p2c_type == "Day"){
            total = parseFloat(amount) + p2c_done;
            if(total > p2c_limit && p2c_limit != 0 ){
                // $('#fee_deduct').html('');
                $('#amount_limit_error').html('').append('<br>'+'Exceeds Daily Limit of $ ' + p2c_limit.toFixed(2))
                $(".confirm_btn").prop("disabled", true);
            }else{
                $('#amount_limit_error').html('');
                $(".confirm_btn").prop("disabled", false);
            }
        }else{
            if(amount > p2c_limit && p2c_limit != 0 ){
                // $('#fee_deduct').html('');
                $('#amount_limit_error').html('').append('<br>'+'Exceeds Transaction Limit of $ ' + p2c_limit.toFixed(2))
                $(".confirm_btn").prop("disabled", true);
            }else{
                $('#amount_limit_error').html('');
                $(".confirm_btn").prop("disabled", false);
            }
        }

    }
});

// $("body").delegate('#add_new_card_check','change click',function(){
//     $('#banks-error').text('');
//     if($(this).prop("checked") == true){
//         $("#Add_new_div_fields").show();
//         // $(".select_card").hide();
//     }
//     else if($(this).prop("checked") == false){
//         $("#Add_new_div_fields").hide();
//         // $(".select_card").show();
//     }
// });
//
// if($("#add_new_card_check").prop("checked") == true){
//     $("#Add_new_div_fields").show();
//     $(".select_card").hide();
// }
// else if($("#add_new_card_check").prop("checked") == false){
//     $("#Add_new_div_fields").hide();
//     $(".select_card").show();
// }

$(document).on('click', '.card_number_image', function () {
    var bank_id = $(this).data("value");
    var card = $(this).data("card");
    var card_no = $(this).find(".p2c_card_dd").text();
    $("#banks").val(bank_id);
    $("#banks").data("card",card);
    $(".select_card").find(".p2c_card_dd").text("").text(card_no);
});

$("body").delegate("#location_id", "change", function () {
    var locations_id = $("#location_id option:selected").val();
    //disabled all
    location_change_func(locations_id);
});
$("body").delegate('#add_new_card_check','click', function () {
    if($(this).prop("checked") == true){
        $(".select_card").prop('disabled', true);
        $("#banks").prop('disabled', true);
        $(".card_holder_before").removeAttr('name');
        $(".card_holder_after").attr('name', 'check[name]');
        $(".card_holder_before").removeClass('check_name_value');
        $(".card_holder_before").removeAttr('required');
        $(".card_holder_after").addClass('check_name_value');
        $('.card_holder_before').attr("disabled", true);
        $('.card_holder_after').attr("disabled", false);
        $(".error_spam_before").removeAttr('id');
        $(".error_spam_after").attr('id','name_error');
        $("#card_dropdown").hide();
        $("#card_holder").hide();
    }
    else if($(this).prop("checked") == false){
        $(".select_card").prop('disabled', false);
        $("#banks").prop('disabled', false);
        $(".card_holder_after").removeAttr('name');
        $(".card_holder_before").attr('name', 'check[name]');
        $(".card_holder_before").addClass('check_name_value');
        $(".card_holder_after").removeClass('check_name_value');
        $('.card_holder_before').attr("disabled", false);
        $('.card_holder_after').attr("disabled", true);
        $(".card_holder_after").removeAttr('required');
        $(".error_spam_before").attr('id','name_error');
        $(".error_spam_after").removeAttr('id');
        $("#card_dropdown").show();
        $("#card_holder").show();
    }
});
$("#cvv").inputmask('Regex', {regex: "^[0-9]+$"});
$(document).on('focusout change keypress', '#location_id, #check_name, #check_recipient, #check_amount,#check_description,#cardNo,#exp_date_debit,#cvv,#zip_code, #banks,#add_new_card_check', function(){
    console.log($(this).val());
    if($(this).is(':valid') && $(this).val() !== ""){
        $(this).css('border-color', '#efefef');
        $(this).prev().css('border-color', '#efefef');
        $(this).closest(".form-group").find("label").css('color', '#575962');
        $(this).closest(".form-group").find(".p_error").html("")

    }else{
        $(this).css('border-color', '#f4516c');
        $(this).prev().css('border-color', '#f4516c');
        //$(this).closest(".form-group").find("label").css('color', '#f4516c');
    }
});
// validateP2CForm("new_p2c"); //valid form sumbo
$("body").delegate(".confirm_btn", "click", function (e) {
    e.preventDefault();
    let location = $("#location_id option:selected").val();
    let rec_email = $("#check_recipient").val()
    let check_name = $("#add_new_card_check").is(':checked') ? $('.card_holder_after').val() : $('#check_name').val()
    let check_amount = $('#check_amount').val() !== "" ? parseFloat($('#check_amount').val()).toFixed(2) : "";
    let check_desc = $("#check_description").val();
    let card_no = $("#cardNo").val();

    let card_valid = ($('#cardNo').attr("data-card_status") == "true")
    let exp = $("#exp_date_debit").val();
    let cvv = $("#cvv").val();
    let zip_code = $("#zip_code").val();

    let bal = $("#balance").text();

    //populate data for confirm modal

    if($("#add_new_card_check").length == 0 || ($('#banks').is(':disabled') && $("#add_new_card_check").is(':checked') ) ){
        $("#bank_account").html("•••• " + card_no.slice(card_no.length - 4));
        $("#zip_div").show()
        $("#trans_to1").html("").html(zip_code)
    }else{
        card_no = $("#banks").data("card")
        $("#bank_account").html("•••• " + card_no.slice(card_no.length - 4));
        $("#zip_div").hide()
    }

    $("#account_name").html($("#location_id option:selected").html())
    $("#email_add").html(rec_email)
    $("#name_onc").html(check_name)
    $("#available_balance").html("$"+$("#balance").text())

    card_no = card_no.replace(/\s/g, '');
    $("#cardNoSafe").val($("#cardNoSafe").val().replace(/\s/g, ''));

    $("#description_memo").html(check_desc)
    $("#trans_amount").html("$"+check_amount)
    $("#trans_fee").html("$"+parseFloat(total_fee).toFixed(2) )
    $("#total_amount").html("$"+  parseFloat(total_amount).toFixed(2) )
    $("#remain_balance").html("$"+  parseFloat( (bal.replace(/\,/g,'') - total_amount)).toFixed(2))

    if(validateEmail(rec_email) &&  location !== "" && rec_email !== "" && check_name !== "" && check_amount !== "" && check_desc !== "" && $("#amount_limit_error").text() == "" && $("#amount_limit_errors").text() == "") {
        if ($("#add_new_card_check").length == 0 ||  ($('#banks').is(':disabled') && $("#add_new_card_check").is(':checked')) ) {
            if (card_no !== "" && exp !== "" && cvv !== "" && zip_code !== "" && card_valid && verify_card_expiry($("#exp_date_debit").val()) && parseInt($("#cardNo").val().replace(/ /g, '')).toString().length >=  13) {
                $('#ConfimP2cModal').modal("show")
                $('#newP2CModal').modal("hide")
            }
            else {
                if (card_no === ""){ $("#card_error").html("Please Enter Debit Card Number");}
                if ($('#cardNo').attr("data-card_status") == "false" && card_no != ""){ $("#card_error").text("").text("For Debit Card Use Only.");}
                if (parseInt($("#cardNo").val().replace(/ /g, '')).toString().length < 13 && card_no !== ""){ $("#card_error").text("").text("Please enter at least 13 characters.");}
                if (exp === ""){ $("#exp_error").html("Please Enter Expiry Date");}
                if (cvv === ""){ $("#cvv_error").html("Please Enter CVC Code");}
                if (zip_code === ""){ $("#zip_error").html("Please Enter Zip Code");}
            }
        }
        else{
            if($("#banks").val() === ""){
                $("#bank_select_error").html("Plese Select a Card");
            }else {
                $('#ConfimP2cModal').modal("show")
                $('#newP2CModal').modal("hide")
            }
        }
    }else{
        if ( $("#add_new_card_check").length == 0 ||  ($('#banks').is(':disabled') && $("#add_new_card_check").is(':checked')))  {

            if (card_no === ""){ $("#card_error").html("Please Enter Debit Card Number");}
            if (!card_valid && card_no != ""){ $("#card_error").html("For Debit Card Use Only.");}
            if (exp === ""){ $("#exp_error").html("Please Enter Expiry Date");}
            if (cvv === ""){ $("#cvv_error").html("Please Enter CVC Code");}
            if (zip_code === ""){ $("#zip_error").html("Please Enter Zip Code");}
        }
        if ($("#banks").val() === ""){ $("#bank_select_error").html("Plese Select a Card");}
        if (location === ""){ $("#account_error").html("Please Select an Account");}
        if (!validateEmail(rec_email)){ $("#e-mail_error").html("Please enter Email");}
        if (check_name === ""){ $("#name_error").html("Please enter Name");}
        if (check_amount === ""){ $("#amount_error").html("Please Enter Amount");}
        if (check_desc === ""){ $("#desc_error").html("Please Enter Memo");}
    }
});
var globalTimeout = null;

$(document).on('change','#filter_form input[type="checkbox"]',function () {
    $('#filter_form input[type="checkbox"]').attr("disabled", true);
    if($(this).attr("id") == "search_all"){
        if( $(this).is(":checked")) {
            $('#filter_form input[type="checkbox"]').prop("checked", false)
            $("#filter_form input[type='checkbox']").prop("checked", true)
        }else{
            $(this).prop("checked", false)
            $('#filter_form input[type="checkbox"]').prop("checked", false)
        }
    }else{
        $('#filter_form input[type="checkbox"]#search_all').prop("checked",false)
    }
    if($("#filter_form input:checkbox:not(:checked)").length == 1)// when all is check except all then all checkbox will automatically checked
    {
        $("#search_all").prop("checked",true)
    }
    if($('#filter_form input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
    {
        $("#filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    }
    search_call("status");
});
if (window.location.href.indexOf("search") < 1) {
    $("#filter_form input[type='checkbox']").prop("checked", true)
}

$(document).on('keyup','.search_input_datatable',function (e) {
    e.preventDefault();

        globalTimeout = setTimeout(function () {
            search_call("search");
        }, 2500);

});
var mutex = true
function search_call(type){
    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    if(mutex) {
            mutex = false;
            var search_value = $('.search_input_datatable').val();
            search_value = search_value.trim()
            data = {"q[data_search]": search_value, filter: $("#filter option:selected").val(), search_form: 'true'};
            $('#filter_form input[type="checkbox"]').each(function (index) {
                if ($('#filter_form input[type="checkbox"]')[index].checked) {
                    data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
                }
            })
            $.ajax({
                url: '/v2/merchant/push_to_card',
                type: 'GET',
                data: data,
                success: function (data) {
                    mutex = true
                    $('.table-data').html('').html(data);
                    $('.search_input_datatable').val(search_value);
                    $('.search_input_datatable').focus();
                    $("#m_table_1").DataTable();
                    $("#page").selectpicker()
                    $("#total_pending").html("").html("$" + $(".total_pending").text());
                    $("#total_void").html("").html("$" + $(".total_void").text());
                    $("#total_failed").html("").html("$" + $(".total_failed").text());
                    $("#total_paid").html("").html("$" + $(".total_paid").text());
                    $("#total_paid_wire").html("").html("$" + $(".total_paid_wire").text());
                    if (type == "status") {
                        $(".btn-status").trigger('click');
                        $('#filter_form input[type="checkbox"]').removeAttr("disabled");
                    }
                }, error: function (data) {
                    mutex = true
                }
            });
    }
}

$("body").delegate("#confirm_p2c", "click", function () {
    $(".check_form")[0].submit();
});
$("body").delegate("#cardNo", "keyup", function () {
    $("#cardNo").inputmask("9999 9999 9999 9999")

    var card_number = $('#cardNo').val().replace(/\s/g,'');
    var card_length = parseInt(card_number)
    card_no_val = card_number
    if (card_length.toString().length >= 13 ) {
        verify_card_number(card_number,"not_present")
        $('.confirm_btn').attr('disabled', false);
    } else {
        $("#errorss").text('');
        $("#errorss_new").text('');
    }
});
function verify_card_number(val,present){
    $('#errorss_new').css('color','red');
    var error_span =  present ? "#card_error" : "#errorss_new1";
    if(val != null ) {
        $.ajax({
            url: "/v2/merchant/push_to_card/present_card_verification",
            method: 'get',
            dataType: 'json',
            data: {card_number: val,present: present},
            success: function (result) {
                console.log(result)
                $("#banks").prop('required',false);
                var type_scheme = result.success;
                $("#result").val(type_scheme);
                if (type_scheme === "false") {
                    console.log("false");
                    $(error_span).text('').text('For Debit Card Use Only.');
                    $('#cardNo').attr("data-card_status",false);
                }else{
                    img = type_scheme == "mastercard" ? "/assets/v2/merchant/icons/master-card-logo-png-7-becb1dcd631a652e34b1fadc7080e01286649e1a6f288a0697a9e33be476117b.jpg" : "/assets/v2/merchant/icons/visa-1af6ed6a313b7e1f02b9f9892d0dc68f864cd2f7e8c783a384a00f0150ff4455.svg"
                    $("#c_logo").attr("src",img);
                    $(error_span).text('');
                    // "type"=>"debit", "scheme"=>"visa" ->  4000056655665556 or "scheme"=>"mastercard" -> 5200828282828210
                    $('#cardNo').attr("data-card_status",true);
                    $("#cardNoSafe").val(card_no_val);
                }
            }
        });
    }else{
        $("#errorss").text('');
        $("#errorss_new").text('');
        $('.confirm_btn').attr('disabled', false);
    }
}

$(".modal").on("hidden.bs.modal", function () {
    if($('.modal.show').length)
    {
        setTimeout(function(){
            $('#body--aa').addClass('modal-open');
        }, 50);
    }
});
$("body").delegate(".two_decimal", "input", function () {
    this.value = this.value.match(/\d{0,}(\.\d{0,2})?/)[0];
});
$("body").delegate(".currency_format", "blur", function () {
    if($(this).val()!== "") {
        this.value = parseFloat(this.value).toFixed(2)
    }
});

$("body").delegate('.ranges:eq(1) ul li','click', function () {
    var startdate = '';
    var enddate = '';
    if($(this).text() == "Yesterday"){
        startdate = moment().subtract(1, "days")._d;
        enddate = moment().subtract(1, "days")._d;
    } else if($(this).text() == "Today") {
        startdate = moment()._d;
        enddate = moment()._d;
    } else if($(this).text() == "Last 6 Days") {
        startdate = moment().subtract(5, "days")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Month to Date") {
        startdate = moment().startOf("month")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Previous month") {
        //startdate = moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")._d;
        //enddate = moment()._d;
        startdate =  new moment().subtract(1, 'months').date(1)._d
        enddate =  moment().subtract(1,'months').endOf('month')._d;
    }
    if($(this).text() != "Custom Range"){
        $.ajax({
            url: '/v2/merchant/push_to_card',
            type: 'GET',
            data: {first_date: startdate, second_date: enddate, wallet_id: $("#hidden_wallet").val()},
            success: function(data) {
                $('.table-data').html(data);
                $("#m_table_1").DataTable();
                $("#page").selectpicker();
                $("#total_pending").html("").html("$"+$(".total_pending").text());
                $("#total_void").html("").html("$"+$(".total_void").text());
                $("#total_failed").html("").html("$"+$(".total_failed").text());
                $("#total_paid").html("").html("$"+$(".total_paid").text());
                $("#total_paid_wire").html("").html("$"+$(".total_paid_wire").text());
            }
        });
    }
});
$("body").delegate('.applyBtn:eq(1)','click', function () {
    var startdate = $("input[name=daterangepicker_start]").last().val();
    var enddate = $("input[name=daterangepicker_end]").last().val();
    $.ajax({
        url: '/v2/merchant/push_to_card',
        type: 'GET',
        data: {first_date: moment(startdate)._d, second_date: moment(enddate)._d, wallet_id: $("#hidden_wallet").val(), custom: true},
        success: function(data) {
            $('.table-data').html(data);
            $("#m_table_1").DataTable();
            $("#page").selectpicker();
            $("#total_pending").html("").html("$"+$(".total_pending").text());
            $("#total_void").html("").html("$"+$(".total_void").text());
            $("#total_failed").html("").html("$"+$(".total_failed").text());
            $("#total_paid").html("").html("$"+$(".total_paid").text());
        }
    });
});
$("body").delegate('.only_numbers','input', function() {
    this.value = this.value.match(/\d*/)[0];
});
$("body").delegate('.only_letters','input', function() {
    this.value = this.value.match(/[a-zA-Z\s]*/);
});
$("body").delegate('.secure','copy paste cut',function(e) {
    e.preventDefault(); //disable cut,copy,paste
});
function validateEmail($email) {
    var emailReg = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return emailReg.test( $email );
}
$(document).on("click","#yes_void",function(){
    id = $("#void_modal").data("void_id")
    $.ajax({
        url: '/v2/merchant/checks/'+id,
        data: {status: "VOID"},
        method: "PATCH"
    })
});
// multiple modal fix
$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});
//........................................debit_card_form.html........................
$("body").delegate('.trash_icon','click',function(e) {
    e.preventDefault();
    var card_no = $(this).data("card_id");
    var card = $(this)

    $.ajax({
        url: "push_to_card/delete_debit_card",
        method: 'get',
        dataType: 'json',
        data: {card_number: card_no},
        success: function (result) {
            if(result.success == "true") {

                card.closest("li").fadeOut(300, function(){ $(this).remove();});
                $("#banks").val("");
                $("#default").click()
            }else{
                //$("#bank_select_error").html("Card could not be deleted.");
            }
        }
    });

});
$(document).on("focusout", "#exp_date_debit", function () {
    var expiry = $(this).val();
    var expiry_error = $("#exp_date_debit-error").text();
    if(expiry != ""){
        verify_card_expiry(expiry);
    }
    if(expiry != ""){
        $("#expiry_error").text('');
    }
});
$(document).on("keyup", "#exp_date_debit", function () {
    var expiry_error = $("#exp_date_debit-error").text();
    if(expiry_error != ""){
        $("#exp_error").text('');
    }
});

function verify_card_expiry(expiry){
    var expiry_date = expiry.split("/");
    var month = expiry_date[0];
    var year = expiry_date[1];
    var d = new Date();
    var current_month = d.getMonth() + 1;
    var current_year = d.getFullYear().toString().substr(-2);
    if (year > current_year && month <= 12 && month > 0) {
        $("#exp_error").text('');
        return true;
    } else if(month >= current_month && month <= 12 && year == current_year && month > 0){
        $("#exp_error").text('');
        return true;
    }else{
        $("#exp_error").text('').text('Invalid Expiry Date.');
        return false;
    }
}

$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<img class="img-fluid" src="'+$(this).data('img') + '" />';
        },
    })
});

$(document).on('keyup','.less_than_zero', function () {
    var amount_val = $(this).val();
    if( amount_val < 1 ) {
        $('#amount_limit_errors').html('').text("Amount must be greater than 1");
        $(".confirm_btn").prop("disabled", true);
    }
    if(amount_val == ''){
        $('#amount_limit_errors').html('').text('');
        $(".confirm_btn").prop("disabled", true);
    }
});

$(document).on("shown.bs.modal", "#newP2CModal", function(){
    var loc_count = $("#location_id").data("loc-count")
    if(loc_count == 1 ){
        var loc_id = $("#location_id").val();
        on_location_select()
        location_change_func(loc_id)
        $("#location_id").siblings('.dropdown-menu').attr("style", "display: none !important");
    }
})

function location_change_func(locations_id){
    $("#check_description").prop("disabled", true);
    $("#check_recipient").prop("disabled", true);
    $(".check_name").prop("disabled", true);
    $("#banks").prop("disabled", true);
    $("#check_amount").prop("disabled", true);
    $(".select_card").prop("disabled", true);
    $("#cardNo").prop("disabled", true);
    $("#exp_date_debit").prop("disabled", true);
    $("#cvv").prop("disabled", true);
    $("#zip_code").prop("disabled", true);
    $("#check_amount").val("");
    $("#fee_div").html('');
    $('#amount_limit_error').html('');
    $('#amount_limit_errors').html('');
    // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Retrieving Balance Information</strong></div>');
    $(".alert-body").css("display","block");
    $(".account-information").text("Please wait, getting account information.")
    if (locations_id != "") {
        $.ajax({
            url: "/v2/merchant/push_to_card/location_data",
            type: "GET",
            data: {location_id: locations_id, type: "push_to_card"},
            success: function (data) {
                $("#balance").text(data["balance"])
                $('#fee_dollar').val(data["fee_dollar"])
                $('#fee_perc').val(data["fee_perc"])
                $('#pushlimit').val(data["p2c_card_limit"])
                $('#pushlimittype').val(data["p2c_card_limit_type"])
                $('#pushtotalcount').val(data["p2c_total_count"])
                if (data["banks"] == null && data["merchant_user"] == true) {
                    $("#bank_account_name").html('<option value="" >No Bank Accounts</option>');
                } else if (data["banks"] != null && data["banks"].length == 1) {
                    $("#bank_account_name").attr("disabled", "disabled");
                    $("#bank_account_name").html("<option value= " + data["banks"][0].id + " > " + data["d_account"] + "</option>");
                    $("#bank_account_name").after("<input type='hidden' name='bank_account_name' value=' " + data["banks"][0].id + "'>");
                } else {
                    if (data["merchant_user"] == false) {
                        if ($("#location_id").val() === "") {
                            $("#bank_account_name").html('')
                            $("#bank_account_name").append('<option value="">Bank</option>')
                        } else {
                            if (data["system_fee"] == true) {
                                $("#bank_account_name").html('').append("<option value= " + data["wallet_id"] + " > " + data["d_account"] + "</option>");
                            }
                        }
                    } else {
                        $("#bank_account_name").removeAttr("disabled");
                        $("#bank_account_name").html('<option value="" >Select Bank Account</option>');
                        if (data["multiple_bank_details"] != null) {
                            for (i = 0; i < data["multiple_bank_details"].length; i++) {
                                // text += cars[i] + "<br>";\
                                $("#bank_account_name").append("<option value= " + data["multiple_bank_details"][i].id + " > " + data["multiple_bank_details"][i].account + "</option>");
                            }
                        }
                    }
                }

                $("#bank_account_name").selectpicker('refresh')
                //NO NEED FOR THAT WE SET THESE FIELDS ALREADY
                // // var fee_dollar = '<%= @fee_dollar%>'
                // // var fee_perc = '<%= @fee_perc %>'
                // $('#fee_dollar').val(fee_dollar)
                // $('#fee_perc').val(fee_perc)

                if (data["location"] != null) {
                    if (data["location"].is_block == true) {
                        $("#amount").prop("disabled", true);
                        $("#description").prop("disabled", true);
                        //$("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Location is Blocked</strong></div>');
                        $(".alert-body").css("display","block");
                        $(".account-information").text("Location is Blocked.")
                        $("#submit_button").attr('disabled', 'disabled');
                    } else if (data["location"].push_to_card == true) {
                        $("#amount").prop("disabled", true);
                        $("#description").prop("disabled", true);
                        // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Push to Card is blocked for this Location</strong></div>');
                        $(".alert-body").css("display","block");
                        $(".account-information").text("Push to Card is blocked for this Location.")
                        $("#submit_button").attr('disabled', 'disabled');
                    } else {
                        $(".check_name").prop("disabled", false);
                        $("#check_recipient").prop("disabled", false);
                        $("#check_amount").prop("disabled", false);
                        $("#check_description").prop("disabled", false);
                        $("#zip_code").prop("disabled", false);
                        $(".confirm_btn").prop("disabled", false);
                        $(".select_card").prop("disabled", false);
                        $("#cardNo").prop("disabled", false);
                        $("#exp_date_debit").prop("disabled", false);
                        $("#cvv").prop("disabled", false);
                        $("#zip_code").prop("disabled", false);
                        $('.card_holder_after').attr("disabled", true);
                        // $("#errormessage").css('display','none').html('');
                        $(".alert-body").css("display","none");
                        if(!$("#add_new_card_check").is(':checked')){
                            $("#banks").prop("disabled", false);
                        }
                        //NO  NEED FOR THIS BECAUSE WE ALREADY SET THESE FIELDS
                        // if (data['location'].push_to_card_limit_type == "Day"){
                        //     var push_to_card_limit = '<%= @push_to_card_limit%>'
                        //     var push_to_card_limit_type = '<%= @push_to_card_limit_type %>'
                        //     var total_count = '<%= @total_count %>'
                        //     $('#pushlimit').val(push_to_card_limit)
                        //     $('#pushtotalcount').val(total_count)
                        //     $('#pushlimittype').val(push_to_card_limit_type)
                        // }else {
                        //     var push_to_card_limit = '<%= @push_to_card_limit%>'
                        //     var push_to_card_limit_type = '<%= @push_to_card_limit_type %>'
                        //     $('#pushlimit').val(push_to_card_limit)
                        //     $('#pushlimittype').val(push_to_card_limit_type)
                        // }
                    }
                }else if (data['user'].push_to_card){
                    $(".check_name").prop("disabled", true);
                    $("#check_recipient").prop("disabled", true);
                    $("#check_amount").prop("disabled", true);
                    $("#check_description").prop("disabled", true);
                    $("#banks").prop("disabled", true);
                    $("#zip_code").prop("disabled", true);
                    $(".confirm_btn").prop("disabled", true);
                    // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Push to Card is blocked for this User</strong></div>');
                    $(".alert-body").css("display","block");
                    $(".account-information").text("Push to Card is blocked for this User.")
                }else{
                    $(".check_name").prop("disabled", false);
                    $("#check_recipient").prop("disabled", false);
                    $("#check_amount").prop("disabled", false);
                    $("#check_description").prop("disabled", false);
                    $("#zip_code").prop("disabled", false);
                    $(".confirm_btn").prop("disabled", false);
                    // $("#errormessage").css('display','none').html('');
                    $(".alert-body").css("display","none");
                    if(!$("#add_new_card_check").is(':checked')){
                        $("#banks").prop("disabled", false);
                    }
                    //NO  NEED FOR THIS BECAUSE WE ALREADY SET THESE FIELDS
                    // if (data['user'].system_fee.push_to_card_limit_type != undefined && user.system_fee.push_to_card_limit_type == "Day" ){
                    //     var push_to_card_limit = '<%= @push_to_card_limit%>'
                    //     var push_to_card_limit_type = '<%= @push_to_card_limit_type %>'
                    //     var total_count = '<%= @total_count %>'
                    //     $('#pushlimit').val(push_to_card_limit)
                    //     $('#pushtotalcount').val(total_count)
                    //     $('#pushlimittype').val(push_to_card_limit_type)
                    //     }else{
                    //         var push_to_card_limit = '<%= @push_to_card_limit%>'
                    //         var push_to_card_limit_type = '<%= @push_to_card_limit_type %>'
                    //         $('#pushlimit').val(push_to_card_limit)
                    //         $('#pushlimittype').val(push_to_card_limit_type)
                    //     }
                }
            },
            error: function (data) {
                $("#check_description").prop("disabled", false);
                $("#check_recipient").prop("disabled", false);
                $(".check_name").prop("disabled", false);
                $("#banks").prop("disabled", false);
                $("#check_amount").prop("disabled", false);
            }

        });
    } else {
        var merchant_user = $("#merchant_user").val();
        if (merchant_user == "true") {
            $('#bank_account_name').val($('#location_id').val());
        }
        $("#balance").html("0.00")
        // $("#errormessage").css('display', 'none').html('');
        $(".alert-body").css("display","none");
        $("#amount").prop("disabled", true);
        $("#amount").val('');
        // $("#fee_deduct").text('');
        $("span#amount").html("$0.00");
        $("span#fee_amount").html("$0.00");
        $("span#total_amount").html("$0.00");
        $('.amount_limit_error').html('');
        $("#description").prop("disabled", true);
        $("#description").html('');
        $("#submit_button").prop("disabled", true);
        $("#bank_account_name").prop("disabled", true);
        if ($("#location_id").children(":selected").val() != 0) {
            // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Retrieving Balance Information</strong></div>');
            $(".alert-body").css("display","block");
            $(".account-information").text("Please wait, getting account information.")
        }
    }
}

function on_location_select(){
    $("#balance").html('0.00');
    $(".check_name").prop("disabled", true);
    $("#check_recipient").prop("disabled", true);
    $("#check_amount").val("").prop("disabled", true);
    $(".confirm_btn").prop("disabled", true);
    $("#fee_div").html('');
    $("#amount_limit_error").text('');
    $("#check_description").prop("disabled", true);
    $("#banks").prop("disabled", true);
    $("#zip_code").prop("disabled", true);
    $(".confirm_btn").prop("disabled", true);
    // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Retrieving Balance Information</strong></div>');
    $(".alert-body").css("display","block");
    $(".account-information").text("Please wait, getting account information.")
    $("#Add_new_div_fields").hide();
}
