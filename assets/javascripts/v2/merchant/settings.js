//= require intlTelInput
$(document).ready(function () {
    var globalTimeout = null;
    $(document).on('keyup','.search_input_datatable',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
            globalTimeout = setTimeout(function () {
                globalTimeout = null;
                var search_value = $('.search_input_datatable').val();
                search_value = search_value.trim()
                var filter_data = "";
                if (filter != "") {
                    filter_data = filter;
                } else {
                    filter_data = 10;
                }
                $.ajax({
                    url: '/v2/merchant/settings',
                    type: 'GET',
                    data: {q: search_value, filter: 10, search: 'true'},
                    success: function (data) {
                        $('#employee_list').html('').html(data);
                        $("#m_table_1").DataTable();
                        $("#page").selectpicker();
                        $("#filter").selectpicker();


                        $('.search_input_datatable').val(search_value);
                        $('.search_input_datatable').focus();
                    }, error: function (data) {


                    }
                });

            }, 2500);
    });
    $(document).on("click", ".show-dynamic-modal", function (ev) {
        // ev.preventDefault();
        $('.show-dynamic-modal').prop('disabled', true)
        var url = $(this).data("url");
        var modal_selector = $(this).data("modal_selector");
        var modal_container_selector = $(this).data("modal_container_selector");
        var method = $(this).data("method");
        $.ajax({
            url: url,
            type: method,
            success: function (data) {
                $('.show-dynamic-modal').prop('disabled', false)
                $(modal_container_selector).empty().html(data);
                $(modal_selector).modal();
                $('.permission_dropdown').selectpicker();
                $('#wallets').selectpicker();
                // var BootstrapSelect={init:function(){$(".m_selectpicker").selectpicker()}};jQuery(document).ready(function(){BootstrapSelect.init()});
                $("#phone_number").intlTelInput({
                    formatOnInit: true,
                    separateDialCode: true,
                    customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                        country=selectedCountryPlaceholder;
                        country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
                        $('#user_phone_code').val(Object.values(selectedCountryData)[2]);
                        if(selectedCountryData.iso2 == 'us') {
                            country = "555-555-5555"
                        }
                        return  country;
                    },
                    excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
                    // utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
                });
            }
        });
        return false;
    });


    $('#wallets').multiselect({
        texts: {
            placeholder    : 'Select Location(s)', // text to use in dummy input
            search         : 'Search Location(s)',         // search input placeholder text
            // selectedOptions: ' selected',      // selected suffix text
            selectAll      : 'Select All Location(s)',     // select all text
            unselectAll    : 'Unselect All Location(s)',   // unselect all text
            // noneSelected   : 'None Selected'   // None selected text
        },
        search             : false,
        selectAll          : true, // add select all option
        minHeight          : 200,   // minimum height of option overlay
        maxHeight          : 300,  // maximum height of option overlay
        maxWidth           : 212,  // maximum width of option overlay (or selector)
        maxPlaceholderOpts : 2,    // maximum number of placeholder options to show until "# selected" shown instead
        checkboxAutoFit    : true,  // auto calc checkbox padding
        onOptionClick      : function( element, option ){
            $('#wallets-error').text('');
        },
    });

    $("body").delegate('li#select_all','click',function() {
        if($(this).prop("checked") == true){
            $("input[type=checkbox]").prop("checked", true);
        }
        else if($(this).prop("checked") == false){
            $("input[type=checkbox]").prop("checked", false);
        }
    });
    $(".add_new_employee_form").validate({
        rules: {
            "user[name]": {
                required: true,
                minlength: 3
            },
            "user[last_name]": {
                required: true,
                minlength: 3
            },
            "user[email]": {
                required: true,
                email: true
            },
            "user[password]": {
                //required: true,
                maxlength: 20,
                minlength: 6
            },
            "user[password_confirmation]": {
                //required: true,
                equalTo: "#user_password"
            },
            "user[phone_number]": {
                required: true,
                // number: true,
                minlength: 6,
                maxlength: 15
            },
            "user[wallets][]": {
                required: function () {
                    if ($('#wallets').find('option:selected').length == 0)
                    {
                        return true;
                    }
                    return false
                }
            },
            "user[permission_id]": {
                required: function () {
                    if ($('#permission').find('option:selected').length == 0)
                    {
                        return true;
                    }
                    return false
                }
            },
        },
        messages: {
            "user[name]":{
                required: "First Name is required.",
                minlength: "Name must have 3 characters"
            },
            "user[last_name]":{
                required: "First Name is required.",
                minlength: "Name must have 3 characters"
            },
            "user[email]":{
                required: "Email is required",
                email: "Please enter a valid email address"
            },
            password: {
                required: "Password is required",
                minlength: "Password 6 characters long"
            },
            password_confirmation: {
                required: "Password confirmation is required",
                equalTo: "Password not Matched"
            },
            "user[phone_number]": {
                required: "Phone Number is required",
                // number: "Valid Number",
                minlength: "Phone Number must have 6 digits",
                maxlength: "Phone Number Only have 15 digits"
            },
            "user[wallets][]": {
                required: "Please Select Atleast One Location",
            },
            "user[permission_id]": {
                required: "Please Select Permission",
            },
        },
        ignore: ':hidden:not("#wallets,#permission")',
        submitHandler: function(form) { // for demo
            $('.spnr').fadeIn();
            return true;
        }
    });

    $(document).on("click", ".delete_employee", function (ev) {
        $('#submerchant_id').val( $(this).data('id'))
    });
    $(".modal").on("hidden.bs.modal", function () {
        if($('.modal.show').length)
        {
            setTimeout(function(){
                $('#body--aa').addClass('modal-open');
            }, 50);
        }
    });
});