$(document).on("click","#revealed_key",function(){
	var id = $(this).data("id")
	$.ajax({
		url: "/v2/merchant/update_revealed/"+id,
		method: "POST",
		success: function(data){
			reveal_user = data["revealed_by"];
			reveal_date = data["revealed_date"];
			if(data["local_time_zone"]==true){
				var gmtDateTime = moment.utc(reveal_date, 'MM-DD-YYYY HH:mm:ss A');
    			reveal_date = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
			}
			$(".cutom_tootltip").attr("data-original-title" , "Revealed by: "+reveal_user+" . "+ reveal_date)
		}
	});
});


$(document).ready(function(){
	var tooltip_message = $(".cutom_tootltip").attr("title")
	if($("#reveal_date").val() == null){
		$(".cutom_tootltip").attr("title" , tooltip_message + $("#reveal_timezone_date").val())
	}else{
		var gmtDateTime = moment.utc($("#reveal_date").val(), 'MM-DD-YYYY HH:mm:ss A');
		reveal_date = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
		$(".cutom_tootltip").attr("title" , tooltip_message + reveal_date)
	}
});
