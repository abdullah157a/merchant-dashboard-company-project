
// require jquery_ujs
//= require v2/merchant/bootstrap-select
//= require v2/merchant/bootstrap-datepicker
$(document).on('change','#permission_filter',function () {
    $('#permission_filter').submit();
})

setTimeout(function() {
    $('.profile-subheader').css("display","none");
}, 20000);

$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});

$("body").delegate(".permission-form input[type='checkbox']",'change',function() {
    $("#error_message").css("display","none");
});

$("body").delegate('#create_permission_btn','click',function() {
    if($(".permission-form").valid() && $(".permission-form").find("input[type='checkbox']:checked").length == 0){
        $("#error_message").css("display","block");
        return false
    }
    else{
        if($('#permission_name').val() != "") {
            $('#create_permission_btn').attr('disabled', true);
            // $('#create_permission_btn').html("").append("Create Permission &nbsp;<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
        }
        // $(".permission-form").submit();
    }
    if($(".permission-form").valid()){
        $('#createuserpermissionModal').modal("hide");
        $('#confirmuserpermissionModal').modal({ show: true });
    }
});

$("body").delegate("#permission_delete","click",function(){
    id = $(this).data("id")
    $("a.delete_permission").attr("href","/v2/merchant/permission/"+id)

    $("#deleteuserpermissionModal").modal("show")

});

$("body").delegate("#confirm_permission","click",function(e){
    e.preventDefault()
    $(".permission-form").submit();
});

// $(".show-dynamic-modal").click(function (ev) {
$(document).on("click", ".show-dynamic-modal", function (ev) {
    $('.add-new-permission').attr("disabled", true);
    $('.edit-permission').attr("disabled", true);
    $('.show-permission').attr("disabled", true);
    ev.preventDefault();
    var url = $(this).data("url");
    var modal_selector = $(this).data("modal_selector");
    var modal_container_selector = $(this).data("modal_container_selector");
    var method = $(this).data("method");
    $.ajax({
        url: url,
        type: method,
        success: function (data) {
            $(modal_container_selector).empty().html(data);
            $(modal_selector).modal();
            $('.add-new-permission').attr("disabled", false);
            $('.edit-permission').attr("disabled", false);
            $('.show-permission').attr("disabled", false);
        }
    });
    return false;
});

$(document).on('keyup change','#permission_name',function () {
    var value = $(this).val()
    if(value != ""){
        $('#create_permission_btn').prop('disabled',false)
        $('#create_permission_btn').addClass('create_permission_loader');
    }
});

$(document).on('click','.for_select, .select_all_permissions',function () {
    var value = $(this).prop('checked');
    var checks = false
    if(value){
        $('#create_permission_btn').prop('disabled',false)
        $('#create_permission_btn').addClass('create_permission_loader');
    }else{
        $.each($('.for_select'), function(){
            var checked = []
            checked = $(this).prop('checked');
            if (checked){
                checks = checked
                return checked
            }
        });
        if(checks) {
            $('#create_permission_btn').attr('disabled', false)
        }else {
            $('#create_permission_btn').attr('disabled', true)
        }
    }
});

$(document).on('click','.child_box',function () {
    var checked =[]
    var type = $(this).data('class');
    if($(this).prop('checked') == false){
        $('.'+type+'_parent_check_box').prop('checked',false)
    }else {
        $.each($('.'+type+'_child_check_box'), function(){
            checked = $(this).prop('checked');
            if (!checked){
                return checked
            }
        });
        if(checked) {
            $('.'+type+'_parent_check_box').prop('checked', true)
        }else {
            $('.'+type+'_parent_check_box').prop('checked', false)
        }
    }
    for_select_check()
});

$(document).on('click','.parent_box',function () {
    var type = $(this).attr('id');
    if($('.'+type+'_parent_check_box').prop('checked') == true){
        $('.'+type+'_child_check_box').prop('checked',true)
    }else{
        $('.'+type+'_child_check_box').prop('checked',false)
    }
    for_select_check()
});

$(document).on('click','.sub_parent',function () {
    var type = $(this).data('id');
    var parent_type = $(this).data('class');
    if($('.'+type+'_parent_box').prop('checked') == true){
        $('.'+type+'_child_box').prop('checked',true)
    }else{
        $('.'+type+'_child_box').prop('checked',false)
    }
    parent_checkbox_checked(parent_type);
    for_select_check()
});

$(document).on('click','.sub_child',function () {
    var checked =[]
    var type = $(this).data('id');
    var parent_type = $(this).data('class');
    if($(this).prop('checked') == false){
        $('.'+type+'_parent_box').prop('checked',false)
    }else {
        $.each($('.'+type+'_child_box'), function(){
            checked = $(this).prop('checked');
            if (!checked){
                return checked
            }
        });
        if(checked) {
            $('.'+type+'_parent_box').prop('checked', true)
        }else {
            $('.'+type+'_parent_box').prop('checked', false)
        }
        parent_checkbox_checked(parent_type);
        for_select_check()
    }
});

$(document).on('keyup change','#permission_name',function () {
    var value = $(this).val()
    if(value != ""){
        $('#create_permission_btn').prop('disabled',false)
    }
});

var globalTimeout = null;

$(document).on('keyup','.search_input_datatable',function (e) {
    e.preventDefault();

    globalTimeout = setTimeout(function () {
        search_call();
    }, 2500);

});

function for_select_check() {
    var checked = []
    $.each($('.for_select'), function(){
        checked = $(this).prop('checked');
        if (!checked){
            return checked
        }
    });
    if(checked) {
        $('.select_all_permissions').prop('checked', true)
    }else {
        $('.select_all_permissions').prop('checked', false)
    }
}

function parent_checkbox_checked(type) {
    var checked = []
    $.each($('.'+type+'_child_check_box'), function(){
        checked = $(this).prop('checked');
        if (!checked){
            return checked
        }
    });
    if(checked) {
        $('.'+type+'_parent_check_box').prop('checked', true)
    }else {
        $('.'+type+'_parent_check_box').prop('checked', false)
    }
}

function search_call(){
    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    globalTimeout = setTimeout(function() {
        globalTimeout = null;
        var search_value = $('.search_input_datatable').val();
        search_value = search_value.trim()
        data = {"q[name_cont]": search_value,filter: 10,search_form:'true'};
        $.ajax({
            url: '/v2/merchant/permission',
            type: 'GET',
            data: data,
            success: function (data) {
                $('.table-data').html(data);
                $('.search_input_datatable').val(search_value);
                $('.search_input_datatable').focus();
                $("#m_table_1").DataTable();
                $("#page").selectpicker()
            }, error: function (data) {
            }
        });
    }, 500);
}

window.onload  = $('.timeUtc').each(function (i, v) {
    var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss A');
    var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
    v.innerText = local
});

$(document).ready(function(){
    $('.for_show').attr("disabled", true);
});

$("body").delegate('#update_permission','click',function() {
    if ($(".permission-form").valid()){
        $( ".permission-form" ).submit();
        $('#update_permission').attr('disabled', true);
        $('#update_permission').addClass('update_Permission_loader');
        $('#update_permission').html("").append("Update Permission &nbsp;<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
    }
});

$("body").delegate('#permission_name','keyup',function() {
    $('#already-exist').removeClass('show-exist');
});

$("body").delegate('#create_permission_btn','click',function() {
    var input = $('#permission_name').val();
    $.ajax({
        url: '/v2/merchant/permission/get_name',
        type: 'GET',
        data: {val: input},
        success: function(data) {
            if(data.success == true ){
                // event.preventDefault();
                $('#already-exist').addClass('show-exist');
                $('#create_permission_btn').attr('disabled', true);
            }
            else if($(".permission-form").valid() && $(".permission-form").find("input[type='checkbox']:checked").length == 0){
                $("#error_message").css("display","block");
                return false
            }
            else {
                if ($(".permission-form").valid() && $(".permission-form").find("input[type='checkbox']:checked").length > 0 && $('#permission_name').val() != ""){
                    $(".permission-form").submit();
                    $('#create_permission_btn').html("").append("Create Permission &nbsp;<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
                }
                else{
                    return false
                }
            }

        }

    });
});

// $(document).on('keyup change','#permission_export_account',function () {
//     if($("#permission_export_account").is(":checked")) {$("#permission_view_accounts").prop("checked",true)}else{$("#permission_view_accounts").prop("checked",false)}
// })
// $(document).on('keyup change','#permission_export_chargeback',function () {
//     if($("#permission_export_chargeback").is(":checked")) {$("#permission_view_chargeback_cases").prop("checked",true)}else{$("#permission_view_chargeback_cases").prop("checked",false)}
// })
// $(document).on('keyup change','#permission_export_invoice',function () {
//     if($("#permission_export_invoice").is(":checked")) {$("#permission_view_invoice").prop("checked",true)}else{$("#permission_view_invoice").prop("checked",false)}
// })
// $(document).on('keyup change','#permission_export_funding_schedule',function () {
//     if($("#permission_export_funding_schedule").is(":checked")) {$("#permission_funding_schedule").prop("checked",true)}else{$("#permission_funding_schedule").prop("checked",false)}
// })
// $(document).on('keyup change','#permission_export_business_settings',function () {
//     if($("#permission_export_business_settings").is(":checked"))
//     {   $("#permission_user_view_only").prop("checked",true)
//         $("#permission_permission_view_only").prop("checked",true)
//         $("#permission_api_key").prop("checked",true)
//         $("#permission_plugin").prop("checked",true)
//         $("#permission_fee_structure").prop("checked",true)}
//     else
//     {$("#permission_user_view_only").prop("checked",false)
//         $("#permission_permission_view_only").prop("checked",false)
//         $("#permission_api_key").prop("checked",false)
//         $("#permission_plugin").prop("checked",false)
//         $("#permission_fee_structure").prop("checked",false)}
// })
// $(document).on('keyup change','#permission_export_withdrawal',function () {
//     if($("#permission_export_withdrawal").is(":checked"))
//     {   $("#permission_check_view_only").prop("checked",true)
//         $("#permission_push_to_card_view_only").prop("checked",true)
//         $("#permission_ach_view_only").prop("checked",true)
//         $("#permission_gift_card_view_only").prop("checked",true)}
//     else
//     {$("#permission_check_view_only").prop("checked",false)
//         $("#permission_push_to_card_view_only").prop("checked",false)
//         $("#permission_ach_view_only").prop("checked",false)
//         $("#permission_gift_card_view_only").prop("checked",false)}
// })
// $(document).on('keyup change','#permission_export',function () {
//     if($("#permission_exports").is(":checked"))
//     {
//         $("#permission_view_accounts").prop("checked",true)
//         $("#permission_view_chargeback_cases").prop("checked",true)
//         $("#permission_view_invoice").prop("checked",true)
//         $("#permission_funding_schedule").prop("checked",true)
//         $("#permission_user_view_only").prop("checked",true)
//         $("#permission_permission_view_only").prop("checked",true)
//         $("#permission_api_key").prop("checked",true)
//         $("#permission_plugin").prop("checked",true)
//         $("#permission_fee_structure").prop("checked",true)
//         $("#permission_check_view_only").prop("checked",true)
//         $("#permission_push_to_card_view_only").prop("checked",true)
//         $("#permission_ach_view_only").prop("checked",true)
//         $("#permission_gift_card_view_only").prop("checked",true)
//     }
//     else {
//         $("#permission_view_accounts").prop("checked",false)
//         $("#permission_view_chargeback_cases").prop("checked",false)
//         $("#permission_view_invoice").prop("checked",false)
//         $("#permission_funding_schedule").prop("checked",false)
//         $("#permission_user_view_only").prop("checked",false)
//         $("#permission_permission_view_only").prop("checked",false)
//         $("#permission_api_key").prop("checked",false)
//         $("#permission_plugin").prop("checked",false)
//         $("#permission_fee_structure").prop("checked",false)
//         $("#permission_check_view_only").prop("checked",false)
//         $("#permission_push_to_card_view_only").prop("checked",false)
//         $("#permission_ach_view_only").prop("checked",false)
//         $("#permission_gift_card_view_only").prop("checked",false)
//         }
// })

// select all checkbox js in create user permission modal
$(document).ready(function(){
    $('input[class="select_all_permissions"]').click(function(){
        if($(this).prop("checked") == true){
            $("input[type=checkbox]").prop("checked", true);
        }
        else if($(this).prop("checked") == false){
            $("input[type=checkbox]").prop("checked", false);
        }
    });
});

$(document).ready(function(){
    $('.create-permissions').change(function(){
        if($('.create-permissions').is(':checked')){
            $('.view-permissions').prop('checked', true).trigger('change');
        } else {
            $('.view-permissions').prop('checked', false).trigger('change');
        }
    });

    $('.view-permissions').change(function(){
        if($('.create-permissions').is(':checked')){
            $('.view-permissions').prop('checked', true).trigger('change');
        }
    });

    $('.create-employee').change(function(){
        if($('.create-employee').is(':checked')){
            $('.view-employee').prop('checked', true).trigger('change');
        } else {
            $('.view-employee').prop('checked', false).trigger('change');
        }
    });

    $('.view-employee').change(function(){
        if($('.create-employee').is(':checked')){
            $('.view-employee').prop('checked', true).trigger('change');
        }
    });

});