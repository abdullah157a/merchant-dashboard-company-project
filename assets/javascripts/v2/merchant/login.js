// Create Password Js
$(".m-create-password-input").focus(function() {
    $("#tooltips").show();
});

// var starPasswordValue;
// $( ".login-eye" ).click(function() {
//     var password=$(".hidpassw").val();
//     starPasswordValue=$(".m-login-password").val();
//     $(".m-login-password").val(password);
//     $(".login-open-eye").show();
//     $(".login-eye").hide();
// });

// $( ".login-open-eye" ).click(function() {
//     var password=$(".hidpassw").val();
//     $(".m-login-password").val(starPasswordValue);
//     $(".login-open-eye").hide();
//     $(".login-eye").show();
// });



$(".m-create-password-input").on("keyup change",function() {
    var PasswordValue=$('.m-create-password-input').val();
    var confirmPasswordValue=$('.m-confirm-password-input').val();

    if($(".termsandservices_cb").prop("checked") == true && confirmPasswordValue!='' && PasswordValue!=''){
        $(".m_login_signin_submit").removeAttr('id');
        $(".m_login_signin_submit").addClass("m-login__btn--primary");
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

$(".m-confirm-password-input").on("keyup change",function() {
    var PasswordValue=$('.m-create-password-input').val();
    var confirmPasswordValue=$('.m-confirm-password-input').val();
    if($(".termsandservices_cb").prop("checked") == true && PasswordValue!='' && confirmPasswordValue !=''){
        $(".m_login_signin_submit").removeAttr('id');
        $(".m_login_signin_submit").addClass("m-login__btn--primary");
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

$('input[type="checkbox"]').click(function(){
    if($(".termsandservices_cb").prop("checked") == true){
        $PasswordValue=$('.m-create-password-input').val();
        $confirmPasswordValue=$('.m-confirm-password-input').val();
        if($PasswordValue!='' && $confirmPasswordValue!=''){
            $(".m_login_signin_submit").removeAttr('id');
            $(".m_login_signin_submit").addClass("m-login__btn--primary");
        }
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

//End Create Password js

//Create password starts
// function createstars(n) {
//     return new Array(n+1).join("*")
// }
// $(document).ready(function() {

//     var timer = "";

//     $(".panel").append($('<input type="text" class="hidpassw" />'));

//     $(".hidpassw").attr("name", $(".pass").attr("name"));

//     $(".pass").attr("type", "text").removeAttr("name");

//     $("body").on("keypress", ".pass", function(e) {
//         var code = e.which;
//         if (code >= 32 && code <= 127) {
//             var character = String.fromCharCode(code);
//             $(".hidpassw").val($(".hidpassw").val() + character);
//         }


//     });

//     $("body").on("keyup", ".pass", function(e) {
//         var code = e.which;

//         if (code == 8) {
//             var length = $(".pass").val().length;
//             $(".hidpassw").val($(".hidpassw").val().substring(0, length));
//         } else if (code == 37) {

//         } else {
//             var current_val = $('.pass').val().length;
//             $(".pass").val(createstars(current_val - 1) + $(".pass").val().substring(current_val - 1));
//         }

//         clearTimeout(timer);
//         timer = setTimeout(function() {
//             $(".pass").val(createstars($(".pass").val().length));
//         }, 200);

//     });

// });

//confirm password starts
function createconfirmstars(n) {
    return new Array(n+1).join("*")
}


$(document).ready(function() {

    var timer = "";

    // $(".panel").append($('<input type="text" class="hidconfirmpassw" />'));

    $(".hidconfirmpassw").attr("name", $(".pass").attr("name"));

    $(".confirmpass").attr("type", "text").removeAttr("name");

    $("body").on("keypress", ".confirmpass", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hidconfirmpassw").val($(".hidconfirmpassw").val() + character);
        }


    });

    $("body").on("keyup", ".confirmpass", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".confirmpass").val().length;
            $(".hidconfirmpassw").val($(".hidconfirmpassw").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.confirmpass').val().length;
            $(".confirmpass").val(createconfirmstars(current_val - 1) + $(".confirmpass").val().substring(current_val - 1));
        }

        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".confirmpass").val(createstars($(".confirmpass").val().length));
        }, 200);

    });
});

var confirmPasswordStarts;
$( ".login-confirm-eye" ).click(function() {
    var password=$(".hidconfirmpassw").val();
    confirmPasswordStarts=$(".m-confirm-password-input").val();
    $(".m-confirm-password-input").val(password);
    $(".login-confirm-open-eye").show();
    $(".login-confirm-eye").hide();
});

$( ".login-confirm-open-eye" ).click(function() {
    var password=$(".hidconfirmpassw").val();
    $(".m-confirm-password-input").val(confirmPasswordStarts);
    $(".login-confirm-open-eye").hide();
    $(".login-confirm-eye").show();
});

$(".m-confirm-password-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.password-confirm-error').hide();
        $('.m-confirm-password-input').css("border","solid 2px #35bab6");
        $(".login-confirm-eye").show();
        $(".login-confirm-open-eye").hide();
    }
    else
    {
        $('.password-confirm-error').show();
        $(".login-confirm-open-eye").hide();
        $(".login-confirm-eye").show();
        $('.m-confirm-password-input').css("border","solid 2px #d45858");
    }
});



// End Confirm Password js

// Login Page Js
$(".email-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.email-login-error').hide();
        $('.email-input').css("border","solid 2px #35bab6");
    }
    else
    {
        $('.email-login-error').show();
        $('.email-input').css("border","solid 2px #d45858");
    }
});

$(".m-login-password").on('keyup change', function (){
    if($(this).val() !==''){
        $('.password-login-error').hide();
        $('.m-login-password').css("border","solid 2px #35bab6");
        $(".login-eye").show();
        $(".login-open-eye").hide();
    }
    else
    {
        $(".login-open-eye").hide();
        $(".login-eye").show();
        $('.password-login-error').show();
        $('.m-login-password').css("border","solid 2px #d45858");
    }
});



$('#terms_and_services').click(function () {
    $('.term_service_text').show();
    const element =  document.querySelector('.term_service_text')
    element.classList.add('animated', 'slideInLeft')
});

$('.close_terms_services').click(function () {

    $('.term_service_text').hide();
    $('#terms_and_services').removeClass('slideInLeft');
});

// End Login Page JS

$('#terms_and_services').click(function () {
    $('.term_service_text').show();
    const element =  document.querySelector('.term_service_text')
    element.classList.add('animated', 'slideInLeft')
});

$('.close_terms_services').click(function () {

    $('.term_service_text').hide();
    $('#terms_and_services').removeClass('slideInLeft');
});

$('body').click(function () {
    $('#tooltips').hide();
});

$('#tooltips').click(function (event) {
    $('#tooltips').show();
    event.stopPropagation();
});

$('.m-create-password-input').click(function (event) {
    $('#tooltips').show();
    event.stopPropagation();
});
$('.password_show').on('click', function () {
    if($('#password').attr('type')=='text'){
        $('#password').attr('type', 'password');
        $('.hide_image').hide()
        $('.show_image').show()
        // $(this).attr("src", window.location.origin +"/assets/v2/login/login-eye.svg");
    }
    else{
        $('#password').attr('type', 'text');
        $('.show_image').hide()
        $('.hide_image').show()
        // $(this).attr("src", window.location.origin +"/assets/v2/login/login-open-eye.svg");
    }
});
$('.new_password_show').on('click', function () {
    if($('#new_password').attr('type')=='text'){
        $('#new_password').attr('type', 'password');
        $('.hide_image').hide()
        $('.show_image').show()
        // $(this).attr("src", window.location.origin +"/assets/v2/login/login-eye.svg");
    }
    else{
        $('#new_password').attr('type', 'text');
        $('.show_image').hide()
        $('.hide_image').show()
        // $(this).attr("src", window.location.origin +"/assets/v2/login/login-open-eye.svg");
    }
});
$('.confrim_password_show').on('click', function () {
    if($('#confirm_password').attr('type')=='text'){
        $('#confirm_password').attr('type', 'password');
        $('.hide_confirm_image').hide()
        $('.show_confirm_image').show()
        // $(this).attr("src", window.location.origin +"/assets/v2/login/login-eye.svg");
    }
    else{
        $('#confirm_password').attr('type', 'text');
        $('.show_confirm_image').hide()
        $('.hide_confirm_image').show()
        // $(this).attr("src", window.location.origin +"/assets/v2/login/login-open-eye.svg");
    }
});

$('#username').focusout(function(){
    var pattern = re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var userinput = $("#username").val();
    if (userinput == ""){
        $("#email_error").text("");
        $(this).removeClass('error-border');
    }else if (!pattern.test(userinput) ){
        $("#email_error").text("Invalid email");
        $(this).addClass('error-border');
    }
    else{
        $(this).removeClass('error-border');
        $("#email_error").text("");

    }
});

$(".verify_credentials").on("click", function () {
    var pattern = re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var userinput = $("#username").val();
    var userpassword = $("#password").val();
    var attempts = $("#attempts").val();
    if (pattern.test(userinput)){
        $("#email_error").text("");
        $("#username").removeClass('error-border');
        if (userpassword.length > 0) {
            $("#password_error").text("");
            $("#password").removeClass('error-border');
            $(".verify_credentials").addClass('m-loader m-loader--right m-loader--light').prop("disabled",true);
            $.ajax({
                url: '/login_verification',
                type: 'post',
                data: {type: "login", email: userinput, password: userpassword, attempts: attempts},
                success: function(data) {
                    $(".original_btn").click();
                },
                error: function(data){
                    $("#attempts").val(parseInt(attempts) + 1);
                    if (data.responseJSON["error"] == "You have exceeded the maximum attempts to login and your account is now locked. Please check the email associated to this account for instructions on how to reset your password."){
                        // $("#password_error").text(data.responseJSON["error"]);
                        $('.welcome_text').addClass('block_hide');
                        $('.forgot_pswd_text').addClass('block_hide');
                        $('.login_text').addClass('block_hide');
                        $('#login_div').addClass('block_hide');
                        $('.block_account_text').removeClass('block_hide');
                        $('.block_account_text_two').removeClass('block_hide');
                        $('.block_account_text_three').removeClass('block_hide');
                        $('.block_account_text').addClass('block_show');
                        $('.block_account_text_two').addClass('block_show');
                        $('.block_account_text_three').addClass('block_show');
                    }
                    else if (data.responseJSON["error"] == "Invalid password - You have 1 more attempt"){
                        $("#password_error").text(data.responseJSON["error"]);
                    }
                    else if (data.responseJSON["error"] == "Invalid password - You have 2 more attempt"){
                        $("#password_error").text("Invalid password - You have 2 more attempts");
                    }
                    else{
                        $("#password_error").text("Invalid Email Address or Password");
                    }
                    $("#password").addClass('error-border');
                    $(".verify_credentials").removeClass('m-loader m-loader--right m-loader--light').prop("disabled",false);
                }
            });
        }else{
            $("#password_error").text("Password required");
            $("#password").addClass('error-border');
        }
    }else{
        $("#email_error").text("Invalid email");
        $("#username").addClass('error-border');
    }
});
$(".verify_opt").on("click", function () {
    var userinput = $("#token").val();
    var curren_user_id = $("#current_user_id").val();
    var attempts = $("#attempts").val();
    if (parseInt(attempts) > 5) {
        logout();
    }else {
        if (userinput.length == 6) {
            $("#opt_error").text("");
            $("#token").removeClass('error-border');
            $.ajax({
                url: '/login_verification',
                type: 'post',
                data: {type: "two_step", id: curren_user_id, token: userinput},
                success: function (data) {
                    $("#two_step_form").submit();
                    // $(".original_btn").click();
                },
                error: function (data) {
                    $("#attempts").val(parseInt(attempts) + 1);
                    remaining = 5 - parseInt(attempts);
                    if(remaining == 1){
                        $("#opt_error").text("Invalid code. You have "+ remaining +" more attempt");
                    }else if (remaining == 0){
                        logout();
                    } else{
                        $("#opt_error").text("Invalid code. You have "+ remaining +" more attempts");
                    }
                    $("#token").addClass('error-border').val('');
                }
            });
        } else {
            $("#opt_error").text("Enter six digits code");
            $("#token").addClass('error-border');
        }
    }
});
function logout(){
    $.ajax({
        url: '/users/sign_out?role=merchant',
        type: 'delete',
        data: {role: "merchant"},
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            location.reload();
        }
    });
}

$(document).ready(function(){
    var height = $(window).height();
    var url = window.location.href;
    if (url.includes("password"))
    {
        var expected = 680;
    }
    else
    {
        var expected = 800;
    }
    console.log(expected);
    if (height < expected)
    {
        $('#remove--hieght').removeClass('hieght-100');
        $('#remove--hieght-1').removeClass('hieght-100');
    }
    else
    {
        $('#remove--hieght').addClass('hieght-100');
        $('#remove--hieght-1').addClass('hieght-100');
    }

});
$( window ).resize(function() {
    var height = $(window).height();
    var url = window.location.href;
    if (url.includes("password"))
    {
        var expected = 680;
    }
    else
    {
        var expected = 800;
    }
    console.log(expected);
    if (height < expected)
    {
        $('#remove--hieght').removeClass('hieght-100');
        $('#remove--hieght-1').removeClass('hieght-100');
    }
    else
    {
        $('#remove--hieght').addClass('hieght-100');
        $('#remove--hieght-1').addClass('hieght-100');
    }
});

$("#forgot_password_btn").on("click",function () {
    var email = $(".forgot_password_email").val();
    $("#forgot_pwd_email").val(email);
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var check_email = regex.test(email);
    if (email == ""){
        $("#email_error").text("Please Enter Email");
        return false;
    }
    else if (!check_email){
        $("#email_error").text("Invalid email");
        return false;
    }
});

$("#back_btn").on("click",function () {
    $("#login_div").show();
    $(".welcome_text").removeClass("display_none");
    $("#forgot_password_div").addClass("display_none");
    $(".login_text").removeClass("display_none");
    $(".forgot_pswd_text").addClass("display_none");
    $(".help_us_text").addClass("display_none");
    $(".m-login__form").addClass("mt-2");

});

$(".send_code_option").on("change",function () {
    if ($(this).val() == "text"){
        $("#send_code_type").val("text");
    }
    else{
        $("#send_code_type").val("email");
    }
});

$(".replace_email").on("click",function () {
    $('#device_phone').css('display', 'none')
    $('#show_email').css('display', 'block');
    $('.replace_email').css('display', 'none');
    $('.replace_phone').css('display', 'block');
    $('#resend_btn').css('display', 'none');
    $('#resend_btn_email').css('display', 'block');
    if ($("#resend_confirmation").length > 0){
        $("#resend_confirmation").html('');
    }
});

$(".replace_phone").on("click",function () {
    $('#device_phone').css('display', 'block')
    $('#show_email').css('display', 'none');
    $('.replace_phone').css('display', 'none');
    $('.replace_email').css('display', 'block');
    $('#resend_btn').css('display', 'block');
    $('#resend_btn_email').css('display', 'none');
    $("#resend_confirmation").html('');
    if ($("#resend_confirmation").length > 0){
        $("#resend_confirmation").html('');
    }
});

$("#resend_btn_email").on("click",function () {
    $("#resend_confirmation").text("Code Sent");
});

$(".btn_send_code").on("click",function () {
    //reset
    var send_code_type = $("#send_code_type").val();
    var user_id = $("#user_id").val();
    var partner = $("#user_type").val();
    var from_reset = $(this).data("from_reset")
    $.ajax({
        url: '/send_2factor_code',
        type: 'post',
        data: {type: send_code_type, user_id: user_id, came_from: "send_code",from_reset: from_reset ,partner: partner},
        success: function (data) {
            if (send_code_type == "text"){
                $("#device_phone").css('display', 'block');
                $("#show_email").css('display', 'none');
                $(".device_phone").css('display', 'none');
                $(".show_email").css('display', 'block');
                $('.two_step_verification_text').removeClass('display_none');
                $('#device_phone').removeClass('display_none');
            }
            else if (send_code_type == "email"){
                $("#device_phone").css('display', 'none');
                $("#show_email").css('display', 'block');
                $(".device_phone").css('display', 'block');
                $(".show_email").css('display', 'none');
                $('.two_step_verification_text').removeClass('display_none');
                $('#show_email').removeClass('display_none');

            }
            $("#login_div").hide();
            $(".welcome_text").addClass("display_none");
            $("#forgot_password_div1").addClass("display_none");
            $("#send_code").removeClass("display_none");
            $(".login_text").addClass("display_none");
            $(".forgot_pswd_text").addClass("display_none");
            $(".help_us_text").addClass("display_none");
            $(".verification_code").removeClass("display_none");
            $("#verify_display").addClass("display_none");
            $(".m-login__form").addClass("mt-2");
        },
        error: function (data) {

        }
    });

});
$(".send_email_code").on("click",function () {
    var email_type = $(".email_type").val();
    var user_id = $("#user_id").val();
    var partner = $("#user_type").val();
    $.ajax({
        url: '/send_2factor_code',
        type: 'post',
        data: {type: email_type, user_id: user_id, came_from: "send_code", partner: partner},
        success: function (data) {
        },
        error: function (data) {

        }
    });
});
$(".send_text_code").on("click",function () {
    var text_type = $(".text_type").val();
    var user_id = $("#user_id").val();
    var partner = $("#user_type").val();
    $.ajax({
        url: '/send_2factor_code',
        type: 'post',
        data: {type: text_type, user_id: user_id, came_from: "send_code", partner: partner},
        success: function (data) {
        },
        error: function (data) {

        }
    });
});
$(".btn_send_code_verfiy").on("click",function () {

    var code = $("#verification_code").val();
    var user_id = $("#user_id").val();
    $("#verification_code_error").text("");
    $.ajax({
        url: '/send_2factor_code',
        type: 'post',
        data: {code: code, user_id: user_id, came_from: "verify_code"},
        success: function (data) {
            $("#login_div").hide();
            $(".welcome_text").addClass("display_none");
            $("#forgot_password_div").addClass("display_none");
            $("#forgot_password_div1").addClass("display_none")
            $("#send_code").addClass("display_none");
            $("#reset_password").removeClass("display_none");
            $(".login_text").addClass("display_none");
            $(".forgot_pswd_text").addClass("display_none");
            $(".help_us_text").addClass("display_none");
            $(".verification_code").addClass("display_none");
            $(".new_password_text").removeClass("display_none");
            $(".rest_password_welcome").removeClass("display_none");
            $(".m-login__form").addClass("mt-2");
        },
        error: function (data) {
            $("#verification_code_error").text("Please Enter correct code.");
        }
    });

});

$("#resend_btn").on("click",function () {

    var send_code_type = $("#send_code_type").val();
    var user_id = $("#user_id").val();
    var partner = $("#partner").val();
    $.ajax({
        url: '/send_2factor_code',
        type: 'post',
        data: {type: send_code_type, user_id: user_id, came_from: "resend_code", partner: partner},
        success: function (data) {
            $("#resend_confirmation").text("Code Sent");
        },
        error: function (data) {

        }
    });

});


function createOldPasswordWelcomeStars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {
    var timer = "";
    $(".m-old-welcome-password").attr("type", "text").removeAttr("name");
    $("body").on("keyup", ".m-old-welcome-password", function(e) {
        var code = e.which;
        if (code == 8) {
            var length = $(".m-old-welcome-password").val().length;
            $(".hide_old_welcome_password").val($(".hide_old_welcome_password").val().substring(0, length));
        } else if (code == 37) {
        } else {
            var current_val = $('.m-old-welcome-password').val().length;
            $(".m-old-welcome-password").val(createOldPasswordWelcomeStars(current_val - 1) + $(".m-old-welcome-password").val().substring(current_val - 1));
        }
        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".m-old-welcome-password").val(createstars($(".m-old-welcome-password").val().length));
        }, 200);
    });
});

$(".new_password").on('keyup change', function () {
    $("#tooltips").addClass("display_show");
});
$('body').click(function () {
    $("#tooltips").removeClass("display_none");
    $('#tooltips').addClass("display_none");
});
$('#tooltips').click(function (event) {
    event.stopPropagation();
});

$(document).on("keyup", ".confirm_password", function () {
    $("#tooltips").removeClass('display_show');
    $('#tooltips').hide();
});

$(document).on("blur", ".new_password", function () {
    $("#tooltips").removeClass('display_show');
    $('#tooltips').hide();
});

$(document).on("keyup", ".new_password", function () {
    $("#tooltips").show();
    password_validation($(this).val());
});

function password_validation(value) {
    var upperCase= new RegExp('[A-Z]');
    var lowerCase= new RegExp('[a-z]');
    var numbers = new RegExp('[0-9]');
    var v_digits = false;
    var v_numbers = false;
    var v_upcase = false;
    var v_downcase = false;
    if(value.length >= 8){
        $("#8_characters").removeClass('fa-times').addClass('fa-check btm');
        $("#8_characters").parent().removeClass('invalid').addClass('valid');
        v_digits = true;
    }else{
        v_digits = false;
        $("#8_characters").addClass('fa-times').removeClass('fa-check');
        $("#8_characters").parent().addClass('invalid').removeClass('valid');
    }
    if(value.match(upperCase)) {
        $("#upcase").removeClass('fa-times').addClass('fa-check');
        $("#upcase").parent().removeClass('invalid').addClass('valid');
        v_upcase = true;
    }else{
        v_upcase = false;
        $("#upcase").addClass('fa-times').removeClass('fa-check');
        $("#upcase").parent().addClass('invalid').removeClass('valid');
    }
    if(value.match(lowerCase)) {
        $("#downcase").removeClass('fa-times').addClass('fa-check');
        $("#downcase").parent().removeClass('invalid').addClass('valid');
        v_downcase = true;
    }else{
        v_downcase = false;
        $("#downcase").addClass('fa-times').removeClass('fa-check');
        $("#downcase").parent().addClass('invalid').removeClass('valid');
    }
    if(value.match(numbers)){
        $("#1_number").removeClass('fa-times').addClass('fa-check');
        $("#1_number").parent().removeClass('invalid').addClass('valid');
        v_numbers = true;
    }else{
        v_numbers = false;
        $("#1_number").addClass('fa-times').removeClass('fa-check');
        $("#1_number").parent().addClass('invalid').removeClass('valid');
    }
    if(v_digits && v_numbers && v_upcase && v_downcase){
        return true;
    }else{
        return false;
    }

}

$(function() {
    $('#alert-message-top').delay(2000).fadeOut();
});

$("body").delegate('.password_submit', 'click', function () {
    if ($('#confirm_password').val() != $('#new_password').val()) {
        { $(".confirm-pass-error").html("Password confirmation not successful");}
        return false;
    }else{
        $(".confirm-pass-error").html("");
    }

});

$("body").delegate('#token', 'keyup', function () {
    if ($('#token').val() == "" ){
        $('#m_login_two_step_submit').css('background-color', '#d6d6d6')
    }
    else{
        $('#m_login_two_step_submit').css('background-color', '#35BAB6')
    }
});

$(document).on('show.bs.dropdown', '.send_code_option', function (e) {
    $(".btn.dropdown-toggle").addClass("change")
});

$(document).on('hide.bs.dropdown', '.send_code_option', function (e) {
    $(".btn.dropdown-toggle").removeClass("change")
});