
// Start credit card

function formatCardNumber(event, element) {
    // if (isNaN(event.key) && !isAllowedKey(event)) {
    //     event.preventDefault();
    // } else {
    var code = event.keyCode ? event.keyCode : event.which;
    console.log(code);
    if (code != 8) {
        if(element.value.length > 14) {
            var position = element.selectionStart;
            element.value = element.value.replace(/\W/gi, '').replace(/^(.{4})(.{4})(.{4})(.*)$/, "$1 $2 $3 $4");
            if(element.value.length != 19) {
                element.setSelectionRange(position, position);
            }
        }
        else {
            element.value = element.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
        }
    }
    // }

}

function isAllowedKey(event) {
    var allowed = false;
    var code = event.keyCode ? event.keyCode : event.which;
    if (code === 8 || code === 9 || code === 37 || code === 39) {
        allowed = true;
    }
    return allowed;
}

function limit(event, element, max_chars) {
    if(isTextSelected(element)){																		//
        max_chars += 1;
    }
    if (element.value.length >= max_chars && !isAllowedKey(event)) {
        event.preventDefault();
    }
}

function showCardValue() {
    document.getElementById("cardNo").value = document.getElementById("cardNoSafe").value;
    // document.getElementById("cardNo1").value = document.getElementById("cardNoSafe").value;
}

function isTextSelected(input) {
    var startPosition = input.selectionStart;
    var endPosition = input.selectionEnd;

    var selObj = document.getSelection();
    var selectedText = selObj.toString();

    if (selectedText.length != 0) {
        input.focus();
        input.setSelectionRange(startPosition, endPosition);
        return true;
    } else if (input.value.substring(startPosition, endPosition).length != 0) {
        input.focus();
        input.setSelectionRange(startPosition, endPosition);
        return true;
    }
    return false;
}

function hideCardValue(val) {
    $("#cardNoSafe").val(val);
    var len = val.length;
    if (len >= 14) {
        const regex = /\d{4}(?= \d{1})/g;
        const substr = "••••";
        new_value = val.replace(regex, substr);
        $("#cardNo").val(new_value);
        $("#cardNo1").val(new_value);
    }
}

//  End credit Card4242 4242 4242 4242

// CVV Code Script

$('#card_exp').on('input keypress change blur', function () {
    $(this).val(function (index, value) {
        var x = value.replace(/[^0-9]+/gi, '').replace(/(.{2})/g,'$1/');
        return x;
    });
    $(this).val(function(index,value){
        var q=document.getElementById('card_exp').value;
        var res = q.slice(0, 5);
        return res
    })

});

//  End CVV code

//  Start Button desiable secript

// function myCardno(x) {
//     var x =  document.getElementById("cardNo").value;
//     var y =  document.getElementById("cardNo1").value;
//     document.getElementById("card").innerHTML = x;
//     if (x =='' || y == '')
//     {
//         document.getElementById("card").innerHTML = '<div class="card-number">\n' +
//             '                                            <span class="dots">....</span> <span class="dots">....</span> <span class="dots">....</span> <span id="card" class="digits">7894</span>\n' +
//             '                                        </div>';
//
//     }
// //                $( "#cardNo" ).text(function(_,x) {
// //                    return x.replace(/\d{12}(\d{4})/, "xxxx xxxx xxxx $1");
// //                });
//
// }
function myCardname() {
    var y =  document.getElementById("card_name").value;
    document.getElementById("CardName").innerHTML = y;
    if (y =='')
    {
        document.getElementById("CardName").innerHTML = 'John Doe';
    }

}

function myCardexp() {
    var z =  document.getElementById("card_exp").value;
    document.getElementById("cardExp").innerHTML = z;
    if (z =='')
    {
        document.getElementById("cardExp").innerHTML = '06/07';
    }

}

function my_Cvv() {
    var c =  document.getElementById("price_per_ticket").value;
    document.getElementById("price_per_ticket").innerHTML = c;
    if (c =='')
    {
        document.getElementById("price_per_ticket").innerHTML = '';
    }

}

function myTerm() {
    var d =  document.getElementById("term_service").value;
    document.getElementById("term_service").innerHTML = d;
    if (d =='')
    {
        document.getElementById("term_service").innerHTML = '';
    }

}

function myPrivacy() {
    var e =  document.getElementById("privacy").value;
    document.getElementById("privacy").innerHTML = e;
    if (e =='')
    {
        document.getElementById("privacy").innerHTML = '';
    }
}


//  End button desabled

//  CVV rotate Script

function myCVV() {
    var x = document.getElementById("imageProcess");
    var y = document.getElementById("front_side")
    if (x.style.display === "block") {
        y.style.display = "block";
        x.style.display = "none"
    } else {
        y.style.display = "none";
        x.style.display = "block";
    }
    $('html, body').animate({
        scrollTop: $("#imageProcess").offset().top
    });
}

//  End CVV Rotate Script




$(document).ready(function () {
    validateCheckoutPaymentForm("new_card");

});

$(document).ready(function () {
    $("#cardNo,#card_name,#card_exp,#card_cvv,#term_service,#privacy").on("change",function () {
        var card_no=$("#cardNo").val();
        var card_name=$("#card_name").val();
        var card_exp=$("#card_exp").val();
        var card_cvv=$("#card_cvv").val();
        var terms=$("#term_service").is(':checked');
        var privacy=$("#privacy").is(':checked');

        if (card_no != "" && card_exp != "" && card_name != "" && card_cvv != "" && terms == true && privacy == true){
            $("#tooltip_text").css("display","none");

            $(".btn-pay").css("background-color","#ffd95a");
            $(".btn-pay").css("color","#4c4c4c");
        }else{
            $("#tooltip_text").css("display","block");
            $(".btn-pay").css("background-color","#e8e8e8");
            $(".btn-pay").css("color","#adadad");
        }
    })
});


$("#new_card").on("submit",function (e) {
    if($("#new_card").valid() && $("#term_service").is(':checked') && $("#privacy").is(':checked')) {
        $(".checkout").css("display", "none");
        $(".progress_responsive").css("display", "block");
    }
    else{
        e.preventDefault();
        return false;
    }
});

function validateCheckoutPaymentForm(form_id) {
    $(function() {
        var formRules = {
            rules: {
                "card_no": {
                    required: true
                },
                "card[name]":{
                    required:true
                },
                "card[exp_date]":{
                    required:true
                },
                "card[cvv]":{
                    required:true
                }


            },
            messages: {
                "card_no": {
                    required: "Please enter Card Number"
                },
                "card[name]": {
                    required: "Please enter Name on card"
                },
                "card[exp_date]": {
                    required: "Please enter Card Expiry"
                },
                "card[cvv]": {
                    required: "Please enter Card CVV"
                }

            },

        };
        $('#' + form_id).validate(formRules);
    });
};

$("#cardNo").on("blur",function () {
    hideCardValue(this.value);
    var card_no=$("#cardNoSafe").val();
    card_no=card_no.replace(/\s/g, '');
    if (card_no != "") {
        $.ajax({
            url: "/checkout/get_card_information",
            type: 'POST',
            data: {card_number: card_no},
            success: function (data) {
                var bank = data["bank"];
                $(".bank_name").html(bank);
                var scheme = data["scheme"]
                if (scheme == "mastercard") {
                    $('.credit-logo').prop('src', '/assets/admins/checkout/master.svg');
                } else if (scheme == "discover") {
                    $('.credit-logo').prop('src', '/assets/admins/checkout/discover.svg');

                } else if (scheme == "jcb") {
                    $('.credit-logo').prop('src', '/assets/admins/checkout/jcb.svg');
                }else if (scheme == "visa") {
                    $('.credit-logo').prop('src', '/assets/admins/checkout/visa.svg');
                }
            },
            error: function (data) {

            }
        });
    }
});

$("#cardNo").on("keyup",function () {
    if ($("#cardNo").val() == ""){
        $("#cardNo1").val("•••• •••• •••• 7894");
    }
});

$("#submit_b").hover(function () {
    var card_no = $("#cardNo").val();
    var card_name = $("#card_name").val();
    var card_exp = $("#card_exp").val();
    var card_cvv = $("#card_cvv").val();
    var terms=$("#term_service").is(':checked');
    var privacy=$("#privacy").is(':checked');

    if (card_no != "" && card_name != "" && card_exp != "" && card_cvv !=""){
        if (terms == false || privacy == false){
            $("#tooltip_text").html("You must first agree to our Privacy Policy and Terms of Service");
        }else if(terms == true && privacy == true){
            // $("#tooltip_text").css("display","none");
        }

    }else{

        $("#tooltip_text").html("First, fill in your credit card information");
    }



});

$("." +
    "cvv_code").on("click",function () {
    myCVV();
});

$("#cardNo").on("focus",function () {
    showCardValue();
});

$("#cardNo").on("keypress change select blur keyup",function (event) {
//     formatCardNumber(ev, this);
    if (isNaN(event.key) && !isAllowedKey(event)) {
        event.preventDefault();
    } else {
        var code = event.keyCode ? event.keyCode : event.which;
        if (code != 8) {
            if($("#cardNo").val().length > 14) {
                var position = this.selectionStart;
                new_value = $("#cardNo").val().replace(/\W/gi, '').replace(/^(.{4})(.{4})(.{4})(.*)$/, "$1 $2 $3 $4");
                $("#cardNo").val(new_value);
                if($("#cardNo").val().length != 19) {
                    this.setSelectionRange(position, position);
                }
            }
            else {
                new_value = $("#cardNo").val().replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                $("#cardNo").val(new_value);
            }
        }
    }
    limit(event, this, 19);
});

$("#card_name").on("input",function (e) {
    this.value = this.value.replace(/[^a-zA-Z\s]/gi,'');
    myCardname(this.value);
});

$("#card_exp").on("keyup",function () {
    myCardexp(this.value);
});

// $("#card_cvv").on("keypress",function (event) {
//     return event.charCode >= 48 && event.charCode <= 57;
// });
$('#card_cvv').on('input', function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
});

$("#myBtn").on("click",function () {
    jsModalShowHide('1');

});

$("#myBtn1").on("click",function () {
    jsModalShowHide('2');
});

$("#myBtn2").on("click",function () {
    jsModalShowHide('3');
});

$("#no_btn").on("click",function () {
    jsModalShowHide('1');
});

$("#close_b1").on("click",function () {
    jsModalShowHide('3');
});

$("#close_b2").on("click",function () {
    jsModalShowHide('2');
});

$("#cardNo").on("change",function () {
    $("#cardNoSafe").val(this.value);

});

$(".revoke-paste").on("paste", function () {
    return false;
});
$(".revoke-paste").on("cut", function () {
    return false;
});
$(".revoke-paste").on("copy", function () {
    return false;
});


function myTimer() {
    var timeleft = 10;
    var downloadTimer = setInterval(function () {
        document.getElementById("countdown").innerHTML = "in " + timeleft + " seconds!";
        timeleft -= 1;
        if (timeleft <= 0) {
            clearInterval(downloadTimer);
            // document.getElementById("countdown").innerHTML = "Finished"
        }
    }, 1000);
}

// hover on CVV icon
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<img class="img-fluid" src="'+$(this).data('img') + '" />';
        },
    })
});

// loader js
function p2c(numb) {

    var rand = numb;
    var x = document.querySelector('.p2c');
    x.style.strokeDasharray = (rand * 7.1) + ' 999';
}

$('.btn-pay').on('click' ,function () {
    setTimeout(p2c(0), 200);
    // setTimeout(function(){ p2c(25);}, 1000);
    setTimeout(function(){ p2c(50);$('#locked').addClass('img--hide');
        $('#unlocked').removeClass('img--hide');
        $('#unlocked').addClass('img--show');
        $('#text_first').addClass('img--hide');
        $('#text_second').removeClass('img--hide');
        $('#text_second').addClass('img--show');   }, 2000);
    setTimeout(function(){ p2c(75);
        $('#text_second').addClass('img--show');  }, 4000);
    setTimeout(function(){ p2c(100);
        $('#unlocked').removeClass('img--show');
        $('#unlocked').addClass('img--hide');
        $('#success').removeClass('img--hide');
        $('#success').addClass('img--show');
        $('#text_second').removeClass('img--show');
        $('#text_second').addClass('img--hide');
        $('#text_third').removeClass('img--hide');
        $('#text_third').addClass('img--show'); }, 6000);
    setTimeout(function(){
        $('.working').addClass('img--hide');
        $('.first--text').addClass('img--hide');
        $('.last--text').addClass('img--show');
        $('.success--image').addClass('img--show');
    }, 7000);
    setTimeout(function(){
        $('.working').addClass('img--hide');
        $('.first--text').addClass('img--hide');
        $('#text_third').removeClass('img--show');
        $('#text_third').addClass('img--hide');
        $('#text_four').addClass('img--show');
        $('.success--image').removeClass('img--show');
        $('.success--image').addClass('img--hide');
        $('.error--image').addClass('img--show');
    }, 8000);
});

$('.btn-pay').click(function () {
    $('.checkout_box_loader').addClass('img--show');
    $('.checkout_box').addClass('img--hide');
    $('.edit_info_sec').addClass('img--hide');
});
$('.btn-edit_info_checkout').click(function () {
    $('.edit_info_sec').addClass('img--show');
    $('.checkout_box').addClass('img--hide');
    $('.checkout_box_loader').addClass('img--hide');
});
$('.btn-no').click(function () {
    $('.checkout_box').addClass('img--show');
    $('.edit_info_sec').removeClass('img--show');
    $('.edit_info_sec').addClass('img--hide');
});
$('.btn_shopping_back').click(function () {
    $('.checkout_box').addClass('img--show');
    $('.checkout_box_loader').removeClass('img--show');
    $('.checkout_box_loader').addClass('img--hide');
});

//********************* Api Check out JS start *******************//
$('.btn-next-api').click(function () {
    $('.api_billing_info').addClass('api_hide');
    $('.api_card_input').removeClass('api_hide');
    $('.api_card_input').addClass('api_show');
});

$('.btn-back-api').click(function () {
    $('.api_card_input').removeClass('api_show');
    $('.api_card_input').addClass('api_hide');
    $('.api_billing_info').removeClass('api_hide');
    $('.api_billing_info').addClass('api_show');
});
$('.btn-submit-api').click(function () {
    $('.api_checkout_box_loader').addClass('img--show');
    $('.api_checkout_box').addClass('img--hide');
});
$('.btn-submit-api').on('click' ,function () {
    setTimeout(p2c(0), 200);
    // setTimeout(function(){ p2c(25);}, 1000);
    setTimeout(function(){ p2c(50);$('#locked').addClass('img--hide');
        $('#unlocked').removeClass('img--hide');
        $('#unlocked').addClass('img--show');
        $('#text_first').addClass('img--hide');
        $('#text_second').removeClass('img--hide');
        $('#text_second').addClass('img--show');   }, 2000);
    setTimeout(function(){ p2c(75);
        $('#text_second').addClass('img--show');  }, 4000);
    setTimeout(function(){ p2c(100);
        $('#unlocked').removeClass('img--show');
        $('#unlocked').addClass('img--hide');
        $('#success').removeClass('img--hide');
        $('#success').addClass('img--show');
        $('#text_second').removeClass('img--show');
        $('#text_second').addClass('img--hide');
        $('#text_third').removeClass('img--hide');
        $('#text_third').addClass('img--show'); }, 6000);
    setTimeout(function(){
        $('.working').addClass('img--hide');
        $('.first--text').addClass('img--hide');
        $('.last--text').addClass('img--show');
        $('.success--image').addClass('img--show');
    }, 7000);
    setTimeout(function(){
        $('.working').addClass('img--hide');
        $('.first--text').addClass('img--hide');
        $('#text_third').removeClass('img--show');
        $('#text_third').addClass('img--hide');
        $('#text_four').addClass('img--show');
        $('.success--image').removeClass('img--show');
        $('.success--image').addClass('img--hide');
        $('.error--image').addClass('img--show');
    }, 8000);
});
//********************* Api Check out JS end   *******************//