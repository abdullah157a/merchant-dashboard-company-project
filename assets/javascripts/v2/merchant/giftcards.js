//= require cable
$(document).on("click", ".show-dynamic-modal", function (ev) {
    ev.preventDefault();
    butt =  $(this)
    $(butt).addClass('m-loader m-loader--right m-loader--warning').prop("disabled",true);
    var url = $(this).data("url");
    var modal_selector = $(this).data("modal_selector");
    var modal_container_selector = $(this).data("modal_container_selector");
    var method = $(this).data("method");
    $.ajax({
        url: url,
        type: method,
        success: function (data) {
            $(modal_container_selector).empty().html(data);
            $(modal_selector).modal();
            $(".m_selectpicker").selectpicker()
            if($('#wallet_id > option').length == 1){
                wallet_balance=$("#wallet_id").children(":selected").data("balance");
                wallet_balance = parseFloat(wallet_balance).toFixed(2);
                wallet_balance = parseFloat(wallet_balance).toLocaleString();
                $(".price").text("").text("$"+wallet_balance);
                $("#bal-error").text("")

                if($("#wallet_id").val() == ""){
                    $("#balance").text("");
                    $("#fee_div").hide();
                    $("#gft_amount_input").prop("disabled", true)
                }
                else{
                    $("#gft_amount_input").prop("disabled", false)
                    $("#fee_div").show();
                    //$("#balance").text(sNumber);
                    let bal = parseFloat($("#wallet_id option:selected").data("balance"))
                    let fee = parseFloat($("#wallet_id option:selected").data("fee"))
                    let amount = parseFloat($("#gft_amount_input").val())
                    let total = amount + parseFloat(fee)
                    if($("#gft_amount_input").val() != "") {
                        $("#amount").html('Amount: $' + parseFloat(amount).toFixed(2))
                        $("#fee2").html('Fee: $' + parseFloat(fee).toFixed(2));
                        $("#total").html('Total:  $' + parseFloat(total).toFixed(2));
                        $("#fee_div").show()
                        if(amount + fee <= bal|| $(this).val() == "")
                        {
                            $("#bal-error").text("")
                        }else{
                            $("#bal-error").text("").text("Insufficient Balance")
                        }
                    }
                }


            }
            $(butt).removeClass('m-loader m-loader--right m-loader--warning').prop("disabled", false);
            // $("#fee_div").hide();
        }
    });
    return false;
});
$("body").delegate(".con","click",function(){
    let wallet = $("#wallet_id").val()
    let bal = $("#wallet_id option:selected").data("balance")
    let fee = $("#wallet_id option:selected").data("fee")
    let gift_card_amount = $("#gft_amount_input").val()
    gift_card_amount = parseFloat(gift_card_amount).toFixed(2)
    let email = $("#check_recipient").val()
    let total_amount = parseFloat(fee) + parseFloat(gift_card_amount)
    let brand_image = $('#brand_img').attr('src');
    let status = $("#wallet_id").children(":selected").data("lstatus");
    let block_gift = $("#wallet_id").children(":selected").data("giftcard");

    $("#account_name").html($("#wallet_id option:selected").html())
    $("#email_add").html(email)
    $("#brand_name").html($('#brand_type').text())
    $('#confirm_img').attr('src',brand_image);
    $("#available_balance").html("$" + bal)
    $("#fee").html("$" +fee)
    $("#gift_card_amount").html("$" + gift_card_amount)
    $("#total_amount").html("$"+total_amount.toFixed(2))
    $("#remain_balance").html("$"+ (parseFloat(bal) - total_amount).toFixed(2))


    if(fee !== ""  && typeof fee !== "undefined" && gift_card_amount !== "" && email !== "" && validateEmail(email) && $("#errorTxt").text() == "" && $("#bal-error").text() == "" && !status &&  !block_gift){
        $('#giftCardComfirmation').modal("show")
        $('#buyGiftCardModal').modal("hide")
    }
    else{
        if ($("#wallet_id").val() == ""){ $("#wallets_error").html("Please Select any Wallet");}
        if (gift_card_amount === ""){ $("#amount_error").html("Please enter Amount");}
        if (!validateEmail(email)){ $("#remail_error").html("Please enter Email");}
    }
});

$("body").delegate("#confirm_gift", "click", function () {
    $("#giftCardComfirmation").modal("hide")
    $("#SuccessGiftModal").modal("show")
    $("#gc-form").submit();
});

//new_gift_card.js

$('.buy_button').attr('disabled', true);


if($('#wallet_id > option').length == 1){
    status = $('#wallet_id > option').attr("data-lstatus");
    block_gift = $('#wallet_id > option').attr("data-giftcard");
    if (status == "true"){
        $(".alert-body").css("display","block");
        $(".account-information").text("Location is Blocked.")
        // $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked </strong></div>');
    }else if (status == "false" && block_gift == true){
        $(".alert-body").css("display","block");
        $(".account-information").text("Gift Card Option Blocked.")
        // $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Gift Card Option Blocked </strong></div>');
    }else{
        $(".alert-body").css("display","none");
        //$("#errormessage").css('display','none').html('');
        wallet_balance=$("#wallet_id").children(":selected").data("balance");
        wallet_balance = parseFloat(wallet_balance).toFixed(2);
        wallet_balance = parseFloat(wallet_balance).toLocaleString();
        $(".price").text("").text("$"+wallet_balance);
        $("#bal-error").text("")
        // debugger
        // var sNumber = (wallet_balance).toLocaleString(undefined,
        //     {'minimumFractionDigits':2,'maximumFractionDigits':2});

        if($("#wallet_id").val() == ""){
            $("#balance").text("");
            $("#fee_div").hide();
            $("#gft_amount_input").prop("disabled", true)
        }
        else{
            $("#gft_amount_input").prop("disabled", false)
            $("#fee_div").show();
            //$("#balance").text(sNumber);
            let bal = parseFloat($("#wallet_id option:selected").data("balance"))
            let fee = parseFloat($("#wallet_id option:selected").data("fee"))
            let amount = parseFloat($("#gft_amount_input").val())
            let total = amount + parseFloat(fee)
            if($("#gft_amount_input").val() != "") {
                $("#amount").html('Amount: $' + parseFloat(amount).toFixed(2))
                $("#fee2").html('Fee: $' + parseFloat(fee).toFixed(2));
                $("#total").html('Total:  $' + parseFloat(total).toFixed(2));
                $("#fee_div").show()
                if(amount + fee <= bal|| $(this).val() == "")
                {
                    $("#bal-error").text("")
                }else{
                    $("#bal-error").text("").text("Insufficient Balance")
                }
            }
        }
    }

}
if($("#wallet_id").val() == ""){
    $("#balance").text("");
}


$("body").delegate("#wallet_id","change", function () {
    wallet_balance=$(this).children(":selected").data("balance");
    wallet_balance = parseFloat(wallet_balance).toFixed(2);
    wallet_balance = parseFloat(wallet_balance).toLocaleString();
    $(".price").text("").text("$"+wallet_balance);
    $("#bal-error").text("")
    // debugger
    // var sNumber = (wallet_balance).toLocaleString(undefined,
    //     {'minimumFractionDigits':2,'maximumFractionDigits':2});

    if($("#wallet_id").val() == ""){
        $("#balance").text("");
        $("#fee_div").hide();
        $("#gft_amount_input").prop("disabled", true)
    }
    else{
        $("#gft_amount_input").prop("disabled", false)
        $("#fee_div").show();
        //$("#balance").text(sNumber);
        let bal = parseFloat($("#wallet_id option:selected").data("balance"))
        let fee = parseFloat($("#wallet_id option:selected").data("fee"))
        let amount = parseFloat($("#gft_amount_input").val())
        let total = amount + parseFloat(fee)
        if($("#gft_amount_input").val() != "") {
            $("#amount").html('Amount: $' + parseFloat(amount).toFixed(2))
            $("#fee2").html('Fee: $' + parseFloat(fee).toFixed(2));
            $("#total").html('Total:  $' + parseFloat(total).toFixed(2));
            $("#fee_div").show()
            if(amount + fee <= bal|| $(this).val() == "")
            {
                $("#bal-error").text("")
            }else{
                $("#bal-error").text("").text("Insufficient Balance")
            }
        }
    }

    let status = $(this).children(":selected").data("lstatus");
    let block_gift = $(this).children(":selected").data("giftcard");
    if (status == true){
        $(".alert-body").css("display","block");
        $(".account-information").text("Location is Blocked.")
        //$("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked. </strong></div>');
    }else if (status == false && block_gift == true){
        $(".alert-body").css("display","block");
        $(".account-information").text("Gift Card Option Blocked.")
        //$("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Gift Card Option Blocked </strong></div>');
    }else{
        $(".alert-body").css("display","none");
        // $("#errormessage").css('display','none').html('');
    }
});

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
$("body").delegate('#gft_amount_input',"keyup",function (e) {
    let bal = parseFloat($("#wallet_id option:selected").data("balance"))
    let fee = parseFloat($("#wallet_id option:selected").data("fee"))
    let amount = parseFloat($(this).val())
    let total = amount + parseFloat(fee)
    if(amount + fee <= bal || $(this).val() == "")
    {
        $("#bal-error").text("")
    }else{
        $("#bal-error").text("").text("Insufficient Balance")
    }

    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57) ) {
        e.preventDefault();
    }

    if($(this).val() !== "") {
        $("#fee_div").show()
        $("#amount").html('Amount: $' + parseFloat(amount).toFixed(2))
        $("#fee2").html('Fee: $' + parseFloat(fee).toFixed(2));
        $("#total").html('Total:  $' + parseFloat(total).toFixed(2));
    }else{
        $("#fee_div").hide()
    }
    var min_val = $(".min_val").html();
    var max_val = $(".max_val").html();
    var input_val = $(this).val();
    var f = parseFloat(input_val);
    if(f > parseFloat(max_val) || f < parseFloat(min_val)){
        $('#errorTxt').html('').html('Amount must in between '+min_val+' to '+max_val);
        console.log(f);
    } else{
        $('#errorTxt').html('');
        console.log(f);
    }
    if(($(this).val()> 0) && (emailReg.test( $('.email_field').val() )) && ($('.email_field').val().length != 0) && ($('#errorTxt').text().length == 0)  && ($('#balance').text()!='0.00')){
        $('#buy_now').removeAttr('disabled');
    } else{
        $('#buy_now').prop('disabled', true );
    }
});
var globalTimeout = null;
//this function is use for two search giftcard and buygiftcard
$(document).on('keyup','.search_input_datatable',function (e) {

    e.preventDefault();

    $('.table-data').html('<div class="col-xl-12 ">\n' +
        '      <center  class="m-5 p-5">\n' +
        '        <img src="v2/merchant/loader.svg" class="loader_1" id="loader_1">\n' +
        '        <p class="No-data-available mt-4 mr-4">Fetching Data</p>\n' +
        '      </center>\n' +
        '    </div>')

    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    globalTimeout = setTimeout(function() {
        globalTimeout = null;
        var search_value = $('.search_input_datatable').val();
        search_value = search_value.trim()
        var url = $(this).attr('id') == "bg_search" ? '/v2/merchant/giftcards/buy_giftcard' : ''
        data = {"q[data_search]": search_value,filter: $("#filter option:selected").val(),search_form:'true'};
        $.ajax({
            url: url,
            type: 'GET',
            data: data,
            success: function (data) {
                $('.table-data').html('').html(data);
                $('.search_input_datatable').val(search_value);
                $('.search_input_datatable').focus();
                $("#m_table_1").DataTable();
                $("#page").selectpicker()
            }, error: function (data) {
            }
        });
    }, 5000);

});

$(document).on('focusout change keypress', '#wallet_id, #check_recipient, #check_recipient, #gft_amount_input', function(){
    console.log($(this).val());
    if($(this).is(':valid') && $(this).val() !== ""){
        $(this).css('border-color', '#efefef');
        $(this).prev().css('border-color', '#efefef');
        $(this).closest(".form-group").find("label").css('color', '#575962');
        $(this).closest(".form-group").find(".p_error").html("")
        $('#buy_submit').attr('disabled', false);

    }else{
        $('#buy_submit').attr('disabled', true);
        $(this).css('border-color', '#f4516c');
        $(this).prev().css('border-color', '#f4516c');
        //$(this).closest(".form-group").find("label").css('color', '#f4516c');
    }
});
$("body").delegate(".two_decimal", "input", function () {
    this.value = this.value.match(/\d{0,}(\.\d{0,2})?/)[0];
});
function validateEmail($email) {
    var emailReg = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return emailReg.test( $email );
}
$(document).ready(function () {
    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local

    });
});
$(document).on('submit', '#search_form_shared', function(e){
    e.preventDefault();
});