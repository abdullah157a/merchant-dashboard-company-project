$(document).ready(function () {
    var expired = $("#days_left").text();
    if(expired == "(Review Expired)"){
        $(".risk_accept_button").css("display","none");
        $(".risk_refund_button").css("display","none");
    }
});
var moderate_risk = $("#moderate_risk").text();
var high_risk = $("#high_risk").text();
var score = $(".risk-score").text();
if (score != ""){
    anychart.onDocumentReady(function () {
        // create data
        var data = [score];

        // set the gauge type
        var gauge = anychart.gauges.linear();
        // set the data for the gauge
        gauge.data(data);
        // set the layout
        gauge.layout('horizontal');
        // create a color scale
        var scaleBarColorScale = anychart.scales.ordinalColor().ranges(
            [
                {
                    from: 0,
                    to: moderate_risk,
                    color: ['#2AD62A', '#2AD62A']
                },
                {
                    from: moderate_risk,
                    to: high_risk,
                    color: ['#EB7A02', '#EB7A02']
                },
                {
                    from: high_risk,
                    to: 100,
                    color: ['#D81E05', '#D81E05']
                }
            ]
        );
        // create a Scale Bar
        var scaleBar = gauge.scaleBar(0);
        // set the height and offset of the Scale Bar (both as percentages of the gauge height)
        scaleBar.width('20%');
        scaleBar.offset('31.5%');
        // use the color scale (defined earlier) as the color scale of the Scale Bar
        scaleBar.colorScale(scaleBarColorScale);
        // add a marker pointer
        var marker = gauge.marker(0);
        var labels = marker.labels();
        labels.enabled(true);
        //labels.fontColor('#ff4040');
        labels.offsetX(-13).offsetY(-29);
        marker.width(20);
        // set the offset of the pointer as a percentage of the gauge width
        marker.offset('31.5%');
        // set the marker type
        marker.type('diamond');
        if (data >= 0 && data <= moderate_risk){
            var color = "#3da318";
        }else if(data >= moderate_risk && data <= high_risk) {
            var color = "#db6623";
        }else if(data >= high_risk && data <= 100){
            var color = "#bf2320";
        }

        labels.fontColor(color);
        marker.color(color);
        // set the zIndex of the marker
        marker.zIndex(10);
        // configure the scale
        var scale = gauge.scale();
        scale.minimum(0);
        scale.maximum(100);
        scale.ticks().interval(10);
        // configure the axis
        // var axis = gauge.axis();
        // axis.minorTicks(true)
        // axis.minorTicks().stroke('#cecece');
        // axis.width('1%');
        // axis.offset('29.5%');
        // axis.orientation('top');
        // format axis labels
        //axis.labels().format('{%value}%');
        // set paddings
        gauge.padding([0, 50]);
        // set the container id
        gauge.container('container');
        // initiate drawing the gauge
        gauge.draw();

    });
}
$(document).ready(function(){
    var clipboard = new Clipboard('.copy_transection_id');
    console.log(clipboard);
});
$(document).on("change","#refund_reason",function () {
    if ($(this).val() == 4){
        $("#refund_description").css("display","block");
        $("#exampleTextarea").prop("disabled",false);
    }else{
        $("#reason_detail").val($("#refund_reason option:selected").text());
        $("#refund_description").css("display","none");
        $("#exampleTextarea").prop("disabled",true);
    }
});
$(document).on("click","#refund_transaction_btn",function () {
    var txn_id = $(this).data("transaction-id");
    $.ajax({
        url: '/v2/merchant/accounts/get_refund_fee',
        type: 'GET',
        data: {trans_id: txn_id},
        success: function(data) {
            $("#fee").text(data.fee.toFixed(2));
            $("#refund_fee").val(data.fee.toFixed(2));
        }
    })
});
$(document).on("keyup","#refund_amount",function () {
    var input = parseFloat($(this).val());
    var fee =  parseFloat($("#fee").text());
    var total = input + fee
    $("#amount").text(input.toFixed(2));
    $("#total").text(total.toFixed(2));
});