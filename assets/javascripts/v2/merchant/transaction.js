
$(document).on('click', ".clickable_row", function () {
    window.location.href = $(this).data("path");
});

$(document).on('click', '#searchfilter', function () {
    $('.filter-form').show();
    $('#searchfilter').hide();
});
$(document).on('click', '#hide', function () {
    $('#searchfilter').show();
    $('.filter-form').hide();

});

$(document).on('click', "#reserve_search", function () {
    $("#reserve_offset").val(moment().local().format('Z'));
});
$(document).on('click', "#tip_search", function () {
    $("#tip_offset").val(moment().local().format('Z'));
});

var globalTimeout = null;
$("#filter_form input[type='checkbox']").prop("checked", true)
$(document).on('change','#filter_form input[type="checkbox"]',function () {
    $('#filter_form input[type="checkbox"]').attr("disabled", true);
    if($(this).attr("id") == "search_all"){
        if( $(this).is(":checked")) {
            $('#filter_form input[type="checkbox"]').prop("checked", false)
            $("#filter_form input[type='checkbox']").prop("checked", true)
        }else{
            $(this).prop("checked", false)
            $('#filter_form input[type="checkbox"]').prop("checked", false)
        }
    }else{
        $('#filter_form input[type="checkbox"]#search_all').prop("checked",false)
    }
    if($("#filter_form input:checkbox:not(:checked)").length == 1)// when all is check except all then all checkbox will automatically checked
    {
        $("#search_all").prop("checked",true)
    }
    if($('#filter_form input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
    {
        $("#filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    }
    search_call("status");
});
function search_call(type){
    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    $('#filter_form input[type="checkbox"]').attr("disabled", true);
    globalTimeout = null;
    data = {filter: $("#filter option:selected").val(),type: "success",wallet_id: $("#wallet_id").val()};
    $('#filter_form input[type="checkbox"]').each(function(index){
        if($('#filter_form input[type="checkbox"]')[index].checked){
            data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
        }
    });
    $.ajax({
        url: '/v2/merchant/accounts/account_transactions',
        type: 'GET',
        data: data,
        success: function (data) {
            $('#account_datatable').html(data);
            $("#m_table_1").DataTable();
            $("#page").selectpicker();
            $("#filter").selectpicker();
            $("#query_time1").selectpicker();
            $("#query_time2").selectpicker();
            $("#query_type_").selectpicker();
            $("#m_daterangepicker_2_success").daterangepicker({
                autoApply: true,

            }, function(a, t) {
                $("#m_daterangepicker_2_success .form-control").val(a.format("MM/DD/YYYY") + " / " + t.format("MM/DD/YYYY"));
            });


            if(type == "status"){
                $(".btn-status").trigger('click');
                $('#filter_form input[type="checkbox"]').attr("disabled", false);
            }

        }, error: function (data) {
        }
    });
}
