// require v2/merchant/vendors_bundle
// require v2/merchant/scripts_bundle
//= require v2/merchant/datatables_bundle
//= require v2/merchant/paginations
//= require v2/merchant/wizard
//= require v2/merchant/custom
//= require v2/merchant/custom1
//= require v2/merchant/custom_2
//= require v2/merchant/custom_3
//= require v2/merchant/custom_4
//= require v2/merchant/custom_5
//= require v2/merchant/login
//= require v2/merchant/bootstrap-select
//= require v2/merchant/bootstrap-datepicker
//= require v2/merchant/daterangerpicker
//= require jquery_ujs
//= require cable
//= require v2/merchant/apps
//= require select2
// require merchant/jquery.inputmask.bundle
//= require merchant/jquery.multiselect
//= require v2/merchant/card_js
//= require clipboard
//= require merchant/jstz.min

var lt = $('.timeUtc').each(function (i, v) {
    var gmtDateTime = moment.utc(v.innerText, 'MM-DD-YYYY HH:mm:ss A');
    var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
    v.innerText = local
});
var dt = $('.dateOnly').each(function (i, v) {
    var gmtDateTime = moment.utc(v.innerText, 'MM-DD-YYYY HH:mm:ss A');
    var local = moment(gmtDateTime).local().format('MM/DD/YYYY');
    v.innerText = local
});

$(document).on("change", ".jump_to_page", function () {
    $(this).parent('form').submit();
});
$("#close_flash").on('click',function () {
    $(".profile-subheader").fadeOut("slow");
});
$(document).on("change click", ".filter_section", function () {
    var id = this.id;
    if ($("#current_selected_filter").val() != id) {
        $(".filter_section").each(function (index) {
            if (this.id == id) {
                $(this).addClass("active");
                $("#current_selected_filter").val(this.id);
            } else {
                $(this).removeClass("active");
            }
        });
        var person_id = $("#selected_person").val();
        var type = $("#activity_log_type").val();
        filter_activity_logs(id, person_id, type, "");
    }
});
$(document).on("change click", ".filter_section", function () {
    var id = this.id;
    if ($("#current_selected_filter").val() != id) {
        $(".filter_section").each(function (index) {
            if (this.id == id) {
                $(this).addClass("active");
                $("#current_selected_filter").val(this.id);
            } else {
                $(this).removeClass("active");
            }
        });
        var date_val = $("#detail-value").val();
        var person_id = $("#selected_person").val();
        var type = $("#activity_log_type").val();
    }
});

//system update button remove all selected persons
$(document).on("click", "#system_update", function () {
    $('ul#mylist').find('li.active').each(function(index){
        $(this).removeClass("active")
    });
    $(this).addClass("active")
    var type = $("#activity_log_type").val();
    var a = $('ul.m-nav').find('li.active').attr("id")
    filter_activity_logs(a, "", type,"");
});
//add search functionlaity person list
$(document).on("keyup", "#search-input", function () {
    var value = $(this).val().toLowerCase();
    $("ul#mylist li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});

//multiple ids pass for filter
$(document).on("change click", "ul#mylist li", function () {
    $("#system_update").removeClass("active")
    // var id=$(this).data('id');
    var ids = []
    $(this).toggleClass("active");

    $('ul#mylist').find('li.active').each(function(index){
        ids.push($(this).data("id"))
    })
    // var interest = $('ul#credit').find('li.active').data('interest');

    // if (id != $("#selected_person").val()){
    //     $("#selected_person").val(id);
    var a = $('ul.m-nav').find('li.active').attr("id")
    var type = $("#activity_log_type").val();
    filter_activity_logs(a, ids, type,"");
    // }
});
function filter_activity_logs(filter, person, type, last_id, from_show_more,date) {
    user_type = $("#user_type").val()
    user_type == "merchant" ? log_url = '/v2/activity_logs/'+type : log_url = '/v2/partner/activity_logs/'+type

    $.ajax({
        url: log_url,
        type: 'GET',
        data: {search: "true", filter: filter, person: person, last_id: last_id, date_val: date},
        success: function(data) {
            if ($.trim(data.activity_counts)) {
                $('#year').text(data.activity_counts)
            }
            if (data.activity_logs.length > 0) {
                if (filter.length == 0 && last_id.length <= 0) {
                    $("#today").text(data.today);
                    $("#yesterday").text(data.yesterday);
                    $("#week").text(data.week);
                    $("#month").text(data.month);
                    $("#year").text(data.year);
                }
                if(from_show_more != "show_more"){
                    $("#activitylog").empty();
                }
                $.each(data.activity_logs, function (index, value) {
                    var gmtDateTime = moment.utc(value.created_at, 'YYYY-MM-DD HH:mm:ss');
                    var date = moment(gmtDateTime).local().format('MM/DD/YYYY');
                    var time = moment(gmtDateTime).local().format('hh:mm A');
                    var initials = value.user_name.split(' ').map(function(str) { return str ? str[0].toUpperCase() : "";}).join('');
                    $("#activitylog").append('<div class="row list" >\n' +
                        '        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12">\n' +
                        '          <div class="time">\n' +
                        '            ' + date + ' <br>\n' +
                        '            at ' + time + '\n' +
                        '          </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-12 col-xs-12 padding-0-right">\n' +
                        '          <div class="circle-1 pull-right">' + initials.substring(2,initials) + ' </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 padding-0-left">\n' +
                        '          <div class="time-1 time">' + value.user_name.substring(12,initials) + '</div>\n' +
                        '        </div>\n' +
                        '        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">\n' +
                        '          <div class="employee pl--2">' + value.note + '</div>\n' +
                        '        </div>\n' +
                        '      </div>')

                });

            }else{
                if(from_show_more != "show_more"){
                    $("#activitylog").html('')
                }
            }
            if (data.activity_logs.length == 10) {
                $("#activity_log_show_more").data('last_id', data.activity_logs[9].id).text('Show more');
            }else{
                $("#activity_log_show_more").data('last_id', '').text('');
            }
        },
        error: function(data){
        }
    });
}
$(document).on("click", "#activity_log_show_more", function () {
    var last_id=$(this).data('last_id');
    var filter = $("#current_selected_filter").val();
    var person = $("#selected_person").val();
    var type = $("#activity_log_type").val();
    if(last_id == ""){
        last_id = "null"
    }
    if (filter == "year_filter") {
        var date_val = $("#detail-value").val()
        filter_activity_logs(filter, person, type, last_id, "show_more",date_val)
    }else {
    filter_activity_logs(filter, person, type, last_id, "show_more")}
});
$(document).ready(function () {
    if (window.location.pathname == "/v2/merchant/accounts"){
        if($("#cookie_loader").val() == "1"){
            setTimeout(function () {
                $(".login-bg").fadeOut("slow").remove();
                $(".m-grid--root").removeClass("display_none").fadeIn("slow");
            }, 4000);
        }
    }
    if($('#user_tos').val()=='true'){
        $('.alert_messages').addClass('welcome_alert_messages');
    }

});

//export modal
// $("#echeckModal").scroll(function() {
//   $(".daterangepicker").css({
//     "margin-top": "-"+($("#echeckModal").scrollTop()) + "px",
//     "margin-left": ($("#echeckModal").scrollLeft()) + "px"
//   });
// });
// $(".range_inputs").last().css("border-right",0)
$(document).ready(function () {
    var tz = jstz.determine();
    document.cookie = "timezone=" + tz.name();
});