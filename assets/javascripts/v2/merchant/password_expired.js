$(document).ready(function () {
    // $("#ResetPasswordModel").modal('show');
    $("#ResetPasswordModel").modal({
        backdrop: 'static',
        keyboard: false
    });
});
$(document).on("click","#submit",function (e) {
    if( ($("#user_current_password").val() == "" || $("#user_password").val() == "" || $("#user_password_confirmation").val() == "") || $("#user_password").val() != $("#user_password_confirmation").val() ){
        if ($("#user_password").val() != $("#user_password_confirmation").val()) {
            $(".password-mismatch").removeClass("display_none");
        }
        return false
    }else{
        $(".password-mismatch").addClass("display_none");
    }
});
$(document).on('focusout', '#user_password, #user_password_confirmation', function(){

    if($("#user_password").val() !== "" && $("#user_password_confirmation").val() !== "" && $("#user_password").val() != $("#user_password_confirmation").val()){
        $(".password-mismatch").removeClass("display_none");

    }else{
        $(".password-mismatch").addClass("display_none");
    }
})
$('.current_password_show').on('click', function () {
    if($('#user_current_password').attr('type')=='text'){
        $('#user_current_password').attr('type', 'password');
        $('#current_password_close').show()
        $('#current_password_open').hide()
    }
    else{
        $('#user_current_password').attr('type', 'text');
        $('#current_password_close').hide()
        $('#current_password_open').show()
    }
});
$('.new_password_show').on('click', function () {
    if($('#user_password').attr('type')=='text'){
        $('#user_password').attr('type', 'password');
        $('#new_password_close').show()
        $('#new_password_open').hide()
    }
    else{
        $('#user_password').attr('type', 'text');
        $('#new_password_close').hide()
        $('#new_password_open').show()
    }
});
$('.confrim_password_show').on('click', function () {
    if($('#user_password_confirmation').attr('type')=='text'){
        $('#user_password_confirmation').attr('type', 'password');
        $('#confrim_password_close').show()
        $('#confrim_password_open').hide()
    }
    else{
        $('#user_password_confirmation').attr('type', 'text');
        $('#confrim_password_close').hide()
        $('#confrim_password_open').show()
    }
});
// $(document).on("click","#current_password_show",function (e) {
//     if($('#user_current_password').attr('type')=='text'){
//         $('#user_current_password').attr('type', 'password')
//         $(this).attr("src", window.location.origin +"/assets/v2/login/login-eye.svg");
//     }
//     else{
//         $('#user_current_password').attr('type', 'text');
//         $(this).attr("src", window.location.origin +"/assets/v2/login/login-open-eye.svg");
//     }
// });
// $(document).on("click","#new_password_show",function (e) {
//     if($('#user_password').attr('type')=='text'){
//         $('#user_password').attr('type', 'password');
//     }
//     else{
//         $('#user_password').attr('type', 'text');
//     }
// });
// $(document).on("click","#confirm_password_show",function (e) {
//     if($('#user_password_confirmation').attr('type')=='text'){
//         $('#user_password_confirmation').attr('type', 'password');
//     }
//     else{
//         $('#user_password_confirmation').attr('type', 'text');
//     }
// });

$(document).ready(function () {
    $("body").delegate('#user_password_confirmation','focusin ',function(){
        $('#tooltips').removeClass('tooltip_show');
        $('#tooltips').addClass('tooltip_hide');
    });

    $("body").delegate('#user_current_password','focusin ',function(){
        $('#tooltips').removeClass('tooltip_show');
        $('#tooltips').addClass('tooltip_hide');
    });



    $("body").delegate('#user_password','focusin ',function(){
        $('#tooltips').removeClass('tooltip_hide');
        $('#tooltips').addClass('tooltip_show');
    });
    $("body").delegate('#user_password','focusout ',function(){
        $('#tooltips').removeClass('tooltip_show');
        $('#tooltips').addClass('tooltip_hide');
    });

    $(document).on("keyup", "#user_password", function () {
        $('#tooltips').addClass('tooltip_show');
        var upperCase= new RegExp('[A-Z]');
        var lowerCase= new RegExp('[a-z]');
        var numbers = new RegExp('[0-9]');
        var v_digits = false;
        var v_numbers = false;
        var v_upcase = false;
        var v_downcase = false;
        var value = $("#user_password").val();
        if(value.length >= 8){
            $("#8_characters").removeClass('fa-times').addClass('fa-check btm');
            $("#8_characters").parent().removeClass('invalid').addClass('valid');
            v_digits = true;
        }else{
            v_digits = false;
            $("#8_characters").addClass('fa-times').removeClass('fa-check');
            $("#8_characters").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(upperCase)) {
            $("#upcase").removeClass('fa-times').addClass('fa-check btm');
            $("#upcase").parent().removeClass('invalid').addClass('valid');
            v_upcase = true;
        }else{
            v_upcase = false;
            $("#upcase").addClass('fa-times').removeClass('fa-check');
            $("#upcase").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(lowerCase)) {
            $("#downcase").removeClass('fa-times').addClass('fa-check btm');
            $("#downcase").parent().removeClass('invalid').addClass('valid');
            v_downcase = true;
        }else{
            v_downcase = false;
            $("#downcase").addClass('fa-times').removeClass('fa-check');
            $("#downcase").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(numbers)){
            $("#1_number").removeClass('fa-times').addClass('fa-check btm');
            $("#1_number").parent().removeClass('invalid').addClass('valid');
            v_numbers = true;
        }else{
            v_numbers = false;
            $("#1_number").addClass('fa-times').removeClass('fa-check');
            $("#1_number").parent().addClass('invalid').removeClass('valid');
        }
        if(v_digits && v_numbers && v_upcase && v_downcase){
            return true;
        }else{
            return false;
        }
    });
    $("body").delegate('.btn_confirm', 'click', function () {
        if ($('#user_password_confirmation').val() !== $('#user_password').val()) {
            { $("#error").html("Password confirmation not successful");}
        }
        return false;
    });
});