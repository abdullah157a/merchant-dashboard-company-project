//= require cable
//= require v2/merchant/bootstrap-select
//= require v2/merchant/bootstrap-datepicker


var total_fee = 0;
var total_amount = 0 ;
$(document).on('change','#filter',function () {
    $('#filter_form input[type="checkbox"]').each(function(index){
        if($('#filter_form input[type="checkbox"]')[index].checked){
            $('#instant_ach_filter').append("<input type='hidden' name="+$('#filter_form input[type="checkbox"]')[index].name+" value='"+
                     $('#filter_form input[type="checkbox"]')[index].value+"' />");
        }
    })
     $('#instant_ach_filter').submit();
})


$(document).on('hidden.bs.modal', function (event) {
  if ($('.modal:visible').length) {
    $('body').addClass('modal-open');
  }
});


$(document).ready(function () {
    if (window.location.href.indexOf("search") < 1) {
        $('#filter_form input[type="checkbox"]').prop("checked",true);
    }
});

$(document).ready(function () {
    $.validator.addMethod("regex", function(value, element, regexpr) {          
     return regexpr.test(value);
    }, "Please enter valid email");  
}); 

function custom_validate(){
    $("#new_check").validate({
        rules: {
            "check[recipient]": {
                required: true,
                email: true,
                regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
            },
            "check[name]": "required",
            "check[amount]": "required",
            "check[description]": "required"
        },
        messages: {
            "check[recipient]": {
                required: "Please enter email",
                email: "Please enter valid email",
                regex: "Please enter valid email"
            },
            "check[name]": "Please enter name",
            "check[amount]": "Please enter amount",
            "check[description]": "Please select memo"
        }
    })  
}  

$("body").delegate(".two_decimal", "input", function () {
    this.value = this.value.match(/\d{0,}(\.\d{0,2})?/)[0];
});

$(document).on("click", "div#checks_table  table  tbody  tr", function (ev) {
    id = $(this).data("id")
    $.ajax({
        url: "/v2/partner/checks/"+id,
        method: "GET",
        success: function(data){
            $("#ach_detail").modal("show");
            $(".ach_heading").text("E-Checks Details")
            $(".ach_amount").text("E-Check Amount")
            $("#account_name").text(data["account"]);
            $("#recipient").text(data["recipient"]);
            $("#account_number").text(data["number"]);
            $("#tango_detail").text(data["status"]);
            $("#tango_memo").text(data["memo"]);
            $("#tango_amount").text("$"+data["amount"]);
            $("#tango_fee").text("$"+data["fee"]);
            $("#tango_t_amount").text("$"+data["total_amount"]);
            $("#void_modal").attr("data-target","#echeck_void_modal")
            if(data["status"] == "PENDING"){
                $("#void_modal").show()
                $("#void_modal").attr("data-void_id",id)
            }else{
                $("#void_modal").hide()
            }
        }
    });
})

$(document).on("click","#yes_void",function(){
    id = $("#void_modal").data("void_id")
    $.ajax({
        url: '/v2/partner/checks/'+id,
        data: {status: "VOID"},
        method: "PATCH"
    })
});

$(document).on("click", ".show-dynamic-modal", function (ev) {
    ev.preventDefault();
    var url = $(this).data("url");
    var modal_selector = $(this).data("modal_selector");
    var modal_container_selector = $(this).data("modal_container_selector");
    var method = $(this).data("method");
    $.ajax({
        url: url,
        type: method,
        success: function (data) {
            $(modal_container_selector).empty().html(data);
            $(modal_selector).modal();
            $("#location_id").selectpicker();
        }
    });
    return false;
});
	

	$("body").delegate(".abs","click",function(){
                var name=$('#check_name').val();
        var amount=$('#check_amount').val();
        if ($("#location_id").val() == "") {
            $("#no-wallet-check").text("");
            $("#no-wallet-check").append("<div class='alert alert-danger'><p class='location_check_style'> Please select a wallet</p></div>");

        } else {
            custom_validate();
            if($("#new_check").valid() && $(".amountlimit_error").text()=== "" &&  $(".amount_error").text()=== "" && $("#errormessage").text() === "")  {
            	$('#newcheckModal').modal("hide");
                $('#ConfimP2cModal').modal({ show: true });
                amount = parseFloat(amount).toFixed(2);
                $("#send-amount-12").text("");
                $("#rec-name-12").text("");
                $("#send-amount-12").text(amount);
                $("#rec-name-12").text(name);

                $("span.available_balance").html("$"+parseFloat($("#balance").text().replace(/,/g, "")).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
	           	$("span.transfer_amount").html("$"+parseFloat($('#check_amount').val()).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
	           	$("span.fee").html("$"+parseFloat(total_fee).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
	           	$("span.total_amount").html("$"+parseFloat(total_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
	           	var remaining_balance = parseFloat($("#balance").text().replace(/,/g, ""))-parseFloat(total_amount)
	           	$("span.remaining_balance").html("$"+parseFloat(remaining_balance).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
	           	$(".description_memo").html($("#description").val())
	            $(".account_name").html($("#location_id option:selected").html())
	            $(".rec_email").html($("#check_recipient").val())
	            $("#trans_date").html($("#check_name").val())
	            $(".description_memo").html($("#check_description").val())
	            

            }
        }


    });

	$("#check_amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});

	$("body").delegate("#check_amount","keypress",function(event){
        if (((event.which != 46 || (event.which == 46 && $(this).val() == '')) ||
                $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    })

  $(document).on('keyup input','#check_amount', function () {
        var amount = $(this).val();
        var limit = parseFloat($('#checklimit').val());
        var limit_type = $('#checklimittype').val();
        var check_amount = parseFloat($('#check_amount').val());
        var fee_dollar = 0;
        var fee_perc = 0;
        var balance = $('#balance').text();
        balance = balance.replace(/,/g, "");
        balance = parseFloat(balance);

        if($('#fee_dollar1').val().length > 0 ){
            var fee_dollar = parseFloat($('#fee_dollar1').val());
        }
        if ($('#fee_perc1').val().length > 0) {
            var fee_perc = parseFloat($('#fee_perc1').val());
        }

        if (amount == ""){
            $("span#amount").html("$0.00");
            $("span#fee_amount").html("$0.00");
            $("span#total_amount").html("$0.00");
            $("#amount_limit_error2").text('');
            $('.amount_error').html('');
        } else if (amount <= 0) {
            $(this).val('');
        }
        else {
            var percentage = 0;
            percentage = (parseFloat(amount) * fee_perc)/100;
            total_fee = fee_dollar + percentage;
            total_amount = parseFloat(amount) + total_fee;

            $("span#amount").html("$"+parseFloat(amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $("span#fee_amount").html("$"+parseFloat(total_fee).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $("span#total_amount").html("$"+parseFloat(total_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

            if ((parseFloat(amount)+ parseFloat(total_fee.toFixed(2))).toFixed(2) > balance){
                $('.amount_error').html('').text("Insufficient balance!");

            }
            else{
                $('.amount_error').html('').text("");
            }

            if (limit_type && limit != 0){
                if ( check_amount > limit && limit_type == "Transaction"){
                    $('.amountlimit_error').html('').append('Exceeds Transaction Limit of $ '+limit.toFixed(2))
                }else{
                    $('.amountlimit_error').html('').append('')
                }
                if (limit_type == "Day"){
                    var total = parseFloat($('#checktotalcount').val());
                    total_sum = total + check_amount;
                    if (total_sum > limit){
                        $('.amountlimit_error').html('').append('Exceeds Daily Limit of $ '+limit.toFixed(2))
                    }else{
                        $('.amountlimit_error').html('').append('')
                    }
                }
            }



        }
    });


$("body").delegate("#location_id","change",function() {
   var locations_id = $( "#location_id option:selected" ).val();
    if(locations_id != ""){

    	// $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Retrieving Balance Information</strong></div>');
        $(".alert-body").css("display","block");
        $(".account-information").text("Please wait, getting account information.")
        $("#check_name").attr("disabled",true);
        $("#check_recipient").attr("disabled",true);
        $("#check_amount").val("").attr("disabled",true);
        $("#check_description").attr("disabled",true);
        $("#balance").html('0.00');
        $('.amountlimit_error').html('');
        $("span#amount").html("$0.00");
        $("span#fee_amount").html("$0.00");
        $("span#total_amount").html("$0.00");

        $("#no-wallet-check").html('');


       $.ajax({
       	  url: "/v2/partner/checks/location_data",
       	  type: "GET",
       	  data: {location_id: locations_id,type: "check"},
       	  success: function(data){
    		$('#fee_dollar1').val(data["fee_dollar1"])
		 	$("#close-btn2").prop("disabled", false); 
		  	$('#fee_perc1').val(data["fee_perc1"])
		  	$("#balance").text(data["balance"])

		  	if(data["location"] != null){

			  	if(data["location_block"] == true || data["location_block_withdraw"] == true){
			  		$("#check_name").prop("disabled", true);
			        $("#check_recipient").prop("disabled", true);
			        $("#check_amount").prop("disabled", true);
			        $("#check_description").prop("disabled", true);
			        $("#close-btn1").prop("disabled", true);
			        if(data["location_block"] == true ){
			        	// $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Location is Blocked</strong></div>');
			            $(".account-information").text("Location is Blocked")
                    }else{
			        	// $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Check is blocked for this Location</strong></div>');
			            $(".account-information").text("Check is blocked for this Location")
                    }
                    $("#close-btn2").attr("disabled", "disabled");
			  	}else if(data["location_block"] == false && data["location_block_withdraw"] == false){
			  		if($("#location_id").children(":selected").val() != 0) {
				        $("#check_name").prop("disabled", false);
				        $("#check_recipient").prop("disabled", false);
                        $("#check_name").removeClass('disabled')
                        $("#check_recipient").removeClass('disabled')
                        $("#check_amount").removeClass('disabled')
                        $("#check_description").removeClass('disabled')
				        $("#check_amount").prop("disabled", false);
				        $("#check_description").prop("disabled", false);
				        $("#errormessage").css('display', 'none').html('');
                        $(".alert-body").css("display","none");
				        $("#close-btn1").prop("disabled", false);
				    }
			  	}



			  	$('#checklimit').val(data["check_limit"])
	      		$('#checklimittype').val(data["check_limit_type"])

	      		if(data["check_limit_type"] == "Transaction"){
	      			var amount = $('#check_amount').val();
				    if (amount != ''){
				        if ( parseFloat(amount) > parseFloat(check_limit)){
				          $('.amountlimit_error').html('').append('Exceeds Transaction Limit of $ '+parseFloat(check_limit).toFixed(2))
				          if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
				            $('#close-btn1').attr('disabled', false);
				          }else{
				            $('#close-btn1').attr('disabled', true);
				          }
				        }else{
				          $('.amountlimit_error').html('').append('')
				          if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
				            $('#close-btn1').attr('disabled', false);
				          }else{
				            $('#close-btn1').attr('disabled', true);
				          }
				        }
				    }
	      		}else if(data["check_limit_type"]=="Day"){
	      			var total_count = data["total_count"] 
	      			$('#checktotalcount').val(total_count)
				    var amount = $('#check_amount').val();
			      	if (amount != ''){
				        var total_sum = parseFloat(amount)+ parseFloat(total_count)
				        if ( total_sum > parseFloat(check_limit) ){
				          $('.amountlimit_error').html('').append('Exceeds Daily Limit of $ '+parseFloat(check_limit).toFixed(2))
				          if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
				            $('#close-btn1').attr('disabled', false);
				          }else{
				            $('#close-btn1').attr('disabled', true);
				          }
				        }else{
				          $('.amountlimit_error').html('').append('')
				          if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0) && ( $('.amountlimit_error').text() == '')){
				            $('#close-btn1').attr('disabled', false);
				          }else{
				            $('#close-btn1').attr('disabled', true);
				          }
				        }
			    	}
	      		}else{
	      			var total_count = data["total_count"]
			      	$('#checktotalcount').val(total_count)
			      	$('.amountlimit_error').html('').append('')
			      	if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
			        	$('#close-btn1').attr('disabled', false);
			     	}
	      		}
	      	}else if(data["location"] != null && data["location"]["block_withdrawl"]){
	      		$("#check_name").prop("disabled", true);
			    $("#check_recipient").prop("disabled", true);
			    $("#check_amount").prop("disabled", true);
			    $("#check_description").prop("disabled", true);
			    $("#close-btn1").prop("disabled", true);
                $(".account-information").text("Check is blocked for this User")
			    // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Check is blocked for this User</strong></div>');
	      	}else{
	      		if($("#location_id").children(":selected").val() != 0) {
			      if(data["balance"] == 0 ){
			          $("#check_name").prop("disabled", true);
			          $("#check_recipient").prop("disabled", true);
			          $("#check_amount").prop("disabled", true);
			          $("#check_description").prop("disabled", true);
			          $("#close-btn1").prop("disabled", true);
			          $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Insufficient balance</strong></div>');
			      }else{

			      $("#check_name").prop("disabled", false);
			      $("#close-btn2").prop("disabled", false);
			          $("#check_recipient").prop("disabled", false);
                        $("#check_name").removeClass('disabled')
                        $("#check_recipient").removeClass('disabled')
                        $("#check_amount").removeClass('disabled')
                        $("#check_description").removeClass('disabled')
			          $("#check_amount").prop("disabled", false);
			          $("#check_description").prop("disabled", false);
			          $("#banks").prop("disabled", false);
			          $("#zip_code").prop("disabled", false);
			          $("#errormessage").css('display','none').html('');
                      $(".alert-body").css("display","none");
			  //        $("#no-wallet-check").css('display','none');
			      }
			    }else {
                    $(".alert-body").css("display","none");
			        $("#errormessage").css("display", "none")
			    }

			    if(data["system_tran_type"] == true){

			    	$('#checklimit').val(data["check_limit"])
				    $('#checklimittype').val(data["check_limit_type"])
				    var amount = $('#check_amount').val();
				    if (amount != ''){
				        if ( parseFloat(amount) > parseFloat(data["check_limit"])){
				            $('.amountlimit_error').html('').append('Exceeds Transaction Limit of $ '+parseFloat(data["check_limit"]).toFixed(2))
				            if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
				                $('#close-btn1').attr('disabled', false);
				            }else{
				                $('#close-btn1').attr('disabled', true);
				            }
				        }else{
				            $('.amountlimit_error').html('').append('')
				            if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
				                $('#close-btn1').attr('disabled', false);
				            }else{
				                $('#close-btn1').attr('disabled', true);
				            }
				        }
				    }


			    }else if(data["system_day_type"] == true){
			    	var check_limit = data["check_limit"]
			      	var check_limit_type = data["check_limit_type"]
			      	var total_count = data["total_count"]
			      	$('#checklimit').val(check_limit)
			      	$('#checktotalcount').val(total_count)
			      	$('#checklimittype').val(check_limit_type)
			      	var amount = $('#check_amount').val();
			      	if (amount != ''){
			          	var total_sum = parseFloat(amount)+ parseFloat(total_count)
			          	if ( total_sum > parseFloat(check_limit) ){
			              	$('.amountlimit_error').html('').append('Exceeds Daily Limit of $ '+parseFloat(check_limit).toFixed(2))
			              	if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
			                  	$('#close-btn1').attr('disabled', false);
			              	}else{
			                  	$('#close-btn1').attr('disabled', true);
			              	}
			          	}else{
			              	$('.amountlimit_error').html('').append('')
			              	if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0) && ( $('.amountlimit_error').text() == '')){
			                  	$('#close-btn1').attr('disabled', false);
			              	}else{
			                  	$('#close-btn1').attr('disabled', true);
			              	}
			          	}
			      	}
				}else{
					var check_limit = data["check_limit"]
				    var check_limit_type = data["check_limit_type"]
				    var total_count = data["total_count"]
				    $('#checklimit').val(check_limit)
				    $('#checktotalcount').val(total_count)
				    $('#checklimittype').val(check_limit_type)
				    $('.amountlimit_error').html('').append('')
				    if( ($('.recipient_field').val().length != 0 )  && ($('.decription_field').val().length != 0) && ($('.name_field').val().length != 0)  && ( $('.amountlimit_error').text() == '')){
				        $('#close-btn1').attr('disabled', false);
				    }
			    }
	      	}

            $("#submit_button").prop("disabled", true);
       	  }
          
       });
    }else{
        var merchant_user = $("#merchant_user").val();
        if(merchant_user == "true"){
            $('#bank_account_name').val($('#location_id').val());
        }
        $("#balance").html("0.00")
        $("#errormessage").css('display', 'none').html('');
        $(".alert-body").css("display","none");
        $("#check_amount").prop("disabled", true);
        $("#check_recipient").prop("disabled", true);
        $("#check_description").prop("disabled", true);
        $("#check_name").prop("disabled", true);
        $("#check_amount").val('');
        // $("#fee_deduct").text('');
        $("span#amount").html("$0.00");
        $("span#fee_amount").html("$0.00");
        $("span#total_amount").html("$0.00");
        $('.amount_limit_error').html('');
        $("#check_description").prop("disabled", true);
        $("#check_description").html('');
        $("#submit_button").prop("disabled", true);
        $("#bank_account_name").prop("disabled",true);
        if($("#location_id").children(":selected").val() != 0) {
            $(".alert-body").css("display","block");
            $(".account-information").text("Please wait, getting account information.")
            // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Retrieving Balance Information</strong></div>');
        }
    }
});

$("body").delegate("#confirm_check","click",function(){
    $("#new_check").submit();
});




var globalTimeout = null;

// $("body").delegate('#filter_form input[type="checkbox"]',"change",function(){
$(document).on('change','#filter_form input[type="checkbox"]',function () {
    $('#filter_form input[type="checkbox"]').attr("disabled", true);
    if($(this).attr("id") == "search_all"){
        if( $(this).is(":checked")) {
            $('#filter_form input[type="checkbox"]').prop("checked", false)
            $("#filter_form input[type='checkbox']").prop("checked", true)
        }else{
            $(this).prop("checked", false)
            $('#filter_form input[type="checkbox"]').prop("checked", false)
        }
    }else{
        $('#filter_form input[type="checkbox"]#search_all').prop("checked",false)
    }
    if($("#filter_form input:checkbox:not(:checked)").length == 1)// when all is check except all then all checkbox will automatically checked
    {
        $("#search_all").prop("checked",true)
    }
    if($('#filter_form input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
    {
        $("#filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    }
    search_call("status");
});

$(document).on('keyup','.search_input_datatable',function (e) {
	e.preventDefault();
    globalTimeout = setTimeout(function () {
         search_call("search");
    }, 2500);
});

function search_call(type){
	if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    globalTimeout = setTimeout(function() {
        globalTimeout = null;
        var search_value = $('.search_input_datatable').val();
        search_value = search_value.trim()
        var data = {"q[data_search]": search_value , filter: $("#filter option:selected").val(),search_form:'true',first_date: startdate, second_date: enddate};
        $('#filter_form input[type="checkbox"]').each(function(index){
            if($('#filter_form input[type="checkbox"]')[index].checked){
                data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
            }
        })
        var wallet_id = $("#wallet_id").val();
        data["wallet_id"] = wallet_id
        $.ajax({
            url: '/v2/partner/checks',
            type: 'GET',
            data: data,
            success: function (data) {
                $('.table-data').html('').html(data);
                if(type == "status"){
                    $(".btn-status").trigger('click');
                }
                update_total();
            }, error: function (data) {
            }
        });
    }, 500);
}

var startdate = '';
var enddate = '';
var custom = false;
$(document).on('click', '.ranges:eq(1) ul li', function () {
    if($(this).text() == "Yesterday"){
        startdate = moment().subtract(1, "days")._d;
        enddate = moment().subtract(1, "days")._d;
    } else if($(this).text() == "Today") {
        startdate = moment()._d;
        enddate = moment()._d;
    } else if($(this).text() == "Last 6 Days") {
        startdate = moment().subtract(5, "days")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Month to Date") {
        startdate = moment().startOf("month")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Previous month") {
        startdate =  new moment().subtract(1, 'months').date(1)._d
        enddate =  moment().subtract(1,'months').endOf('month')._d;
    }
    let filter = $("#filter option:selected").val();
    var search_value = $('.search_input_datatable').val();
    search_value = search_value.trim()
    if($(this).text() != "Custom"){
        custom = false;
        data = {"q[data_search]": search_value, filter: filter,custom: custom,first_date: startdate, second_date: enddate, search_form: 'true',wallet_id: $("#wallet_id").val()};
        $('#filter_form input[type="checkbox"]').each(function (index) {
            if ($('#filter_form input[type="checkbox"]')[index].checked) {
                data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
            }
        })
        $.ajax({
            url: '/v2/partner/checks',
            type: 'GET',
            data: data,
            success: function(data) {
                $('.table-data').html(data);
                update_total();
            }
        });
    }
});
$(document).on('click', '.applyBtn:eq(1)', function () {
    startdate = $("input[name=daterangepicker_start]").last().val();
    enddate = $("input[name=daterangepicker_end]").last().val();
    startdate = moment(startdate)._d
    enddate = moment(enddate)._d
    custom = true
    let filter = $("#filter option:selected").val();
    var search_value = $('.search_input_datatable').val();
    search_value = search_value.trim()
    data = {"q[data_search]": search_value, filter: filter,custom: custom,first_date: startdate, second_date: enddate, search_form: 'true',wallet_id: $("#wallet_id").val()};
    $('#filter_form input[type="checkbox"]').each(function (index) {
        if ($('#filter_form input[type="checkbox"]')[index].checked) {
            data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
        }
    })
    $.ajax({
        url: '/v2/partner/checks',
        type: 'GET',
        data: data,
        success: function(data) {
            $('.table-data').html(data);
            update_total();
        }
    });
});

function update_total(){
    $("#m_table_1").DataTable();
    $("#page").selectpicker();
    $("#total_pending").html("").html("$"+$(".total_pending").text());
    $("#total_void").html("").html("$"+$(".total_void").text());
    $("#total_failed").html("").html("$"+$(".total_failed").text());
    $("#total_paid").html("").html("$"+$(".total_paid").text());
    $("#total_unpaid").html("").html("$"+$(".total_unpaid").text());
    $("#total_paid_wire").html("").html("$"+$(".total_paid_wire").text());
}

$(document).on('keyup','.less_than_zero', function () {
    var amount_val = $(this).val();
    if( amount_val < 1 ) {
        $('.amountlimit_error').html('').text("Amount must be greater than 1");
        $(".btn_confirm").prop("disabled", true);
    }
    if(amount_val == ''){
        $('.amountlimit_error').html('').text('');
        $(".btn_confirm").prop("disabled", true);
    }
    if( amount_val >= 1 ) {
        $(".btn_confirm").prop("disabled", false);
    }
});

