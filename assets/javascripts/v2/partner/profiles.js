//= require intlTelInput
$(document).on('show.bs.modal', '#verifyPasswordModel', function () {
    $("#digitcodeInput").val("");
    $("#digitcodeInput").css('border-color', '#efefef');
    $(".p_error").html("")
    var send_code_type = $("#send_code_type").val();
    $(".send-code-again-text").attr('data-id', send_code_type);
})
$('#change_pass').prop('disabled', true);
$('#send').prop('disabled', true);
$(document).ready(function(){
    // $("#profileModel, #verifyPasswordModel").modal({
    //     show:false,
    //     backdrop:'static'
    // });
});
$('#digitcodeInput').on('keyup', function () {

    if ($('#digitcodeInput').val().length <= 5){
        $('#send').addClass('disable');
    }else{
        $('#send').removeClass('disable');
    }
});

$("#phone_number").intlTelInput({
    formatOnInit: true,
    separateDialCode: true
});

$('#2SV').on('change', function () {
    var set = $('#2SV').is(":checked");
    $('#2SV').attr("disabled", true);
    $.ajax({
        url: "/v2/partner/profiles/update_two_step_verification",
        method: 'get',
        data: {two_step: set},
        success: function (data) {
            $('#2SV').attr("disabled", false);
            console.log(set);
        }, error: function (data) {
            $('#2SV').attr("disabled", false);
        }
    });
});
$('#create_pass').on('click', function () {
    if ($("#user_password").val() == "") {
        $("#pass-error").html("Please Enter a Password");
    } else if ($("#user_password_confirmation").val() == "") {
        $("#error").html("Please Enter a Confirm New Password");
    } else if ($("#user_password").val() != $("#user_password_confirmation").val()){
        $("#error").html("Passwords do not match.");
    }else{
        $('#confirmProfileModal').modal("show")
        $('#profileModel').modal("hide")
    }
})
$(document).on('focusout change keypress', '#user_password,#digitcodeInput, #user_password_confirmation', function(){

    if($(this).val() !== ""){
        $(this).css('border-color', '#efefef');
        $(this).closest(".form-group").find(".p_error").html("")

    }else{
        $(this).css('border-color', '#f4516c');
        $(this).prev().css('border-color', '#f4516c');
        //$(this).closest(".form-group").find("label").css('color', '#f4516c');
    }
})
$('.close_class, #cancel_btn').on('click',function () {
    $('#user_password').val("");
    $('#user_password_confirmation').val("");
    $('#digitcodeInput').val("");
    $("#strength_human").html('').append("")
    $('#change_pass').prop('disable', true);
    $('#send').prop('disable', true);

});

$(document).ready(function () {
    // $("#user_password").on('focusin keyup',function () {
    //     $('.strength').css('display', "block");
    // });
    // var length, one_number, upper_case, lower_case;
    // $(document).on("keyup", "#user_password", function () {
    //     var password = $(this).val();
    //     if (password.length >= 8 && password.length <=20){
    //         length = true
    //     }else{
    //         length = false
    //     }
    //     if (/[0-9]/.test(password)){
    //         one_number = true
    //     }
    //     else{
    //         one_number = false
    //     }
    //     if (/[A-Z]/.test(password)){
    //         upper_case = true
    //     }
    //     else{
    //         upper_case = false
    //     }
    //     if (/[a-z]/.test(password)){
    //         lower_case = true
    //     }
    //     else{
    //         lower_case = false
    //     }
    // });
    // $(document).on("keyup", "#user_password", function () {
    //     if($('#user_password').val()!=$('#user_password').val()){
    //
    //     }
    // });
    // $(document).on("keyup", "#user_password_confirmation", function () {
    //     $('#tooltips').hide();
    // });
    // $('#user_password ,#user_password_confirmation').on('keyup', function () {
    //console.log($('#user_password').val() + "-------" + $('#user_password_confirmation').val())
    // if ($('#user_password').val() == $('#user_password_confirmation').val() && $('#user_password').val() != "" && $('#user_password_confirmation').val() != "" && length == true && one_number == true && upper_case == true && lower_case == true) {
    //$('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password</label>");

    //         $('#change_pass').removeClass('disable');
    //         $('.strength').css('display', "none");
    //         $('#error').html("");
    //
    //             $('#change_pass').removeClass('disable');
    //             $('.strength').css('display', "none");
    //             $('#error').html("");
    //             $("#create_pass").attr("pass","valid")
    //         } else {
    //             if ($("#user_password_confirmation").val() != ""){
    //                 $('#error').html("").append("<span class='text-danger error_class'> Passwords do not match. Try again.</span>");
    //             }
    //             $("#create_pass").attr("pass","invalid")
    //             $('#change_pass').addClass('disable');
    //         }
    //
    //     });
    $(".send_pin").on('click', function () {
        $(this).addClass("change_color");
        id=$(this).data('id');
        if($("#send_pin").hasClass("disable")){
            e.preventDefault();
        }
        $.ajax({
            url: "/v2/partner/profiles/update_pin",
            method: 'post',
            data: {send_with:id},
            success: function () {
                setTimeout(function () {
                    if (id == "first_send") {
                        // $("#user_p").val($('#user_password').val());
                        // $("#user_con_p").val($('#user_password_confirmation').val());
                        // $('#close_modal').click();
                        $(".close").click();
                        $("#show_pin_code_modal").click();
                    }
                    else if(id=='send_mobile'){
                        $('#email_code').text('')
                        $('#code_sent').text('').text('Code Sent');

                    }else if(id == "email"){
                        $('#code_sent').text('')
                        $('#email_code').text('').text('Code Sent');
                        $(".close").click();
                        $("#show_pin_code_modal").click();
                    }
                    else{
                        $('#code_sent').text('')
                        $('#email_code').text('').text('Code Sent');
                    }

                }, 1000);
                //$('#verifyPasswordModel').modal('show');
            }, error: function (result) {
                $('#pincode_button').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger');
                setTimeout(function () {
                    $('#pincode_button').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                }, 1500);
                $(".error-pincode").show();
            }
        });
    });

    $("#send").on('click', function (e) {
        let code = $("#digitcodeInput").val();
        if($("#send").hasClass("disable")){
            e.preventDefault();
        }

        if(code !== "") {
            $.ajax({
                url: "/v2/partner/profiles/update_pin",
                method: 'post',
                data: {pincode: code},
                success: function (data) {
                    // $('#verifyPasswordModel').modal('hide');
                    // $('#profileModel').modal('show');
                    $(".close").click();
                    $('#set_pass').click();
                }, error: function (data) {
                    $(".error-pincode").html("").append("<b>Wrong PIN Code</b>");
                }
            });
        }
        else{
            $(".valid-pincode").html("").append("<b>Please Enter the PIN Code</b>");
        }

    });
    $('#password_show').on('click', function () {
        if($('#user_password').attr('type')=='text'){
            $('#user_password').attr('type', 'password');
            $(this).attr("src", window.location.origin +"/assets/v2/login/login-eye.svg");
        }
        else{
            $('#user_password').attr('type', 'text');
            $(this).attr("src", window.location.origin +"/assets/v2/login/login-open-eye.svg");
        }
    });
    $('#confirm_show').on('click', function () {
        if($('#user_password_confirmation').attr('type')=='text'){
            $('#user_password_confirmation').attr('type', 'password');
            $(this).attr("src", window.location.origin +"/assets/v2/login/login-eye.svg");
        }
        else{
            $('#user_password_confirmation').attr('type', 'text');
            $(this).attr("src", window.location.origin +"/assets/v2/login/login-open-eye.svg");
        }
    });

    // $(document).on('hide.bs.modal','#profileModel', function () {
    //     $("#user_password").val('');
    //     $("#user_password_confirmation").val('');
    //     $('.strength').css('display', "none");
    //     $('.error_class').text('');
    //     //Do stuff here
    // });

    var globalTimeout = null;
    $(document).on('keyup','.search_input_datatable',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            var search_value = $('.search_input_datatable').val();
            var filter_data = "";
            if(filter != ""){
                filter_data = filter;
            }else{
                filter_data = 10;
            }
            $.ajax({
                url: '/v2/partner/profiles/employee_list',
                type: 'GET',
                data: {q: search_value,filter: 10,search:'true'},
                success: function (data) {
                    $('#employee_list').html('').html(data);
                    $('.search_input_datatable').val(search_value);
                    $('.search_input_datatable').focus();
                }, error: function (data) {


                }
            });

        }, 1500);
    });
    $("body").delegate('#phone_number','keyup',function(){

        $('#phone-error').css('color','red');
        country_code = parseInt($('.selected-dial-code').text())
        var phone_number = $("#phone_number").val();
        phone_number =  country_code + phone_number
        var reg = new RegExp('^[0-9]+$');
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        var user_id = $("#user_id").val();
        if(phone_number.length > 0){
            $('#phone-error').html('');
            if(phone_number.match(reg)) {
                $.ajax({
                    url: '/v2/partner/base/verifying_user',
                    method: 'get',
                    // dataType: 'json',
                    data: {phone_number: country_code + $(this).val()  , user_id: user_id},
                    success: function (result) {

                        if (result.success == "notverified") {
                            if ($('#phone-error').text() == "") {
                                $('#phone-error').html('').html(result.message);
                                $('#save_update-btn').prop('disabled', true);
                            }
                        } else {
                            if ($("#error_email").text()== "" && $("#phone-error").text() == "") {
                                $('#phone-error').html('');
                                $('#save_update-btn').prop('disabled', false);
                            } else {
                                $('#phone-error').html('');
                                $('#save_update-btn').prop('disabled', true);
                            }
                        }
                    },
                    error: function (result) {
                        $('#phone-error').html('');
                        $('#save_update-btn').prop('disabled', true);
                    }
                });
            }else{
                $('#save_update-btn').prop('disabled', true);
                $("#phone-error").text('').text('Must be Number');
            }
        }else{
            $('#save_update-btn').prop('disabled', true);
            $("#phone-error").text('').text('Please Enter Phone Number');
        }
    });
    $("body").delegate('#user_email','keyup',function() {
        var email = $('#user_email').val();
        var user_id = $('#user_id').val();
        var reg = new RegExp('^([a-zA-Z0-9_+\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$');
        if( email.length > 0){
            if(email.match(reg)){
                $.ajax({
                    url: '/v2/partner/base/verifying_user',
                    method: 'get',
                    dataType: 'json',
                    data: {email: email, user_id: user_id, merchant: true},
                    success: function (result) {
                        if (result.success == "notverified") {
                            $('#error_email').html('').html('<span style="color: red;" >A user with this email already exists</span>');
                            $('#email').addClass('is-invalid');
                            $('#save_update-btn').prop('disabled', true);
                        } else {
                            if ($("#error_email").text() == "" && $("#phone-error").text() == "") {
                                $('#error_email').html('')
                                $('#email').removeClass('is-invalid');
                                $('#save_update-btn').prop('disabled', false);
                            } else {
                                $('#error_email').html('');
                                $('#email').removeClass('is-invalid');
                                $('#save_update-btn').prop('disabled', true);
                            }
                        }
                    },
                    error: function (result) {
                        $('#email').removeClass('is-invalid').addClass('is-valid')
                        $('#error_email').html('');
                    }
                });
            }else{
                $('#error_email').html('').html('<span style="color: red;" >Please Enter a Valid Email</span>');
                $('#email').addClass('is-invalid');
                $('#save_update-btn').prop('disabled', true);
            }
        }else{
            $('#error_email').html('').html('<span style="color: red;" >Please Enter email</span>');
            $('#email').addClass('is-invalid');
            $('#save_update-btn').prop('disabled', true);
        }
    });

});

$("#confirm_send").on('click', function () {
    $('form#pass-form').submit();
});
$(".send_pin").on('click', function () {
    globalTimeout = setTimeout(function () {
        globalTimeout = null;
        if($(".send_pin").hasClass("change_color")){
            $(".send_pin").removeClass("change_color");
        }
    }, 100);
});

$("body").delegate('.only_numbers','input', function() {
    this.value = this.value.match(/\d*/)[0];
});
$('#phone_number').inputmask('Regex', {regex: "^[0-9]+$"});


$(document).ready(function () {

    $("body").delegate('#user_password_confirmation','focusin ',function(){
        $('#tooltips').removeClass('tooltip_show');
        $('#tooltips').addClass('tooltip_hide');
    });

    $(document).on("keyup", "#user_password", function () {
        $('#tooltips').addClass('tooltip_show');
        var upperCase= new RegExp('[A-Z]');
        var lowerCase= new RegExp('[a-z]');
        var numbers = new RegExp('[0-9]');
        var v_digits = false;
        var v_numbers = false;
        var v_upcase = false;
        var v_downcase = false;
        if(value.length >= 8){
            $("#8_characters").removeClass('fa-times').addClass('fa-check btm');
            $("#8_characters").parent().removeClass('invalid').addClass('valid');
            v_digits = true;
        }else{
            v_digits = false;
            $("#8_characters").addClass('fa-times').removeClass('fa-check');
            $("#8_characters").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(upperCase)) {
            $("#upcase").removeClass('fa-times').addClass('fa-check btm');
            $("#upcase").parent().removeClass('invalid').addClass('valid');
            v_upcase = true;
        }else{
            v_upcase = false;
            $("#upcase").addClass('fa-times').removeClass('fa-check');
            $("#upcase").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(lowerCase)) {
            $("#downcase").removeClass('fa-times').addClass('fa-check btm');
            $("#downcase").parent().removeClass('invalid').addClass('valid');
            v_downcase = true;
        }else{
            v_downcase = false;
            $("#downcase").addClass('fa-times').removeClass('fa-check');
            $("#downcase").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(numbers)){
            $("#1_number").removeClass('fa-times').addClass('fa-check btm');
            $("#1_number").parent().removeClass('invalid').addClass('valid');
            v_numbers = true;
        }else{
            v_numbers = false;
            $("#1_number").addClass('fa-times').removeClass('fa-check');
            $("#1_number").parent().addClass('invalid').removeClass('valid');
        }
        if(v_digits && v_numbers && v_upcase && v_downcase){
            return true;
        }else{
            return false;
        }
    });
    $("body").delegate('.btn_confirm', 'click', function () {
        if ($('#user_password_confirmation').val() !== $('#user_password').val()) {
            { $("#error").html("Password confirmation not successful");}
        }
        return false;
    });
});

$(".send_code_option").on("change",function () {
    if ($(this).val() == "text"){
        $(".btn_send_code").attr('data-id','first_send');
        $("#send_code_type").val("first_send");
    } else{
        $(".btn_send_code").attr('data-id','email');
        $("#send_code_type").val("email");
    }
});

$("#merchant_profile").submit(function(){
    country_code = parseInt($('.selected-dial-code').text())
    number = parseInt($("#phone_number").val());
    $("#phone_number_to_save").prop("value","" + country_code + number);
});
