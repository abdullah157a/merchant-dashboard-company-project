//= require cable
//= require v2/merchant/bootstrap-select
//= require v2/merchant/bootstrap-datepicker

$(document).on('show.bs.modal', '#newAchModal', function () {
    if ($("#bank_account_name").data("bank_present") == false){
        $(".alert-body").css("display","block");
        $(".account-information").text("Bank Not Added, Please submit a support ticket with ACH Bank Information.");
        $("#amount").attr('disabled',true);
        $("#description").attr('disabled',true);
        $("#bank_account_name").attr('disabled',true);
        $("#location_id").attr('disabled',true);
        $("#submit_button").attr('disabled',true);
    }
});

function custom_validate(){
    $("#ach-form").validate({
        rules: {
            amount: "required",
            bank_account_name: "required",
            description: "required",
            location_id: "required"
        },
        messages: {
            amount: "Please enter amount",
            bank_account_name: "Please select bank account",
            description: "Please enter memo",
            location_id: "Please select location"
        }
    })  
}  

$(document).on('hidden.bs.modal', function (event) {
  if ($('.modal:visible').length) {
    $('body').addClass('modal-open');
  }
});

$(document).on('change','#filter',function () {
    data = {}
    $('#filter_form input[type="checkbox"]').each(function(index){
        if($('#filter_form input[type="checkbox"]')[index].checked){
            $('#instant_ach_filter').append("<input type='hidden' name="+$('#filter_form input[type="checkbox"]')[index].name+" value='"+
                     $('#filter_form input[type="checkbox"]')[index].value+"' />");
        }
    })
    $(".filter-option").trigger('click');
    $('#instant_ach_filter').submit();
    //search_call("entries")
})

$(document).on("click", "div#ach_table table  tbody  tr", function (ev) {
    id = $(this).data("id")
    $.ajax({
        url: "/v2/partner/instant_ach/"+id,
        method: "GET",
        success: function(data){
            $("#ach_detail").modal("show");
            $(".ach_heading").text("ACH Details")
            $("#account_name").text(data["account"]);
            $("#recipient").text(data["recipient"]);
            $("#account_number").text(data["ach_id"]);
            $("#ach_id").text("ACH ID");
            $("#tango_detail").text(data["status"]);
            $("#tango_memo").text(data["memo"]);
            $("#tango_amount").text("$"+data["amount"]);
            $("#tango_fee").text("$"+data["fee"]);
            $("#tango_t_amount").text("$"+data["total_amount"]);
            if(data["status"] == "PENDING"){
                $("#void_modal").show()
                $("#void_modal").attr("data-void_id",id)
            }else{
                $("#void_modal").hide()
            }

        }
    });
})
$(document).on("click","#yes_void",function(){
    id = $("#void_modal").data("void_id")
    $.ajax({
        url: '/v2/partner/checks/'+id,
        data: {status: "VOID"},
        method: "PATCH"
    })
});

$(document).on("click", ".show-dynamic-modal", function (ev) {
    ev.preventDefault();
    var url = $(this).data("url");
    var modal_selector = $(this).data("modal_selector");
    var modal_container_selector = $(this).data("modal_container_selector");
    var method = $(this).data("method");
    $.ajax({
        url: url,
        type: method,
        success: function (data) {
            $(modal_container_selector).empty().html(data);
            $(modal_selector).modal();
            $("#location_id").selectpicker();
            $("#bank_account_name").selectpicker()
        }
    });
    return false;
});


$(document).ready(function () {
    if (window.location.href.indexOf("search") < 1) {
        $('#filter_form input[type="checkbox"]').prop("checked",true)
    }
    $("#location_id").selectpicker()
    var total_fee;
    var total_amount;
    var amount_exceed_limit = false ;
     // add the rule here
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
       return arg !== value;
    }, "Value must not equal arg.");


     $("#ach-form").validate({
        rules: {
            amount: {
                required: true,
                // minlength: 8
            },
            bank_account_name: { valueNotEquals: "default" },
            description: "required",
            location_id: {valueNotEquals: "default" }
        },
        messages: {
            amount: {
                required: "Please enter amount"
            },      
            description: "Please enter some data",
            bank_account_name:  { valueNotEquals: "Please select an item!" },
            location_id: {valueNotEquals: "Please select an item!" }
        },
        errorPlacement: function(error, element) {
            // debugger
            if (element.hasClass('m-bootstrap-select')) {
                error.insertAfter(element.next('.btn'));
            } else {
                error.insertAfter(element);
            }
        }
    })

    $('body').delegate('#bank_account_name' , 'change', function() {
        $(".bank_dropdown #bank_account_name-error").remove()
    });
//-----------------------new_form.html.erb-----------------//

    $(".account-number").inputmask('Regex', {regex: "^[0-9]+$"});
    $('#export_class').attr('disabled',false);
    jQuery(function() {
        jQuery('form').bind('submit', function() {
            //jQuery(this).find(':disabled').removeAttr('disabled');
            $("#bank_account_name").removeAttr("disabled");
        });
    });
    $(document).on('click','.close_ach',function () {
        $('.modal').removeClass('fade');
        $('.confirm_send_modal').modal('hide');
        $('.modal').addClass('fade');
    });

    $("#amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
        if($("#location_id").children(":selected").val() == 0) {
            $("#amount").prop("disabled", true);
            $("#description").prop("disabled", true);
            $("#submit_button").prop("disabled", true);
        }
        // validateACHForm('ach-form');
        $("body").delegate(".abs","click",function(event){
            custom_validate();
        var amount = $('#amount').val();
        $("#fee_deduct").css({"color": "black"});
        if ($('#ach-form').valid() && $('.amount_limit_error').text() == '' && $("#amount_limit_error2").text() == ''){
            $('#AchConfirmModal').modal("show");
            $('#newAchModal').modal("hide")
            $("span#available_balance").html("$"+parseFloat($('#balance').text().replace(/,/g, "")).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
            $("span#transfer_amount").html("$"+parseFloat($('#amount').val()).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
            $("span#fee").html("$"+parseFloat(total_fee).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
            $("span#total_amount").html("$"+parseFloat(total_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
            var remaining_balance = parseFloat($("#balance").text().replace(/,/g, ""))-parseFloat(total_amount)
            $("span#remaining_balance").html("$"+parseFloat(remaining_balance).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
            $("#description_memo").html($("#description").val())
            $("#bank_account").html($("#bank_account_name option:selected").html())
            $("#account_name").html($("#location_id option:selected").html())
        }else{
            if($(".bank_dropdown #bank_account_name-error").length == 1){
                // $(".bank_dropdown #bank_account_name-error").remove();
                $("#bank_account_name-error").insertAfter(".bank_dropdown .dropdown-menu")
            }
            if($(".location_dropdown #location_id-error").length == 1){
                $("#location_id-error").insertAfter(".location_dropdown .dropdown-menu")
            }
            $(".dropdown-menu #bank_account_name-error").remove()
            event.preventDefault();
        }
        $(document).on('click','.submit_check',function () {
            submit_ach()
        })
    });

    $("body").delegate("#amount","keypress",function(event){
        if (((event.which != 46 || (event.which == 46 && $(this).val() == '')) ||
                $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    })
    $("body").delegate(".two_decimal", "input", function () {
        this.value = this.value.match(/\d{0,}(\.\d{0,2})?/)[0];
    });


    $("body").delegate("#amount","keyup input",function(){
        var amount = $(this).val();
        var fee_dollar = 0;
        var fee_perc = 0;
        var balance = $('#balance').text();
        balance = balance.replace(/,/g, "");
        balance = parseFloat(balance);
        var ach_limit = parseFloat($('#achlimit').val());
        var ach_type = $('#achlimittype').val();
        var ach_done = 0;

        if ($('#achtotalcount').val().length > 0) {
            var ach_done = parseFloat($('#achtotalcount').val());
        }
        if($('#fee_dollar').val().length > 0 ){
            var fee_dollar = parseFloat($('#fee_dollar').val());
        }
        if ($('#fee_perc').val().length > 0) {
            var fee_perc = parseFloat($('#fee_perc').val());
        }
        if (amount == ""){
            $("span#amount").html("$0.00");
            $("span#fee_amount").html("$0.00");
            $("span#total_amount").html("$0.00");
            $('.amount_limit_error').html('');
            $('#amount_limit_error2').html('');

        } else if (amount == 0) {
            $(this).val('');
        } else {
            var percentage = 0;
            percentage = (parseFloat(amount) * fee_perc)/100;
            total_fee = fee_dollar + percentage;
            total_amount = parseFloat(amount) + total_fee;
            $("span#amount").html("$"+parseFloat(amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $("span#fee_amount").html("$"+parseFloat(total_fee).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $("span#total_amount").html("$"+parseFloat(total_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            if (parseFloat(amount)+ parseFloat(total_fee) > balance){
                $('.amount_limit_error').html('').text("Insufficient Balance");
                $("#close-btn1").prop("disabled", true);
                $('#bank_account_name-error').each(function() {
                $(this).insertAfter($(this).parent().find('.btn'));
                });
                $('#location_id-error').each(function() {
                    $(this).insertAfter($(this).parent().find('.btn'));
                });
                amount_exceed_limit = true;
            }else{
                $('.amount_limit_error').html('')
            }
            if(ach_type == "Day"){
                total = parseFloat(amount) + ach_done;
                if(total > ach_limit && ach_limit > 0){
                    $('#amount_limit_error2').html('').append('Exceeds Daily Limit of $ ' + ach_limit.toFixed(2))
                    $("#close-btn1").prop("disabled", true);
                    amount_exceed_limit = true;
                }else{
                    $('#amount_limit_error2').html('');
                    $("#close-btn1").prop("disabled", false);
                     amount_exceed_limit = false;
                }
            }else{
                if(amount > ach_limit && ach_limit > 0 ){
                    $('#amount_limit_error2').html('').append('Exceeds Transaction Limit of $ ' + ach_limit.toFixed(2))
                    $("#close-btn1").prop("disabled", true);
                    amount_exceed_limit = true;
                }else{
                    $('#amount_limit_error2').html('');
                    $("#close-btn1").prop("disabled", false);
                    amount_exceed_limit = false;
                }
            }

        }
    });

    //---------------------------intant_ach/new.html.erb-----------------

    $('#cover-spin').fadeOut("slow");
    $('#test_submit').on('click',function () {
        $("#ach_req_form").click();
    });

    $('.field input, .field textarea').keyup(function () {
        var empty = false;
        $('.field input').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        $('.field textarea').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('.actions input').attr('disabled', 'disabled');
        } else {
            if ($('#myInput').val() == $('#bank_account').val() && $('#bank_account').val() != "" && $('#myInput').val() != "") {
                $('.actions input').removeAttr('disabled');
            }
        }
    });

    $('#bank_account, #myInput').on('keyup', function () {
        if ($('#myInput').val() == $('#bank_account').val() && $('#bank_account').val() != "" && $('#myInput').val() != "") {
            $('#account_p_c').hide();
            $('.ach-submit-btn').removeAttr('disabled');
        } else {
            $('#account_p_c').show();
            $('.ach-submit-btn').prop('disabled','disabled');
            $('.actions input').prop('disabled', true);
        }
        if ($('#bank_account').val() == "" && $('#myInput').val() == "") {
            $(':input[type="submit"]').prop('disabled', false);
        }
    });

    $("#bank_name, #bank_routing, #bank_account, #myInput").on('keyup', function () {
    if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
        this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
    }
});

    $("#email_address").on('keyup', function () {
    if (this.value.match(/[^a-zA-Z0-9@. ]/g)) {
        this.value = this.value.replace(/[^a-zA-Z0-9@. ]/g, '');
    }
});

    // $(document).on('keyup', '#amount', function () {
    $("body").delegate("#amount","keyup",function(){
    $(this).val($(this).val().replace(/[^0-9. ]/g, ''));
    })

    $(document).on("change","#location_id",function () {
    // $("body").delegate("#location_id","change",function(){
        var merchant_user = $("#merchant_user").val();
        if(merchant_user == "true"){
            $('#bank_account_name').val($('#location_id').val());
        }
        $("#errormessage").css('display', 'none').html('');
        $(".alert-body").css("display","none");
        $("#amount").prop("disabled", true);
        $("#amount").val('');
        // $("#fee_deduct").text('');
        $("span#amount").html("$0.00");
        $("span#fee_amount").html("$0.00");
        $("span#total_amount").html("$0.00");
        $('.amount_limit_error').html('');
        $("#description").prop("disabled", true);
        $("#submit_button").prop("disabled", true);
        if($("#location_id").children(":selected").val() != 0) {
            $(".alert-body").css("display","block");
            $(".account-information").text("Please wait, getting account information.")
            // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Retrieving Balance Information</strong></div>');
        }
        $("#location_id-error").remove();
    });

    $(document).on("shown.bs.modal", "#new_check", function(){
        $(".send_check").html("New ACH");
    });

})
function confirm_ach_submit() {
    validateNewAchForm('ach-form');
    if ($("#ach-form").valid()) {
        if ($("#bank_routing").val() !== "" && $("#bank_account").val() !== "" && $("#bank_name").val() !== "" && $("#email_address").val() !== "" && $("#description").val() !== "" && $("#location_id").val() !== "" && $("#bank_account_type").val() !== "") {
            var name = $('#bank_name').val();
            var amount = $('#amount').val();
            amount = parseFloat(amount).toFixed(2);
            $("#ach-confirm-amount").text("");
            $("#ach-receiver-name").text("");
            $("#ach-confirm-amount").text(amount);
            $("#ach-receiver-name").text(name);
            $('.confirmationDialog').modal({ show: true });
        }
    }
}

function submit_ach() {

    if ($("#ach-form").valid()) {
        if ($("#bank_routing").val() !== "" && $("#bank_account").val() !== "" && $("#bank_name").val() !== "" && $("#email_address").val() !== "" && $("#description").val() !== "" && $("#location_id").val() !== "" && $("#bank_account_type").val() !== "") {
            $(".hello-world").prop('disabled',true);
            $("#ach-form").closest("form")[0].submit();
            $('#cover-spin').show(0);
        }
    }
}


function myFunction() {
    $('.confirm_send_modal').modal('toggle')
     $('.confirm_send_modal').remove();
}


$("body").delegate("#location_id","change",function() {
   var locations_id = $( "#location_id option:selected" ).val();
    if(locations_id != ""){
       $.ajax({
          url: "/v2/partner/instant_ach/location_data",
          type: "GET",
          data: {location_id: locations_id,type: "instant_ach"},
          success: function(data){
            $("#balance").text(data["balance"])
            $('#fee_dollar').val(data["fee_dollar"])
            $('#fee_perc').val(data["fee_perc"])
            if($("#location_id").val() === ""){
                $("#bank_account_name").html('')
                $("#bank_account_name").append('<option value="">Bank</option>')
            }else{
                if( data["system_fee"] == true ){
                    if(data["d_account"] == null){
                        $(".account-information").text("You have no bank account.")
                            $("#description").prop("disabled", true);
                            $("#description").html('');
                            $("#submit_button").prop("disabled", true);
                            $("#bank_account_name").prop("disabled",true);
                            $("#bank_account_name").attr('disabled','disabled');
                            return

                    }else{
                        $("#bank_account_name").html('').append("<option value= " + data["wallet_id"] + " > "  + data["d_account"] + "</option>");
                    }
                }
            }
            $("#location_id-error").empty()
            $("#bank_account_name").selectpicker('refresh')

            $("#achlimit").val(data["ach_limit"]);
            $("#achlimittype").val(data["ach_limit_type"]);
            $('#achtotalcount').val(data["total_count"])
            if($("#location_id").children(":selected").val() != 0) {
                       $("#amount").prop("disabled", false);
                    $("#description").prop("disabled", false);
                    $(".alert-body").css("display","none");
                    $("#errormessage").css('display', 'none').html('');
                    $("#submit_button").prop('disabled', false);
                    $("#bank_account_name").attr('disabled',false);
            }


          }
          
       });
    }else{
        var merchant_user = $("#merchant_user").val();
        if(merchant_user == "true"){
            $('#bank_account_name').val($('#location_id').val());
        }
        $("#balance").html("0.00")
        $("#errormessage").css('display', 'none').html('');
        $(".alert-body").css("display","none");
        $("#amount").prop("disabled", true);
        $("#amount").val('');
        $("span#amount").html("$0.00");
        $("span#fee_amount").html("$0.00");
        $("span#total_amount").html("$0.00");
        $('.amount_limit_error').html('');
        $("#description").prop("disabled", true);
        $("#description").html('');
        $("#submit_button").prop("disabled", true);
        $("#bank_account_name").prop("disabled",true);
        $("#bank_account_name").attr('disabled','disabled');
        if($("#location_id").children(":selected").val() != 0) {
            $(".alert-body").css("display","block");
            $(".account-information").text("Please wait, getting account information.")
            // $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Retrieving Balance Information</strong></div>');
        }
    }
});


$("body").delegate("#confirm_ach","click",function(){
    $("#ach-form").submit();
});



var globalTimeout = null;

// $("body").delegate('#filter_form input[type="checkbox"]',"change",function(){
$(document).on('change','#filter_form input[type="checkbox"]',function () {
    $('#filter_form input[type="checkbox"]').attr("disabled", true);
    if($(this).attr("id") == "search_all"){
        if( $(this).is(":checked")) {
            $('#filter_form input[type="checkbox"]').prop("checked", false)
            $("#filter_form input[type='checkbox']").prop("checked", true)
        }else{
            $(this).prop("checked", false)
            $('#filter_form input[type="checkbox"]').prop("checked", false)
        }
    }else{
        $('#filter_form input[type="checkbox"]#search_all').prop("checked",false)
    }
    if($("#filter_form input:checkbox:not(:checked)").length == 1)// when all is check except all then all checkbox will automatically checked
    {
        $("#search_all").prop("checked",true)
    }
    if($('#filter_form input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
    {
        $("#filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    }
    search_call("status");
});




$(document).on('keyup','.search_input_datatable',function (e) {
    e.preventDefault();
    globalTimeout = setTimeout(function () {
        search_call("search");
    }, 2500);
});


function search_call(type){
    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
        globalTimeout = null;
        var search_value = $('.search_input_datatable').val();
        search_value = search_value.trim()
        data = {"q[data_search]": search_value, filter: $("#filter option:selected").val(),custom: custom,first_date: startdate, second_date: enddate, search_form: 'true'};
        $('#filter_form input[type="checkbox"]').each(function(index){
            if($('#filter_form input[type="checkbox"]')[index].checked){
                data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
            }
        })
        var wallet_id = $("#wallet_id").val();
        data["wallet_id"] = wallet_id
        $.ajax({
            url: '/v2/partner/instant_ach',
            type: 'GET',
            data: data,
            success: function (data) {
                $('.table-data').html(data);
                if(type == "status"){
                    $(".btn-status").trigger('click');
                }
                update_total();
            }, error: function (data) {
            }
        });
}

var startdate = '';
var enddate = '';
var custom = false;

$(document).on('click', '.ranges:eq(1) ul li', function () {

    if($(this).text() == "Yesterday"){
        startdate = moment().subtract(1, "days")._d;
        enddate = moment().subtract(1, "days")._d;
    } else if($(this).text() == "Today") {
        startdate = moment()._d;
        enddate = moment()._d;
    } else if($(this).text() == "Last 6 Days") {
        startdate = moment().subtract(5, "days")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Month to Date") {
        startdate = moment().startOf("month")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Previous month") {
        startdate =  new moment().subtract(1, 'months').date(1)._d
        enddate =  moment().subtract(1,'months').endOf('month')._d;
    }
    let filter = $("#filter option:selected").val();
    var search_value = $('.search_input_datatable').val();
    search_value = search_value.trim()
    if($(this).text() != "Custom"){
        custom = false;
        data = {"q[data_search]": search_value, filter: filter,custom: custom,first_date: startdate, second_date: enddate, search_form: 'true'};
        $('#filter_form input[type="checkbox"]').each(function (index) {
            if ($('#filter_form input[type="checkbox"]')[index].checked) {
                data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
            }
        })
        $.ajax({
            url: '/v2/partner/instant_ach',
            type: 'GET',
            data: data,
            success: function(data) {
                $('.table-data').html(data);
                update_total();
            }
        });
    }
});
$(document).on('click', '.applyBtn:eq(1)', function () {
    // var startdate = $("input[name=daterangepicker_start]").last().val();
    // var enddate = $("input[name=daterangepicker_end]").last().val();

    startdate = $("input[name=daterangepicker_start]").last().val();
    enddate = $("input[name=daterangepicker_end]").last().val();
    startdate = moment(startdate)._d
    enddate = moment(enddate)._d
    custom = true
    let filter = $("#filter option:selected").val();
    var search_value = $('.search_input_datatable').val();
    search_value = search_value.trim()
    data = {"q[data_search]": search_value, filter: filter,custom: custom,first_date: startdate, second_date: enddate, search_form: 'true'};
    $('#filter_form input[type="checkbox"]').each(function (index) {
        if ($('#filter_form input[type="checkbox"]')[index].checked) {
            data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
        }
    })

    $.ajax({
        url: '/v2/partner/instant_ach',
        type: 'GET',
        data: data,
        success: function(data) {
            $('.table-data').html(data);
            update_total();
        }
    });
});

function update_total(){
    $("#m_table_1").DataTable();
    $("#page").selectpicker();
    $("#total_pending").html("").html("$"+$(".total_pending").text());
    $("#total_void").html("").html("$"+$(".total_void").text());
    $("#total_failed").html("").html("$"+$(".total_failed").text());
    $("#total_paid").html("").html("$"+$(".total_paid").text());
    $("#total_in_process").html("").html("$"+$(".total_in_process").text());
    $("#total_paid_wire").html("").html("$"+$(".total_paid_wire").text());
}

$(document).on('keyup','.less_than_zero', function () {
    var amount_val = $(this).val();
    if( amount_val < 1 ) {
        $('.amount_limit_error').html('').text("Amount must be greater than 1");
        $(".btn_confirm").prop("disabled", true);
    }
    if(amount_val == ''){
        $('.amount_limit_error').html('').text('');
        $(".btn_confirm").prop("disabled", true);
    }
    if( amount_val >= 1 ) {
        $(".btn_confirm").prop("disabled", false);
    }
});
