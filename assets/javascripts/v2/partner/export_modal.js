$("#export_button").on("click",function () {
    start_date=$('#date_start').val();
    end_date=$('#date_end').val();
    date = start_date +" - "+ end_date;
    var type = $("#type").val();
    var sub_type = $(this).data("subtype");
    var wallet_id= $(this).data("wallet_id");
    controller_call(date, type, sub_type, wallet_id,'', "");
});

$("#batch_export").on("click", function(){
    var start_date = $(this).data("date");
    var batch_date = start_date
    var type = $("#type").val();
    var sub_type = $(this).data("sub_type");
    var wallet_id = $(this).data("wallet_id");
    controller_call("", type, sub_type, wallet_id,'', batch_date);
})

$("#search_export").on("click",function () {
    start_date=$('#date_start').val();
    end_date=$('#date_end').val();
    date = start_date +" - "+ end_date;
    var type = $(this).data("type");
    var sub_type=$(this).data("subtype");
    var wallet_id=$(this).data("wallet_id");
    var batch_date = $(this).data("batch-date");
    var search_query = {
        name:  $(this).data('search-name'),
        email: $(this).data('search-email'),
        phone: $(this).data('search-phone'),
        amount: $(this).data('search-amount'),
        date: $(this).data('search-date'),
        last4: $(this).data('search-last4'),
        type: $(this).data('search-type'),
        time1: $(this).data('search-time1'),
        time2: $(this).data('search-time2'),
        first6: $(this).data('search-first6'),
        // date: date,
        block_id: $(this).data('search-block-id'),
        batch_id: $(this).data('batch-id'),
        status:  $('#checkboxes').val(),
        sender_name: $(this).data("sender-name"),
        sender_email: $(this).data("sender-email"),
        sender_phone_number: $(this).data("phone"),

    };
    controller_call(null, type, sub_type, wallet_id, search_query, batch_date);
});
function controller_call(date, type, sub_type, wallet_id, search_query, batch_date) {
    var offset = moment().local().format('Z');
    $.ajax({
        url: "/v2/generate_export_file",
        dataType: "json",
        data: {date: date, batch_date: batch_date, type: type, offset: offset,trans: sub_type,wallet_id: wallet_id, search_query: search_query},
        success: function (data) {
        }
    })
}