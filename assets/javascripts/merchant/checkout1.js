$(document).ready(function () {
    $("#welcome_popover").popover('show');
});
$('.data-wizard-next-button').click(function () {
    $("#step1_popover").popover('show');
    $("#welcome_popover").popover('hide');
    $('.m_wizard_form_step_1').removeClass('m-wizard__step--current');
    $('.m_wizard_formm_step_1').removeClass('m-wizard__form-step--current');
    $('.m_wizard_form_step_1').addClass('m-wizard__step--done');
    $('.m_wizard_form_step_2').removeClass('m-wizard__step--done');
    $('.m_wizard_form_step_2').addClass('m-wizard__step--current');
    $('.m_wizard_formm_step_2').addClass('m-wizard__form-step--current');
    $('.m_wizard_form_step_3').removeClass('m-wizard__step--current');
    $('.m_wizard_formm_step_3').removeClass('m-wizard__form-step--current');
    $('.m_wizard_formm_step_3').removeClass('m-wizard__step--done');
    $('#wizard-progress-bar').removeClass('width-0-percent');
    $('#wizard-progress-bar').addClass('width-49-percent');
    $('.m_wizard_form_step_1 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('show');
    $('.m_wizard_form_step_1 .m-wizard__step-number > span > i').addClass('color-fff');
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('show');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').removeClass('hide');
});

$('.data-shipping-wizard-next-button').click(function () {
    $("#step1_popover").popover('hide');
    $('.ready-message').hide();
    $('.main-content').show();
    $("#step2_popover").popover('show');
    $('#wizard-progress-bar').addClass('width-100-percent');
    $('.m_wizard_form_step_3').addClass('m-wizard__form-step--current');
    $('.m_wizard_formm_step_3').addClass('m-wizard__form-step--current');
    $('.m_wizard_formm_step_2').removeClass('m-wizard__form-step--current');
    $('.m_wizard_form_step_2').addClass('m-wizard__form-step--done');
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span').addClass('background-ffa701 border-none');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
    $('.m_wizard_form_step_3 .m-wizard__step-number > span').addClass('background-fff border-none');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').addClass('show');
    $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').removeClass('hide');

});

$('.m_wizard_form_step_3').click(function () {
    return false;
});

$('.m_wizard_form_step_1').click(function () {
    return false;
});

$('.btn_send').click(function () {
    $('#term-service-alert-message').show();
});

$('.close-toast').click(function () {
    $('#term-service-alert-message').hide();
});
$('.close_pwd_toast').click(function () {
    $('#pw-alert-message').hide();
});

$("#data-wizard-back-button").click(function () {
    $("#step1_popover").popover('hide');
    $("#welcome_popover").popover('show');
    $('.m_wizard_form_step_1').addClass('m-wizard__step--current');
    $('.m_wizard_formm_step_1').addClass('m-wizard__form-step--current');
    $('.m_wizard_form_step_1').removeClass('m-wizard__step--done');
    $('.m_wizard_form_step_2').removeClass('m-wizard__step--current');
    $('.m_wizard_formm_step_2').removeClass('m-wizard__form-step--current');
    $('#wizard-progress-bar').addClass('width-0-percent');
    $('#wizard-progress-bar').removeClass('width-49-percent');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_1 .m-wizard__step-number .wizard_dot').addClass('show');
    $('.m_wizard_form_step_1 .m-wizard__step-number .wizard_dot').removeClass('hide');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').removeClass('color-fff');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span').removeClass('background-ffa701');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span').removeClass('border-none');
});

$("#data-wizard-card-button").click(function () {
    $("#step1_popover").popover('show');
    $("#step2_popover").popover('hide');
    $('.m_wizard_form_step_2').addClass('m-wizard__step--current');
    $('.m_wizard_formm_step_2').addClass('m-wizard__form-step--current');
    $('.m_wizard_form_step_2').removeClass('m-wizard__step--done');
    $('.m_wizard_form_step_3').removeClass('m-wizard__step--current');
    $('.m_wizard_formm_step_3').removeClass('m-wizard__form-step--current');
    $('#wizard-progress-bar').removeClass('width-100-percent');
    $('#wizard-progress-bar').addClass('width-49-percent');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('show');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').removeClass('hide');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
    $('.m_wizard_form_step_3 .m-wizard__step-number > span').removeClass('background-fff');
    $('.m_wizard_form_step_3 .m-wizard__step-number > span').removeClass('border-none');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').removeClass('show');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').removeClass('show');

});





