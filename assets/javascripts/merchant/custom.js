
$('#close__leftsidebar').on('click',function () {

    $('body').addClass("m-brand--minimize m-aside-left--minimize");
    $('#m_aside_left_minimize_toggle').removeClass('m-brand__toggler--active');
});
// Side bar icon show and dide
$('#m_aside_left_minimize_toggle').click(function () {
    $('.side_bar_close_image').toggle();
});

$(".cancel-order-dd-list").change(function () {
    if(this.value=="Other"){
        $('.cancel-reason').show();
    }
    else{
        $('.cancel-reason').hide();
    }
});

// click on cancel button to hide
$(document).ready(function(){
    $("#hide").click(function(){
        $(".filter-form").hide();
    });
});

$("#mobile_no").on('keyup change', function (){
    if($(this).val() !==''){
        $('#mobile_phone_error').hide();
        $("#mobile_pencil").hide();
        $('#mobile_no').attr('style', 'background: #ffffff !important');
        $('#mobile_no').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
        $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#mobile_phone_error').show();
        $("#mobile_no").css("border", "2px solid #e88a8a");
        $("#mobile_pencil").show();
        $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");
    }
});

$("#user_email").on('keyup change', function (){
    if($(this).val() !==''){
        $('#email_error').hide();
        $('#user_email').attr('style', 'background: #ffffff !important');
        $('#user_email').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
        $("#email_pencil").hide();
        $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#email_error').show();
        $("#user_email").css("border", "2px solid #e88a8a");
        $("#email_pencil").show();
        $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");
    }
});
$("#password").on('keyup change', function (){
    if($(this).val() !==''){
        $('#password_error').hide();
        $("#password").css("border", "2px solid rgb(53, 186, 182)");
        $('#password').attr('style', 'background: #ffffff !important');
        $('#password').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
        $(".pencel_icon").hide();
        $(".login-eye").show();
        $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#password_error').show();
        $(".pencel_icon").show();
        $("#password").css("border", "2px solid #e88a8a");
        $(".login-eye").hide();
        $(".login-open-eye").hide();
        $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");

    }
});


$( '#m_filter_dd ' ).on('click', function () {
    if($(".dropdown-menu").hasClass("show")){
        $('.dropdown-menu').prop('selectedIndex', 1); // select 4th option
        $('.bootstrap-select > .dropdown-toggle:after').attr('style', 'content: !important');
        $('.bootstrap-select > .dropdown-toggle:after').attr('style', 'content:\f0d8 !important');
    }
    else{
        $('.bootstrap-select > .dropdown-toggle:after').attr('style', 'content:\f0dd !important');
    }


});


// search Filter
$('#searchfilter').click(function () {
       $('.filter-form').toggle();

});


// user profile js
function createprofilestars(n) {
    return new Array(n+1).join("*")
}


$(document).ready(function() {

    var timer = "";

    // $(".panel").append($('<input type="text" class="profilehidpassw" />'));

    $(".profilehidpassw").attr("name", $(".profilepass").attr("name"));

    $(".profilepass").attr("type", "text").removeAttr("name");

    $("body").on("keypress", ".profilepass", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".profilehidpassw").val($(".profilehidpassw").val() + character);
        }


    });

    $("body").on("keyup", ".profilepass", function(e) {
        $(".login-open-eye").hide();
        var code = e.which;
        if (code == 8) {
            var length = $(".profilepass").val().length;
            $(".profilehidpassw").val($(".profilehidpassw").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.profilepass').val().length;
            $(".profilepass").val(createprofilestars(current_val - 1) + $(".profilepass").val().substring(current_val - 1));
        }

        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".profilepass").val(createprofilestars($(".profilepass").val().length));
        }, 200);

    });

});

// Order Riview js
$('.review-order-link').click(function () {
    $('.order_review_box').show();
    $('.order_review_box').css({"width":"300px","background-color":"#f7f7f7","border":"1px solid rgb(249, 249, 249)","box-shadow":" 0 0px 0px 0 rgba(0, 0, 0, 0)","position":"relative","bottom":"1em","margin-left":"12px"});
    $('#order_details').removeClass("order-details");
    $('.order-review-para-text').css({"margin": "-0.6875em  0.75em"});
    $('.responsive_order_id').css({"display": "flex","padding":"28px 0px 0px 0px"});
    $('.order_responsive_order_id').css({"display": "flex","padding":"28px 0px 0px 0px"});
    $('.data_value_id').css("text-align","right");
    $('.col-md-9').css("display","none");
    $('.side_bar_line').hide();
    $('.case__opened').hide();
    $('.order-review').hide();
    $('.contact-info').hide();
    $('.m-subheader__title').hide();
    $('.review-order-link').hide();
    $('.order-review-para-text').show();
    $('.dasktop_order_id').hide();
    $('#alert-message').hide();
    $(".order_review_box_heading").hide();
});



//chat box js

$('.chat-box-title').click(function () {
    $('#inbox-sidebar').addClass("chatbox-responsive-title");
    $('.m-subheader__title ').addClass('m-responsive-subheader__title');
    $('#r_m_aside_left_minimize_toggle').addClass('chatbox-sidebar');
    $(".m-footer").addClass("chatbox-footer");
    $('.go-back').addClass("return-chatbox");
    $('#inbox-body').addClass('chat-box');
    $('#m_header_topbar').addClass("chatbox-topbar");
});

var starPasswordValue;
$(".login-eye").click(function(){
    $(".login-open-eye").show();
    $('.login-eye').hide();
    starPasswordValue=$(".m-profile-password-input").val();
    $('#password').val($('.profilehidpassw').val());
});
$(".login-open-eye").click(function(){
    $(".login-eye").show();
    $(".login-open-eye").hide();
    $('#password').val(starPasswordValue);
});

$("input[type=file]").on("click", function(e){
    e.stopPropagation();

});

$('#OpenImgUpload').click(function(e){
    $('#imgupload').val('');
    $('#imgupload').trigger('click');

});
$("#imgupload").change(function(){
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#editProfile').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

        $(".delete_profile").show();

    }
}


$(".delete_profile").click(function(e){

    $('#editProfile').attr('src', 'assets/img/profile/edit-profile.svg');
    $(".delete_profile").toggle();
});

function openRightMenu() {
    document.getElementById("rightMenu").style.display = "block";
    $('body').append('<div class="m-quick-notification-sidebar-overlay"></div>');
}


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            $('#'+$(this).attr('data-id')).text('Show more');
            $('#'+$(this).attr('data-id')).css('color','rgba(0, 0, 0, 0.5)');
            $('#'+this.id).removeClass('text');
        } else {
            <!--  alert(this.id); -->
            panel.style.maxHeight = panel.scrollHeight + "px";
            $('#'+$(this).attr('data-id')).text('Show less');
            $('#'+$(this).attr('data-id')).css('color','rgba(0, 0, 0, 0.7)');
            $('#'+this.id).addClass('text');

        }
    });
}

/* get help js */

$('html').click(function() {
    $('#rightMenu').hide();
    $('.m-quick-notification-sidebar-overlay').remove();
});

$('#m_header_topbar').click(function(event){
    event.stopPropagation();
});


$("#help_menu").click(function(){
    document.getElementById("helpMenu").style.display = "block";
});

$("#refund-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "hidden";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#ffd95a");

});

$("#chargeback-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "hidden";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#ffd95a");
});

$("#chat-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "hidden";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#ffd95a");
});


$("#other-subject").click(function(){
    document.getElementById("subject_description").style.display = "block";
    document.getElementById("subject_description").style.visibility = "visible";
    document.getElementById("horizontal_line").style.display = "block";
    $('.continue-btn').show();
    $('.continue-btn').css("background-color","#d6d6d6");
});

$("#subject_description").bind("keyup change", function(e) {
    if($(this).val()!='')
        $('.continue-btn').css("background-color","#ffd95a");
    else{
        $('.continue-btn').css("background-color","#d6d6d6");
    }
})

$('html').click(function() {
    $('#helpMenu').hide();
});

$('#help_menu').click(function(event){
    event.stopPropagation();
});

$('#helpMenu').click(function(event){
    event.stopPropagation();
});

/* End get help js */

$( ".r_aside_left_minimize_toggle" ).click(function(){
   $('.m-aside-left').css("left","auto");
});

$('html').click(function() {
   $(".m-aside-left").removeAttr("style");

});

$('#m_aside_left').click(function(event){
   event.stopPropagation();
});

$(document).on('click','.person-box',(function (event) {
    event.stopImmediatePropagation();
}))

$('#close_sidebar').click(function(){
    $('body').removeClass('m-brand--minimize m-aside-left--minimize');
});


var options = ['sale_processed','invoice_processed','avg_txn','cbk_perc','refund','card_type'];
$( '#graph_dropdown a' ).on( 'click', function( event ) {
    console.log(options);
    var $target = $( event.currentTarget ),
        val = $target.attr( 'data-value' ),
        $inp = $target.find( 'input' ),
        idx;
    console.log( options );
    if ( ( idx = options.indexOf( val ) ) > -1 ) {
        options.splice( idx, 1 );
        setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
        $("."+val).hide();
    } else {
        options.push( val );
        setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
        $("."+val).show();
    }
    $( event.target ).blur();
    console.log( options );
    return false;
});


$( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
} );


$( ".side_bar_menue" ).mouseover(function()
{
    $('#body--aa').removeClass("m-aside--offcanvas-default m-aside-left--minimize m-brand--minimize");
    $('#BotMenu1').hide();
    $('#BotMenu2').show();
    $('.Logo').hide();
    $('.Logo-1').show();
    $('.n-expand').hide();
    $('.expand').show();
});

$( ".side_bar_menue" ).mouseout(function()
{
    $('#body--aa').addClass("m-aside--offcanvas-default m-aside-left--minimize m-brand--minimize");
    $('#BotMenu2').hide();
    $('#BotMenu1').show();
    $('.Logo-1').hide();
    $('.Logo').show();
    $('.expand').hide();
    $('.n-expand').show();
});

function randomize(numb) {
    var rand = numb;
    var x = document.querySelector('.progress-circle-prog');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}
$('.succes--call').on('click' ,function () {

    setTimeout(randomize(0), 200);
    setTimeout(function(){ randomize(25); }, 1000);
    setTimeout(function(){ randomize(50);  }, 2000);
    setTimeout(function(){ randomize(75); }, 3000);
    setTimeout(function(){ randomize(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function ach(numb) {
    var rand = numb;
    var x = document.querySelector('.ach');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.success-ach-call').on('click' ,function () {

    setTimeout(ach(0), 200);
    setTimeout(function(){ ach(25); }, 1000);
    setTimeout(function(){ ach(50);  }, 2000);
    setTimeout(function(){ ach(75); }, 3000);
    setTimeout(function(){ ach(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

$('.ok--btn').on('click' ,function () {
    setTimeout(function(){
    $('.working').show();
    $('.success--image').hide();
        $('.first--text').show();
        $('.last--text').hide();
    randomize(0);
    }, 3000);
});


$(".rotate--icon").click(function(){
   $(this).find("img").toggleClass("down");
    $(this).find("i").toggleClass("down");
});

$(document).ready(function(){
    var height = $(window).height();
    if (height < 830)
    {
        $('#last--li').removeClass('last--li');
    }
    else
    {
        $('#last--li').addClass('last--li');
    }
});
$( window ).resize(function() {
    var height = $(window).height();
    if (height < 830)
    {
        $('#last--li').removeClass('last--li');
    }
    else
    {
        $('#last--li').addClass('last--li');
    }
});

// Activity Log Scripts
$(document).ready(function(){
    $(document).on('click','.slide-toggle',(function () {
        $(".activit-box").animate({
            width: "toggle"
        });
    }));
    $(document).on('click','.slide-toggle-open',(function () {
        var type=$(this).data('type');
        $.ajax({
            url: '/v2/activity_logs/'+type,
            type: 'GET',
            success: function(data) {
                $(".activit-box").empty().append(data);
                $("#activity_log_type").val(type);
                $('html body').addClass('disable-scroll');
                $('body').append('<div class="m-quick-sidebar-overlay" id="hover-ley"></div>');
            },
            error: function(data){
            }
        });
    }));
    $(document).on('click','.slide-toggle-close',(function () {
        $('html body').removeClass('disable-scroll');
        $('.m-quick-sidebar-overlay').remove();
    }));
    $("#hover-ley").on('click',function(){
        $(".activit-box").animate({
            width: "toggle"
        });
        $('html body').removeClass('disable-scroll');
        $('.m-quick-sidebar-overlay').remove();
    });
    $("#personeJs").click(function(){
        console.log('hello heklo');
        if ($(this).hasClass('user-hover'))
        {
            $(this).removeClass('user-hover');
            $(this).addClass('person');
            $(this).find('span').removeClass('hide--person');
            $(this).find('#w-img').removeClass('hide--person');
            $(this).find('#b-img').addClass('hide--person');
        }
        else
        {
            $(this).addClass('user-hover');
            $(this).removeClass('person');
            $(this).find('span').addClass('hide--person');
            $(this).find('#w-img').addClass('hide--person');
            $(this).find('#b-img').removeClass('hide--person');
        }
    });
});
$("#filterLog").click(function(){
    $('#bottom-e').addClass('hide--person');
    $('#top-e').removeClass('hide--person');
});
function person() {
    $("#personeJs").addClass('user-hover');
    $("#personeJs").removeClass('person');
    $("#personeJs").find('span').addClass('hide--person');
    $("#personeJs").find('#w-img').addClass('hide--person');
    $("#personeJs").find('#b-img').removeClass('hide--person');


    $('#bottom-e').removeClass('hide--person');
    $('#top-e').addClass('hide--person');
}


$("#search-input").keyup(function() {
console.log('xccx');
    // Retrieve the input field text and reset the count to zero
    var filter = $(this).val(),
        count = 0;
    // Loop through the comment list
    $('#mylist li .time').each(function() {
        // If the list item does not contain the text phrase fade it out
        if ($('#mylist li .time').text().search(new RegExp(filter, "i")) < 0) {
            $('#mylist li').hide();  // MY CHANGE

            // Show the list item if the phrase matches and increase the count by 1
        } else {
            $('#mylist li').show(); // MY CHANGE
            count++;
        }

    });
});


jQuery("#search-input").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    jQuery("#mylist li").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});

var locs = [];
$( '#locs_dropdown a' ).on( 'click', function( event ) {
    var $target = $( event.currentTarget ),
        val = $target.attr( 'data-value' ),
        $inp = $target.find( 'input' ),
        idx;
    if(val === 'all')
    {
        locs = [];
        setTimeout( function() { $('.locs_checkboxes').prop( 'checked', true ) }, 0);
        $("#locs_dropdown a").each(function() {
            locs.push($(this).attr('data-value'));
        });
        locs.shift();
    }
    else
    {
        if ( ( idx = locs.indexOf( val ) ) > -1 ) {
            setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            locs.splice( idx, 1 );
        } else {
            setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            locs.push( val );
        }
        $( event.target ).blur();
    }
    if (locs.length === 0)
    {
        $( '#locs_show_selected' ).text('Select Location');
    }
    else
    {
        $( '#locs_show_selected' ).text(locs.join(','));
    }
    console.log(locs);
    return false;
});

$( '.double--click' ).on( 'click', function( event ) {
    var target = $( event.currentTarget );
    target.addClass('pointer--none');
});
// Hide on modal close
$(".modal").on("hidden.bs.modal", function () {
    var target = $( '.double--click' );
    target.removeClass('pointer--none');
});
$(document).ready(function() {
    if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1) {
        $('#browser--error').trigger('click');
    } else if (navigator.userAgent.indexOf("Chrome") != -1) {
        //$('#browser--error').trigger('click');
    } else if (navigator.userAgent.indexOf("Safari") != -1) {
        $('#browser--error').trigger('click');
    } else if (navigator.userAgent.indexOf("Firefox") != -1) {
        $('#browser--error').trigger('click');
    } else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
        $('#browser--error').trigger('click');
    } else {  }
});
