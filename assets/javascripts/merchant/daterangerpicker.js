var a = moment().subtract(29, "days"), t = moment();

// Apply button and input hidden script
$(document).ready(function()
{
    $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(1), #body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2),#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(3),#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(4)').click(function(e)
    {
        setTimeout(function(){
            if ($('#detail-value').val() == 'date')
            { $('.daterangepicker').css('height','235px') }
            else  { $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft').addClass('show-calendar modal-range');  }
            $('.ranges ul li:last-child').mouseover();
            $('.daterangepicker_input').hide();
            $('.applyBtn').hide();
        }, 50);
    });
    $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:last-child').click(function(e)
    {
        if ($('#detail-value').val() == 'date')
        { $('.daterangepicker').css('height','400px') }
        $('.daterangepicker_input').show();
        $('.applyBtn').show();
    });
    $('#myID > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:last-child').click(function(e)
    {
        if ($('#detail-value').val() == 'date')
        { $('.daterangepicker').css('height','400px') }
        $('.daterangepicker_input').show();
        $('.applyBtn').show();
    });
    $('#myID > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:last-child').click(function(e)
    {
        if ($('#detail-value').val() == 'date')
        { $('.daterangepicker').css('height','400px') }
        $('.daterangepicker_input').show();
        $('.applyBtn').show();
    });
});

// Hide on modal close
$(".modal").on("hidden.bs.modal", function () {
    $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft').removeClass('show-calendar modal-range');
    $('#detail-value').val('date');
    $('.daterangepicker').css('height','235px');
});
$(".modal").on("hidden.bs.modal", function () {
    if($('.modal.show').length)
    {
        setTimeout(function(){
            $('#body--aa').addClass('modal-open');
        }, 50);
    }
});
//Daily Batch
$("#daily_batch").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
},function(a, t, n) {
    console.log('Start Date :' + a.format("MM/DD/YYYY") + ' End Date :' + t.format("MM/DD/YYYY"));
    $("#daily_batch .form-control").val(n);
});
//Merchant Performance
$("#merchant_performance").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
},function(a, t, n) {
    console.log('Start Date :' + a.format("MM/DD/YYYY") + ' End Date :' + t.format("MM/DD/YYYY"));
    $("#daily_batch .form-control").val(n);
});

//Agent Performance
$("#agent_performance").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
},function(a, t, n) {
    console.log('Start Date :' + a.format("MM/DD/YYYY") + ' End Date :' + t.format("MM/DD/YYYY"));
    $("#daily_batch .form-control").val(n);
});
// E-Checks Date Range Picker
var a_a = t_t = moment();
$("#e-checks").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a_a,
    endDate: t_t,
    ranges: {
        Today: [moment(), moment()],
        "Month to Date": [moment().startOf("month"), moment()],
        "Last 6 Days": [moment().subtract(5, "days"), moment()],
        "Previous month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
    }
},function(a_a, t_t, n) {
    console.log('Start Date :' + a_a.format("MM/DD/YYYY") + ' End Date :' + t_t.format("MM/DD/YYYY"));
    $("#e-checks .form-control").val(n);
});
var date_echeck = $("#daterangepicker_echeck").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});
$('.e-export-btn').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_echeck.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_echeck.on('apply.daterangepicker', function(ev, date_echeck) {
    console.log('Start Date :' + date_echeck.startDate.format("MM/DD/YYYY") + ' End Date :' + date_echeck.endDate.format("MM/DD/YYYY"));
});


// ACH Date Range Pickers
 $("#total_ach").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a_a,
    endDate: t_t,
    ranges: {
        Today: [moment(), moment()],
        "Month to Date": [moment().startOf("month"), moment()],
        "Last 6 Days": [moment().subtract(5, "days"), moment()],
        "Previous month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
    }
},function(a_a, t_t, n) {
     console.log('Start Date :' + a_a.format("MM/DD/YYYY") + ' End Date :' + t_t.format("MM/DD/YYYY"));
     $("#total_ach .form-control").val(n);
 });

var date_ach = $("#daterangepicker_ach").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    parentEl:'#myID',
    alwaysShowCalendars: true,
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});
$('.export-ach-btn').on('click',function () {
    $('#daterangepicker_ach').trigger("click");
    $('#myID .daterangepicker').addClass('for-modal-range');
    $('#detail-value').val('');
    $('#myID .daterangepicker').css('height','400px');
    $('.ranges ul li:last-child').mouseover();
    // stop to hide
    date_ach.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_ach.on('apply.daterangepicker', function(ev, date_ach) {
    $('.ranges ul li:last-child').mouseover();
    console.log('Start Date :' + date_ach.startDate.format("MM/DD/YYYY") + ' End Date :' + date_ach.endDate.format("MM/DD/YYYY"));
});

// P2C Date Range Picker
$("#push_to_card").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a_a,
    endDate: t_t,
    ranges: {
        Today: [moment(), moment()],
        "Month to Date": [moment().startOf("month"), moment()],
        "Last 6 Days": [moment().subtract(5, "days"), moment()],
        "Previous month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
    }
},function(a_a, t_t, n) {
    console.log('Start Date :' + a_a.format("MM/DD/YYYY") + ' End Date :' + t_t.format("MM/DD/YYYY"));
    $("#push_to_card .form-control").val(n);
});
var date_p2c = $("#daterangepicker_p2c").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.p-export-btn').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_p2c.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_p2c.on('apply.daterangepicker', function(ev, date_p2c) {
    console.log('Start Date :' + date_p2c.startDate.format("MM/DD/YYYY") + ' End Date :' + date_p2c.endDate.format("MM/DD/YYYY"));
});

// Export Bulk Date Range Picker
$("#bulk--checks").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a_a,
    endDate: t_t,
    ranges: {
        Today: [moment(), moment()],
        "Month to Date": [moment().startOf("month"), moment()],
        "Last 6 Days": [moment().subtract(5, "days"), moment()],
        "Previous month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
    }
},function(a_a, t_t, n) {
    console.log('Start Date :' + a_a.format("MM/DD/YYYY") + ' End Date :' + t_t.format("MM/DD/YYYY"));
    $("#bulk--checks .form-control").val(n);
});

var date_export_bulk = $("#daterangepicker_export_bulk").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.export_bulk').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_export_bulk.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_export_bulk.on('apply.daterangepicker', function(ev, date_export_bulk) {
    console.log('Start Date :' + date_export_bulk.startDate.format("MM/DD/YYYY") + ' End Date :' + date_export_bulk.endDate.format("MM/DD/YYYY"));
});

// Chargeback Date Range Picker
var date_chargeback = $("#daterangepicker_chargeback").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.export-chargeback').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_chargeback.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_chargeback.on('apply.daterangepicker', function(ev, date_chargeback) {
    console.log('Start Date :' + date_chargeback.startDate.format("MM/DD/YYYY") + ' End Date :' + date_chargeback.endDate.format("MM/DD/YYYY"));
});


// Export Account Transactions
var date_account = $("#daterangepicker_account").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.account-transaction').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_account.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_account.on('apply.daterangepicker', function(ev, date_account) {
    console.log('Start Date :' + date_account.startDate.format("MM/DD/YYYY") + ' End Date :' + date_account.endDate.format("MM/DD/YYYY"));
});

// Export Tip Transactions
var date_tip = $("#daterangepicker_tip").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.tip-transaction').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_tip.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_tip.on('apply.daterangepicker', function(ev, date_tip) {
    console.log('Start Date :' + date_tip.startDate.format("MM/DD/YYYY") + ' End Date :' + date_tip.endDate.format("MM/DD/YYYY"));
});

// Fundings
var date_funding = $("#daterangepicker_fundings").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.fundings-click').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_funding.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_funding.on('apply.daterangepicker', function(ev, date_funding) {
    console.log('Start Date :' + date_funding.startDate.format("MM/DD/YYYY") + ' End Date :' + date_funding.endDate.format("MM/DD/YYYY"));
});

// Reserve Transaction
var date_reserve_transaction = $("#daterangepicker_reserve_transaction").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.reserve--transaction').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    date_reserve_transaction.data('daterangepicker').hide = function () {};
});
// Get Date Values
date_reserve_transaction.on('apply.daterangepicker', function(ev, date_reserve_transaction) {
    console.log('Start Date :' + date_reserve_transaction.startDate.format("MM/DD/YYYY") + ' End Date :' + date_reserve_transaction.endDate.format("MM/DD/YYYY"));
});

// Reserve Transaction
var export_employee = $("#daterangepicker_export_employee").daterangepicker({
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-secondary",
    startDate: a,
    endDate: t,
    ranges: {
        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "Last 60 Days": [moment().subtract(59, "days"), moment()],
        "Last 90 Days": [moment().subtract(89, "days"), moment()],
    }
});

$('.export-per-btn').on('click',function () {
    $('#detail-value').val('');
    $('.daterangepicker').css('height','400px');
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft .ranges ul li:nth-child(2)').trigger('click');
    }, 500);
    // stop to hide
    export_employee.data('daterangepicker').hide = function () {};
});
// Get Date Values
export_employee.on('apply.daterangepicker', function(ev, export_employee) {
    console.log('Start Date :' + export_employee.startDate.format("MM/DD/YYYY") + ' End Date :' + export_employee.endDate.format("MM/DD/YYYY"));
});


// Daterange Picker without Left Options
$("#m_daterangepicker_2").daterangepicker({
    autoApply: true,

}, function(a, t) {
    $("#m_daterangepicker_2 .form-control").val(a.format("MM/DD/YYYY") + " / " + t.format("MM/DD/YYYY"));
});
$("#m_daterangepicker_2").click(function (){
    $('.daterangepicker').css('height','300px');
    $('.daterangepicker').css('width','605px');
    $('.calendar').css('border-right','none');
});

$("#m_daterangepicker_2_success").daterangepicker({
    autoApply: true,

}, function(a, t) {
    $("#m_daterangepicker_2_success .form-control").val(a.format("MM/DD/YYYY") + " / " + t.format("MM/DD/YYYY"));
});
$("#m_daterangepicker_2_success").click(function (){
    $('.daterangepicker').css('height','300px');
    $('.daterangepicker').css('width','605px');
    $('.calendar').css('border-right','none');

});

$("#m_daterangepicker_2_decline").daterangepicker({
    autoApply: true,

}, function(a, t) {
    $("#m_daterangepicker_2_decline .form-control").val(a.format("MM/DD/YYYY") + " / " + t.format("MM/DD/YYYY"));
});
$("#m_daterangepicker_2_decline").click(function (){
    $('.daterangepicker').css('height','300px');
    $('.daterangepicker').css('width','605px');
    $('.calendar').css('border-right','none');

});


// Due Date for Invoice
$("#due__date__invoice_1").daterangepicker({
    autoApply: true,

}, function(a, t) {
    $("#due__date__invoice_1 .form-control").val(a.format("MM/DD/YYYY") + " / " + t.format("MM/DD/YYYY"));
});
$("#due__date__invoice_1").click(function (){
    $('.daterangepicker').css('height','300px');
    $('.daterangepicker').css('width','605px');
    $('.calendar').css('border-right','none');
});

// Due Date for Invoice
$("#due__date__invoice_2").daterangepicker({
    autoApply: true,

}, function(a, t) {
    $("#due__date__invoice_2 .form-control").val(a.format("MM/DD/YYYY") + " / " + t.format("MM/DD/YYYY"));
});
$("#due__date__invoice_2").click(function (){
    $('.daterangepicker').css('height','300px');
    $('.daterangepicker').css('width','605px');
    $('.calendar').css('border-right','none');
});

// Due Date for Invoice
$("#due__date__invoice_3").daterangepicker({
    autoApply: true,

}, function(a, t) {
    $("#due__date__invoice_3 .form-control").val(a.format("MM/DD/YYYY") + " / " + t.format("MM/DD/YYYY"));
});
$("#due__date__invoice_3").click(function (){
    $('.daterangepicker').css('height','300px');
    $('.daterangepicker').css('width','605px');
    $('.calendar').css('border-right','none');
});