$(".mdp-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.mdp-input').attr('style', 'background-color:transparent');
    }
    else
    {
        $('.mdp-input').attr('style', 'background-color:#f7f7f7 !important');
    }
});

// invoice toggle js
$("#togBtn").on('change', function() {
    if ($(this).is(':checked')) {
        $('#discount_sign').addClass('addon_left');
    }
    else {
        $('#discount_sign').removeClass('addon_left');
    }
});

$("#fee_togBtn").on('change', function() {
    if ($(this).is(':checked')) {
        $('#fee_sign').addClass('addon_left');
    }
    else {
        $('#fee_sign').removeClass('addon_left');

    }
});

// Business Setting js
$('.reveal_key_button').click(function () {
$('.reveal_key_button').hide();
$('.blur-text').css('filter','blur(0px)');
$('.hide_key_button').show();
$('.component_1').addClass('hide');
$('.component').removeClass('hide');
});
$('.hide_key_button').click(function () {
    $('.hide_key_button').hide();
    $('.blur-text').css('filter','blur(3px)');
    $('.reveal_key_button').show();
    $('.component_1').removeClass('hide');
    $('.component').addClass('hide');
});

// select all checkbox js in create user permission modal
$('input[class="select_all_permissions"]').click(function(){
    if($(this).prop("checked") == true){
        $("input[type=checkbox]").prop("checked", true);
    }
    else if($(this).prop("checked") == false){
        $("input[type=checkbox]").prop("checked", false);
    }
});

$("select.permission_dropdown").change(function(){
    var selectedCountry = $(this).children("option:selected").val();
    if(selectedCountry =='Admin (Full Access)'){
        $('#adminFullAccessModal').modal('show');
    }
});

$('.accept_chargeback').click(function () {
    $('.chargeback-accepted-status').show();
    $('.fight-chargeback-and-accept-btn').hide();
    $('.vl_1').show();
    $('.refund_accept_images').show();
    $('.vl_2').show();
    $('.refund_group_images').show();
    $('.charback-accepted-case').show();
    $('.fight-chargeback-part').hide();
});

$('.chargeback-submit-btn').click(function () {
    $('.fight-chargeback-case').show();
    $('.vl_3').show();
    $('.vl_4').show();
    $('.case-won-section').show();
    $('.refund_fcharge_images').show();
    $('.refund_fcharge1_images').show();

    $('.refund_group_images').show();
    $('.fight-chargeback-and-accept-btn').hide();
    $('.vl_1').show();
    $('.charback-accepted-case').hide();
    $('.fight-chargeback-part').hide();
});


$('#declinedSearchFilter').click(function () {
   $('#declinedFilterForm').show();
   $('#declinedSearchFilter').hide();
});


// loader js
function p2c(numb) {

    var rand = numb;
    var x = document.querySelector('.p2c');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.success-p2c-call').on('click' ,function () {
    setTimeout(p2c(0), 200);
    setTimeout(function(){ p2c(25); }, 1000);
    setTimeout(function(){ p2c(50);  }, 2000);
    setTimeout(function(){ p2c(75); }, 3000);
    setTimeout(function(){ p2c(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function echeck(numb) {
    var rand = numb;
    var x = document.querySelector('.echk');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.success-echeck-call').on('click' ,function () {

    setTimeout(echeck(0), 200);
    setTimeout(function(){ echeck(25); }, 1000);
    setTimeout(function(){ echeck(50);  }, 2000);
    setTimeout(function(){ echeck(75); }, 3000);
    setTimeout(function(){ echeck(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function withdraw_ach(numb) {
    var rand = numb;
    var x = document.querySelector('.ach');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.ach-succes--call').on('click' ,function () {

    setTimeout(echeck(0), 200);
    setTimeout(function(){ withdraw_ach(25); }, 1000);
    setTimeout(function(){ withdraw_ach(50);  }, 2000);
    setTimeout(function(){ withdraw_ach(75); }, 3000);
    setTimeout(function(){ withdraw_ach(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function withdraw_p2c(numb) {
    var rand = numb;
    var x = document.querySelector('.p2c');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.p2c-succes').on('click' ,function () {

    setTimeout(echeck(0), 200);
    setTimeout(function(){ withdraw_p2c(25); }, 1000);
    setTimeout(function(){ withdraw_p2c(50);  }, 2000);
    setTimeout(function(){ withdraw_p2c(75); }, 3000);
    setTimeout(function(){ withdraw_p2c(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function void_ach(numb) {
    var rand = numb;
    var x = document.querySelector('.echk');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.void-success-cal').on('click' ,function () {

    setTimeout(echeck(0), 200);
    setTimeout(function(){ void_ach(25); }, 1000);
    setTimeout(function(){ void_ach(50);  }, 2000);
    setTimeout(function(){ void_ach(75); }, 3000);
    setTimeout(function(){ void_ach(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function withdraw_echeck(numb) {
    var rand = numb;
    var x = document.querySelector('.echeck');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.echeck_succes--call').on('click' ,function () {
    setTimeout(echeck(0), 200);
    setTimeout(function(){ withdraw_echeck(25); }, 1000);
    setTimeout(function(){ withdraw_echeck(50);  }, 2000);
    setTimeout(function(){ withdraw_echeck(75); }, 3000);
    setTimeout(function(){ withdraw_echeck(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function giftCard(numb) {
    var rand = numb;
    var x = document.querySelector('.giftCarD');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

// dropdown opetion image js
$('.permission-dropdown--menu a').click(function () {
   var value=$(this).attr('data-value');
   $('.permission-dropdown').text(value);
   $('.dropdown-menu--pad').removeClass('show');
});

$('.success-giftCard-call').on('click' ,function () {

    setTimeout(giftCard(0), 200);
    setTimeout(function(){ giftCard(25); }, 1000);
    setTimeout(function(){ giftCard(50);  }, 2000);
    setTimeout(function(){giftCard(75); }, 3000);
    setTimeout(function(){ giftCard(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

$('.p2c-confirm-btn').click(function () {
    setTimeout(function(){
        $('.first--text').hide();
        $('.account-transfer-success .small').hide();
        $('.last--text').show();
    }, 45000);
});

$('.ach-confirm-btn').click(function () {
    setTimeout(function(){
        $('.first--text').hide();
        $('.account-transfer-success .small').hide();
        $('.last--text').show();
    }, 45000);
});

$('.giftcard-confirm-btn').click(function () {
    setTimeout(function(){
        $('.first--text').hide();
        $('.account-transfer-success .small').hide();
        $('.last--text').show();
    }, 45000);
});

$('#hide_declin_form').click(function () {
  $('#declinedFilterForm').hide();
  $('#declinedSearchFilter').show();
});

$('.fight_chargeback_btn').click(function () {
  $('.fight-chargeback-part').show();
    $('.fight-chargeback-and-accept-btn').hide();
});

$('.gift-card-back-btn').click(function () {
    $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft').removeClass('show-calendar');
    $('.opensleft').css('display','none');
});

//add giftcard enter amount addons
$(".product_amount1").on('keyup change', function (){
    if($(this).val() !==''){
        $(".dollar-addon").css("cssText", "display: block !important;");
    }
    else
    {
        $(".dollar-addon").css("cssText", "display: none !important;");
    }
});

// Dateranger modal js

$('#tab_7_1').click(function () {
  $('.modal-header-text').text("New ACH");
});

$('#tab_7_2').click(function () {
    $('.modal-header-text').text("New P2C");
});

$('#tab_7_3').click(function () {
    $('.modal-header-text').text("New Check");
});

$('#tab_7_4').click(function () {
    $('.modal-header-text').text("New Gift Card");
});

// welcome password tooltip js
$(".m-new-welcome-password").focus(function() {
    $("#tooltips").show();
});
$('body').click(function () {
    $('#tooltips').hide();
});

$('#tooltips').click(function (event) {
    event.stopPropagation();
});
//wizard js
$('.data-wizard-ok-button').click(function () {
    $("#step1_popover").popover('show');
    $("#welcome_popover").popover('hide');
    $('#wizard-progress-bar').removeClass('width-0-percent');
    $('#wizard-progress-bar').addClass('width-33-percent');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').show();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
});

$('#agreeTermsAndServiceBtn').click(function () {
    setTimeout(function(){
        setTimeout(function () {
            $('.m_wizard_form_step_3').addClass('m-wizard__form-step--current');
            $('.m_wizard_form_step_2').removeClass('m-wizard__form-step--current');
            $('.m_wizard_form_step_2').addClass('m-wizard__form-step--done');
            $('#wizard-progress-bar').addClass('width-42-percent');
            $('.ready-message').addClass('show');

        },2000);

        setTimeout(function () {
            $("#step1_popover").popover('hide');
            $('.ready-message').hide();
            $('.main-content').show();
            $("#step2_popover").popover('show');
            $('#wizard-progress-bar').addClass('width-66-percent');
            $('.m_wizard_form_step_2').removeClass('m-wizard__step--current');
            $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
            $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
            $('.m_wizard_form_step_2 .m-wizard__step-number > span').addClass('background-ffa701 border-none');
            $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
            $('.m_wizard_form_step_3 .m-wizard__step-number > span').addClass('background-fff border-none');
            $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').hide();
            $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').show();
        },7000);
    }, 1000);
});

$('.m_wizard_form_step_3').click(function () {
    return false;
});

$('.m_wizard_form_step_1').click(function () {
    return false;
});

$('.btn_send').click(function () {
    $('#term-service-alert-message').show();
});

$('.close-toast').click(function () {
    $('#term-service-alert-message').hide();
});
$('.close_pwd_toast').click(function () {
    $('#pw-alert-message').hide();
});

$('.login-welcome-change-password-button').click(function () {
    $('#pw-alert-message').show();

    setTimeout(function(){
        $("#step2_popover").popover('hide');
        $("#step3_popover").popover('show');
        $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').hide();
        $('.m_wizard_form_step_4').addClass('m-wizard__form-step--current');
        $('.m_wizard_form_step_3').removeClass('m-wizard__form-step--current');
        $('.m_wizard_form_step_3').addClass('m-wizard__form-step--done');
        $('#wizard-progress-bar').addClass('width-100-percent');
        $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
        $('.m_wizard_form_step_3 .m-wizard__step-number > span >i').addClass('color-fff');
        $('.m-wizard.m-wizard--2 .m-wizard__head .m-wizard__nav .m-wizard__steps .m-wizard__step .m-wizard__step-number > span').addClass('background-ffa701 border-none');
        $('.m-wizard.m-wizard--2 .m-wizard__head .m-wizard__nav .m-wizard__steps .m-wizard__step .m-wizard__step-number > span').addClass('background-fff border-none');
        $('.m_wizard_form_step_4 .m-wizard__step-number >span').addClass('color-fff');
        $('.m_wizard_form_step_4 .m-wizard__step-number >span').removeClass('background-ffa701');
        $('.m_wizard_form_step_4 .m-wizard__step-number .wizard_dot').show();
    }, 5000);

});


// login wellcome faq js
$('.m_tabs_3_1_next').click(function () {
 $('#m_tabs_3_1').removeClass('active show');
 $('.m_tabs_3_1').removeClass('active show');
 $('.m_tabs_3_2').addClass('active show');
 $('#m_tabs_3_2').addClass('active show');
});

$('.m_tabs_3_2_next').click(function () {
    $('#m_tabs_3_2').removeClass('active show');
    $('.m_tabs_3_2').removeClass('active show');
    $('.m_tabs_3_3').addClass('active show');
    $('#m_tabs_3_3').addClass('active show');
});

$('.m_tabs_3_3_next').click(function () {
    $('#m_tabs_3_3').removeClass('active show');
    $('.m_tabs_3_3').removeClass('active show');
    $('.m_tabs_3_4').addClass('active show');
    $('#m_tabs_3_4').addClass('active show');
});

$('.m_tabs_3_4_next').click(function () {
    $('#m_tabs_3_4').removeClass('active show');
    $('.m_tabs_3_4').removeClass('active show');
    $('.m_tabs_3_5').addClass('active show');
    $('#m_tabs_3_5').addClass('active show');
});

$('.m_tabs_3_5_next').click(function () {
    $('#m_tabs_3_5').removeClass('active show');
    $('.m_tabs_3_5').removeClass('active show');
    $('.m_tabs_3_1').addClass('active show');
    $('#m_tabs_3_1').addClass('active show');
});
//chargeback filter js
$('.btn-filter').click(function () {
  $('.filter-form').toggleClass('show');
  $('.btn-filter').addClass('btn-filter-bg');
});
$('.close_filter').click(function () {
    $('.filter-form').removeClass('show');
    $('.filter-form').addClass('hide');
    $('.btn-filter').removeClass('btn-filter-bg');
});

//popover js
$(document).ready(function () {
    $("#welcome_popover").popover('show');

});
// upload file js
$('.refund-upload-button').click(function(){
    const inpFile=document.getElementById("my_file");
    const progressBarFill=document.querySelector("#progressBar > .file-progress-bar");
    inpFile.addEventListener("change",uploadFile);
    function uploadFile(e) {

        const xhr= new XMLHttpRequest();
        xhr.open("POST","upload.php");
        xhr.upload.addEventListener("progress" , e =>{
            const percent=e.lengthComputable ?(e.loaded / e.total) * 100 : 0 ;
            progressBarFill.style.width =percent.toFixed(2) + "%";
        });
        xhr.send(new FormData(uploadForm));
        setTimeout(function () {
            $('.doc-file').show()
            $('.file-close').css('display','none')
            $(".trash-file").show()
        }, 4000);
    }
});

// old password js
var starPasswordValue;
$( ".login-old-password-open-eye" ).click(function() {
    var password=$(".hide_old_welcome_password").val();
    starPasswordValue=$(".m-old-welcome-password").val();
    $(".m-old-welcome-password").val(password);
    $(".login-old-password-open-eye").removeClass('show');
    $(".login-old-password-open-eye").addClass('hide');
    $(".login-old-password-eye").addClass('show');
});

$( ".login-old-password-eye" ).click(function() {
    var password=$(".hide_old_welcome_password").val();
    $(".m-old-welcome-password").val(starPasswordValue);
    $(".login-old-password-open-eye").addClass('show');
    $(".login-old-password-eye").removeClass('show');
});

// new eye password js
var starNewPasswordValue;
$( ".login-new-password-open-eye" ).click(function() {
    var password=$(".hide_new_welcome_password").val();
    starNewPasswordValue=$(".m-new-welcome-password").val();
    $(".m-new-welcome-password").val(password);
    $(".login-new-password-open-eye").removeClass('show');
    $(".login-new-password-open-eye").addClass('hide');
    $(".login-new-password-eye").addClass('show');
});

$( ".login-new-password-eye" ).click(function() {
    var password=$(".hide_new_welcome_password").val();
    $(".m-new-welcome-password").val(starNewPasswordValue);
    $(".login-new-password-open-eye").addClass('show');
    $(".login-new-password-eye").removeClass('show');
});

// confirm password js
var starConfirmPasswordValue;
$( ".login-confirm-password-open-eye" ).click(function() {
    var password=$(".hide_confirm_welcome_password").val();
    starConfirmPasswordValue=$(".m-confirm-welcome-password").val();
    $(".m-confirm-welcome-password").val(password);
    $(".login-confirm-password-open-eye").removeClass('show');
    $(".login-confirm-password-open-eye").addClass('hide');
    $(".login-confirm-password-eye").addClass('show');
});

$( ".login-confirm-password-eye" ).click(function() {
    var password=$(".hide_confirm_welcome_password").val();
    $(".m-confirm-welcome-password").val(starConfirmPasswordValue);
    $(".login-confirm-password-open-eye").addClass('show');
    $(".login-confirm-password-eye").removeClass('show');
});
// Create old welcome password stars js
function createOldPasswordWelcomeStars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {
    var timer = "";
    $(".m-old-welcome-password").attr("type", "text").removeAttr("name");
    $("body").on("keypress", ".m-old-welcome-password", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hide_old_welcome_password").val($(".hide_old_welcome_password").val() + character);
        }
    });
    $("body").on("keyup", ".m-old-welcome-password", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".m-old-welcome-password").val().length;
            $(".hide_old_welcome_password").val($(".hide_old_welcome_password").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.m-old-welcome-password').val().length;
            $(".m-old-welcome-password").val(createOldPasswordWelcomeStars(current_val - 1) + $(".m-old-welcome-password").val().substring(current_val - 1));
        }
        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".m-old-welcome-password").val(createstars($(".m-old-welcome-password").val().length));
        }, 200);
    });
});

// Create confirm welcome  password stars js
function createConfirmPasswordWelcomeStars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {
    var timer = "";
    $(".m-confirm-welcome-password").attr("type", "text").removeAttr("name");
    $("body").on("keypress", ".m-confirm-welcome-password", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hide_confirm_welcome_password").val($(".hide_confirm_welcome_password").val() + character);
        }
    });
    $("body").on("keyup", ".m-confirm-welcome-password", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".m-confirm-welcome-password").val().length;
            $(".hide_confirm_welcome_password").val($(".hide_confirm_welcome_password").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.m-confirm-welcome-password').val().length;
            $(".m-confirm-welcome-password").val(createConfirmPasswordWelcomeStars(current_val - 1) + $(".m-confirm-welcome-password").val().substring(current_val - 1));
        }
        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".m-confirm-welcome-password").val(createstars($(".m-confirm-welcome-password").val().length));
        }, 200);
    });
});

// Create New welcome password stars js
function createNewPasswordWelcomeStars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {
    var timer = "";
    $(".m-new-welcome-password").attr("type", "text").removeAttr("name");
    $("body").on("keypress", ".m-new-welcome-password", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hide_new_welcome_password").val($(".hide_new_welcome_password").val() + character);
        }
    });
    $("body").on("keyup", ".m-new-welcome-password", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".m-new-welcome-password").val().length;
            $(".hide_new_welcome_password").val($(".hide_new_welcome_password").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.m-new-welcome-password').val().length;
            $(".m-new-welcome-password").val(createNewPasswordWelcomeStars(current_val - 1) + $(".m-new-welcome-password").val().substring(current_val - 1));
        }
        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".m-new-welcome-password").val(createstars($(".m-new-welcome-password").val().length));
        }, 200);
    });
});

// Clipboard Copy js
$('.copy_help_email').on("click",function () {

    $('.copy_help_email').tooltip('hide')
        .attr('data-original-title', 'Copy to Clipboard')
        .tooltip('show');
});

$('.copy_customer_support_number').on("click",function () {

    $('.copy_customer_support_number').tooltip('hide')
        .attr('data-original-title', 'Copy to Clipboard')
        .tooltip('show');
});

$('.copy_api_key_1').on("click",function () {
    $('.copy_api_key_1').tooltip('hide')
        .attr('data-original-title', 'Copy to Clipboard')
        .tooltip('show');
});

$('.copy_api_key_2').on("click",function () {
    $('.copy_api_key_2').tooltip('hide')
        .attr('data-original-title', 'Copy to Clipboard')
        .tooltip('show');
});

$('.copy_api_key_3').on("click",function () {
    $('.copy_api_key_3').tooltip('hide')
        .attr('data-original-title', 'Copy to Clipboard')
        .tooltip('show');
});

$('.copy_api_key_4').on("click",function () {
    $('.copy_api_key_4').tooltip('hide')
        .attr('data-original-title', 'Copy to Clipboard')
        .tooltip('show');
});

$('.copy_transection_id').on("click",function () {
    $('.copy_transection_id').tooltip('hide')
        .attr('data-original-title', 'Copy to Clipboard')
        .tooltip('show');
});

// welcome change password screen valdidation
$(".m-old-welcome-password").on('keyup change', function (){
    if($(this).val() !==''){
        $('.m-old-welcome-password').css("border","2px solid rgb(53, 186, 182");
    }
    else
    {
        $('.m-old-welcome-password').css("border", "2px solid #e88a8a");
    }
});

$(".m-confirm-welcome-password").on('keyup change', function (){
    if($(this).val() !==''){
        $('.m-confirm-welcome-password').css("border","2px solid rgb(53, 186, 182");
    }
    else
    {
        $('.m-confirm-welcome-password').css("border", "2px solid #e88a8a");
    }
});


// Get current date in invoice
var today = new Date();
var day = today.getDate().toString();
var m = today.getMonth()+1;
if(m < 10){
    m='0'.concat(m);
}
var month=m.toString();
var yyyy = today.getFullYear().toString();
$('#m_datepicker_1_modal').val(month.concat('/',day,'/',yyyy));
$('#m_datepicker_2_modal').val(month.concat('/',day,'/',yyyy));


// file upload js in fight chargeback case
$('#get_file').on('click',function () {
// document.getElementById('get_file').onclick = function() {
    document.getElementById('my_file').click();
});

$('#my_file').on('change',function () {
// document.getElementById('my_file').onchange = function () {
    $('.attatchments').show();
    $('.filename').html(this.files.item(0).name);
});

$('.alert-dismissible').click(function () {
    $('.attatchments').hide();
    $('#my_file').val('');
});





