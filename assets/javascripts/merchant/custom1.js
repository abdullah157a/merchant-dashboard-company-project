// data table search //
$('#searchfilter').click(function () {
    $('#successfullFilter').show();
    $('#declinedFilterForm').hide();
    $('#searchfilter').hide();
});
$('#hide').click(function () {
    $('#searchfilter').show();
    $('#successfullFilter').hide();
});
// End table search //
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<img class="img-fluid" src="'+$(this).data('img') + '" />';
        },
    })
});

//  hyper link on table row //
jQuery(document).ready(function($)
{
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
//  modal link on table row //
$(function(){
    $('#ach_detail').modal({
        keyboard: true,
        backdrop: "static",
        show:false,
    })
});
$(function(){
    $('#p2c_detail').modal({
        keyboard: true,
        backdrop: "static",
        show:false,
    })
});
$(function(){
    $('#echeck_detail').modal({
        keyboard: true,
        backdrop: "static",
        show:false,
    })
});
$('.btn-exported').on('click',function () {
    setTimeout(function(){
        $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft').addClass('show-calendar modal-range');
    }, 500);
    $('.ranges ul li:last-child').mouseover();
});
// p2c slect
$('.p2c_select a').click(function () {
    var value=$(this).attr('data-value');
    $('.permission-dropdown').text(value);
    $('.dropdown-menu--pad').removeClass('show');
});
// hide card filed on checkbox
$(document).on('click','#add_new_card_check', function () {
    if($(this).prop("checked") == true){
        $(".select_card").prop('disabled', true);
    }
    else if($(this).prop("checked") == false){
        $(".select_card").prop('disabled', false);
    }
});


// welcome button action

$('input[id="agree_btn"]').click(function(){
    if($(this).prop("checked") == true){
        $(".agree_btn").prop('disabled', false);
    }
    else if($(this).prop("checked") == false){
        $(".agree_btn").prop('disabled', true);
    }
});

$("#mobile_no").on('keyup change', function (){
    if($(this).val() !==''){
        $('#mobile_phone_error').hide();
        $('#email_error').hide();
        $('#mobile_no').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
        $('#email').attr('style', 'border:2px solid rgb(53, 186, 182) !important');
    }
    else
    {
        $('#mobile_phone_error').show();
        $('#email_error').show();
        $("#mobile_no").css("border", "2px solid #e88a8a");
        $("#email").css("border", "2px solid #e88a8a");
    }
});
$('#hide').click(function () {
    $('.mail_box_second').show();
    $('.mail_box_first').hide();
});

// $(document).on('change','#filter_form input[type="checkbox"]',function () {
//     if($(this).attr("class") == "checkbox_2"){
//         $('#filter_form input[type="checkbox"]').prop("checked",true)
//         $(this).prop("checked",true)
//     }else{
//         $('#filter_form input[type="checkbox"]#search_all').prop("checked",false)
//     }
//     // search_call();
// });

$(".checkbox_2").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
});