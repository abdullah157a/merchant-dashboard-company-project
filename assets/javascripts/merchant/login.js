// Create Password Js
$(".m-create-password-input").focus(function() {
    $("#tooltips").show();
});

var starPasswordValue;
$( ".login-eye" ).click(function() {
    var password=$(".hidpassw").val();
    starPasswordValue=$(".m-login-password").val();
    $(".m-login-password").val(password);
    $(".login-open-eye").show();
    $(".login-eye").hide();
});

$( ".login-open-eye" ).click(function() {
    var password=$(".hidpassw").val();
    $(".m-login-password").val(starPasswordValue);
    $(".login-open-eye").hide();
    $(".login-eye").show();
});



$(".m-create-password-input").on("keyup change",function() {
    var PasswordValue=$('.m-create-password-input').val();
    var confirmPasswordValue=$('.m-confirm-password-input').val();

    if($(".termsandservices_cb").prop("checked") == true && confirmPasswordValue!='' && PasswordValue!=''){
        $(".m_login_signin_submit").removeAttr('id');
        $(".m_login_signin_submit").addClass("m-login__btn--primary");
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

$(".m-confirm-password-input").on("keyup change",function() {
    var PasswordValue=$('.m-create-password-input').val();
    var confirmPasswordValue=$('.m-confirm-password-input').val();
    if($(".termsandservices_cb").prop("checked") == true && PasswordValue!='' && confirmPasswordValue !=''){
        $(".m_login_signin_submit").removeAttr('id');
        $(".m_login_signin_submit").addClass("m-login__btn--primary");
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

$('input[type="checkbox"]').click(function(){
    if($(".termsandservices_cb").prop("checked") == true){
        $PasswordValue=$('.m-create-password-input').val();
        $confirmPasswordValue=$('.m-confirm-password-input').val();
        if($PasswordValue!='' && $confirmPasswordValue!=''){
            $(".m_login_signin_submit").removeAttr('id');
            $(".m_login_signin_submit").addClass("m-login__btn--primary");
        }
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

//End Create Password js

//Create password starts
function createstars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {

    var timer = "";

    $(".panel").append($('<input type="text" class="hidpassw" />'));

    $(".hidpassw").attr("name", $(".pass").attr("name"));

    $(".pass").attr("type", "text").removeAttr("name");

    $("body").on("keypress", ".pass", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hidpassw").val($(".hidpassw").val() + character);
        }


    });

    $("body").on("keyup", ".pass", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".pass").val().length;
            $(".hidpassw").val($(".hidpassw").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.pass').val().length;
            $(".pass").val(createstars(current_val - 1) + $(".pass").val().substring(current_val - 1));
        }

        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".pass").val(createstars($(".pass").val().length));
        }, 200);

    });

});

//confirm password starts
function createconfirmstars(n) {
    return new Array(n+1).join("*")
}


$(document).ready(function() {

    var timer = "";

    $(".panel").append($('<input type="text" class="hidconfirmpassw" />'));

    $(".hidconfirmpassw").attr("name", $(".pass").attr("name"));

    $(".confirmpass").attr("type", "text").removeAttr("name");

    $("body").on("keypress", ".confirmpass", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hidconfirmpassw").val($(".hidconfirmpassw").val() + character);
        }


    });

    $("body").on("keyup", ".confirmpass", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".confirmpass").val().length;
            $(".hidconfirmpassw").val($(".hidconfirmpassw").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.confirmpass').val().length;
            $(".confirmpass").val(createconfirmstars(current_val - 1) + $(".confirmpass").val().substring(current_val - 1));
        }

        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".confirmpass").val(createstars($(".confirmpass").val().length));
        }, 200);

    });
});

var confirmPasswordStarts;
$( ".login-confirm-eye" ).click(function() {
    var password=$(".hidconfirmpassw").val();
    confirmPasswordStarts=$(".m-confirm-password-input").val();
    $(".m-confirm-password-input").val(password);
    $(".login-confirm-open-eye").show();
    $(".login-confirm-eye").hide();
});

$( ".login-confirm-open-eye" ).click(function() {
    var password=$(".hidconfirmpassw").val();
    $(".m-confirm-password-input").val(confirmPasswordStarts);
    $(".login-confirm-open-eye").hide();
    $(".login-confirm-eye").show();
});

$(".m-confirm-password-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.password-confirm-error').hide();
        $('.m-confirm-password-input').css("border","solid 2px #35bab6");
        $(".login-confirm-eye").show();
        $(".login-confirm-open-eye").hide();
    }
    else
    {
        $('.password-confirm-error').show();
        $(".login-confirm-open-eye").hide();
        $(".login-confirm-eye").show();
        $('.m-confirm-password-input').css("border","solid 2px #d45858");
    }
});



// End Confirm Password js

// Login Page Js
$(".email-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.email-login-error').hide();
        $('.email-input').css("border","solid 2px #35bab6");
    }
    else
    {
        $('.email-login-error').show();
        $('.email-input').css("border","solid 2px #d45858");
    }
});

$(".m-login-password").on('keyup change', function (){
    if($(this).val() !==''){
        $('.password-login-error').hide();
        $('.m-login-password').css("border","solid 2px #35bab6");
        $(".login-eye").show();
        $(".login-open-eye").hide();
    }
    else
    {
        $(".login-open-eye").hide();
        $(".login-eye").show();
        $('.password-login-error').show();
        $('.m-login-password').css("border","solid 2px #d45858");
    }
});



$('#terms_and_services').click(function () {
    $('.term_service_text').show();
    const element =  document.querySelector('.term_service_text')
    element.classList.add('animated', 'slideInLeft')
});

$('.close_terms_services').click(function () {

    $('.term_service_text').hide();
    $('#terms_and_services').removeClass('slideInLeft');
});

// End Login Page JS

$('#terms_and_services').click(function () {
    $('.term_service_text').show();
    const element =  document.querySelector('.term_service_text')
    element.classList.add('animated', 'slideInLeft')
});

$('.close_terms_services').click(function () {

    $('.term_service_text').hide();
    $('#terms_and_services').removeClass('slideInLeft');
});

$('body').click(function () {
   $('#tooltips').hide();
});

$('#tooltips').click(function (event) {
    $('#tooltips').show();
    event.stopPropagation();
});

$('.m-create-password-input').click(function (event) {
    $('#tooltips').show();
    event.stopPropagation();
});








